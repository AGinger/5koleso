<?php
/**
 * Options Management Administration Screen.
 *
 * If accessed directly in a browser this page shows a list of all saved options
 * along with editable fields for their values. Serialized data is not supported
 * and there is no way to remove options via this page. It is not linked to from
 * anywhere else in the admin.
 *
 * This file is also the target of the forms in core and custom options pages
 * that use the Settings API. In this case it saves the new option values
 * and returns the user to their page of origin.
 *
 * @package WordPress
 * @subpackage Administration
 */
require_once __DIR__ . '/admin.php';
wp_reset_vars(array( 'action', 'option_page' ));
$redirect = 'options-general.php?page=5kplugin';
global $wpdb;

$table = 'wp_post_blocks_item';
$post_status ='publish';
$data = [];
$post_1 = trim($_POST['post_1']);
$post_2 = trim($_POST['post_2']);
$post_3 = trim($_POST['post_3']);
$post_4 = trim($_POST['post_4']);
$post_5 = trim($_POST['post_5']);

// Привет как-то так
// Привет, мир!
//SELECT id FROM `wp_posts` WHERE post_type = 'post' AND post_status = 'publish' AND post_title = 'Чем отполировать кузов, чтобы он долго сохранял блеск?' AND post_title = '';
$revision_ids = $wpdb->get_col("SELECT * FROM $wpdb->posts WHERE post_type = 'post' AND post_status = 'publish' AND post_title IN ('$post_1','$post_2','$post_3','$post_4','$post_5')");

$post_id = implode(",", $revision_ids);


if (!isset($_POST['post_status'])) {
    $status = 0;
} else {
    $status = 1;
}

if (!empty($_POST)) {
    $format = [
    '%s','%s','%s','%s','%s','%s','%s'
  ];
    $data = [
      'name_post_1' => trim($_POST['post_1']),
      'name_post_2' => trim($_POST['post_2']),
      'name_post_3' => trim($_POST['post_3']),
      'name_post_4' => trim($_POST['post_4']),
      'name_post_5' => trim($_POST['post_5']),
      'post_id' => $post_id,
      'status' => $status,
  ];
    $where = [
    'id' => '1',
  ];
    $wpdb->update($table, $data, $where, $format);
    wp_redirect(admin_url($redirect));
}
