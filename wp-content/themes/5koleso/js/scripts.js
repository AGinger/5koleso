$(document).ready(function(){

	//$('iframe').wrapAll('<div class="video-wrap"></div>');

	if($(".default-slider").length) {
		$('.default-slider .owl-carousel').owlCarousel({
				center: false,
		    loop: false,
		    nav: true,
		    margin:20,
				loop: true,
		   	navText:['',''],
		    responsive:{
		        0:{
		            items:1,
		            margin:11,
		           	stagePadding: 30,
		        },
		        767:{
		            items:3
		        }
		    }
		});
	}

	if($(".partners-slider").length) {
		$('.partners-slider .owl-carousel').owlCarousel({
		    loop: false,
		    nav: true,
		    margin:20,
		   	navText:['',''],
		    responsive:{
		        0:{
		            items:3
		        },
		        767:{
		            items:5
		        },
		        991:{
		        	items:15
		        }
		    }
		});
	}
	if($(window).width() > 768) {
		$('#menu-osnovnoe-menyu').owlCarousel({
		    loop: false,
		    nav: true,
				autoWidth:true,
		    margin:20,
		   	navText:['',''],

		});
}
	$('body').prepend('<div class="menu-overlay"/>');
	$('.toggle-menu').on('click', function(){
		$('.toggle-menu,.topmenu-close,.primary-menu,.topmenu-container,.menu-overlay,header .text-center.logo a,body,html').toggleClass('active');
	});

	$('.menu-overlay, .topmenu-close').on('click', function(){
		$('.toggle-menu,.topmenu-close,.primary-menu,.topmenu-container,.menu-overlay,header .text-center.logo a,body,html').removeClass('active');
	});

	$('.news-page .owl-carousel').owlCarousel({
	    loop: true,
	    nav: true,
	    margin:20,
	   	navText:['',''],
	   	responsive:{
	        0:{
	            items:1,
	            margin:11,
	           	stagePadding: 30,
	        },
	        767:{
	            items:1,
							margin: 0,
							stagePadding: 0
	        }
	    }
	});
	$('.col .driving').each(function(){
		var colDriving = parseInt($(this).attr('data-rate')).toFixed();
		$(this).css({'width' : ''+colDriving+'0%'});
	});
	$('.col .salon').each(function(){
		var colSalon = parseInt($(this).attr('data-rate')).toFixed();
		$(this).css({'width' : ''+colSalon+'0%'});
	});
	$('.col .security').each(function(){
		var colSecurity = parseInt($(this).attr('data-rate')).toFixed();
		$(this).css({'width' : ''+colSecurity+'0%'});
	});
	$('.col .comfort').each(function(){
		var colComfort = parseInt($(this).attr('data-rate')).toFixed();
		$(this).css({'width' : ''+colComfort+'0%'});
	});
	$('.col .price').each(function(){
		var colPrice = parseInt($(this).attr('data-rate')).toFixed();
		$(this).css({'width' : ''+colPrice+'0%'});
	});

	$('.must-log-in').html('<p>Для отправки комментария вам необходимо авторизоваться.</p>');

	function loadPage( page ) {
		//$(this).text('Загружаю...'); // изменяем текст кнопки, вы также можете добавить прелоадер
		
		true_posts = true_posts.replace( /"offset";i:0/g, '"offset";i:1' ); 
		max_pages = parseInt( max_pages );
		if( current_page == page )
			return;
		var data = {
			'action': 'loadmore',
			'query': true_posts,
			'page' : page - 1
		};
		$.ajax({
			url:'/wp-content/themes/5koleso/new_ajax.php', // обработчик
			data:data, // данные
			type:'POST', // тип запроса
			success:function(data){
				if( data ) {
					$('.section-cards-wrapper > .flex .default-link').remove();										
					$('.section-cards-wrapper > .flex').prepend(data);
					if (current_page == max_pages) $("#loadmore").text("Больше нет новых записей").addClass('nopage');
				} else {
					$('#loadmore').text("Больше нет новых записей").addClass('nopage');
				}

				//$('#loadmore').text('Загрузить ещё').before(data); // вставляем новые посты
				current_page = parseInt( page ); // ++; // увеличиваем номер страницы на единицу
				history.pushState({page: current_page}, document.title , location.href.replace( /\?pg=\d+$/g, '' ) + ( current_page > 1 ? "?pg=" + current_page : '' ));
				$('.currpage_number').text( page );

				nextPg = current_page <  max_pages ?  current_page  + 1 :  max_pages;
				prevPg = current_page > 1 ? current_page - 1 : 1;

				nextPgHref = location.href.replace( /\?.+/g, '' ) + '?pg=' + nextPg;
				prevPgHref = location.href.replace( /\?.+/g, '' ) + ( prevPg > 1 ? '?pg=' + prevPg : '' );

				$('.paging-numbers .next .loadpage').data('page', nextPg ).attr('href', nextPgHref );
				$('.paging-numbers .prev .loadpage').data('page', prevPg ).attr('href', prevPgHref );
			}		
		});
	}

	$('.loadpage').click(function(e){
		e.preventDefault();
		page = $(this).data('page');
		loadPage( page );
	});

	$('#selectyear').change(function(){
		true_posts = true_posts.replace( /"offset";i:0/g, '"offset";i:1' ); 
		var data = {
			'action': 'yearfilter',
			'query': true_posts,
			'selectyear': $('#selectyear').val(),
			'page' : 1
		};
		$.ajax({
			url:'/wp-content/themes/5koleso/new_ajax.php', // обработчик
			data:data, // данные
			type:'POST', // тип запроса
			success:function(data){
				if( data ) {
					$('.flex-archive-cards').html(data); // вставляем новые посты
				} else {
					$('.flex-archive-cards').html("<p>Результаты запроса отсутствуют.<br>Проверьте параметры и попробуйте еще раз.</p>");
				}
			}
		});
	});

	$('#selectpage').change(function(){
		$(location).attr('href',$('#selectpage').val());
	});


	$('.select_page').on('click',function(){
		$(this).toggleClass('act');
		$('.bg_pagination').toggleClass('act');
	});

	$('.bg_pagination').on('click',function(){
		$(this).toggleClass('act');
		$('.select_page').toggleClass('act');
	});

	$('.archive-card, .magazine-wrapper').bind('contextmenu', function(e) {
    return false;
	});

	$('.search_button').on('click', function(){
			$(this).next('.form_search').toggleClass('act');
			$('header .logo').toggleClass('act');
			$('#searchform #s').focus();
	});

	$('.bg_search').on('click',function(){
		$(this).toggleClass('act');
		$('.form_search').toggleClass('act');
	$('header .logo').toggleClass('act');
	$('#searchform #s').focus();
	});

	/*if( pageParam )
		loadPage(pageParam);*/
	
$("#comment").focus(function(){
    	$(".comment-form-author, .comment-form-email, .comment-notes, .comment-form-cookies-consent, .form-submit, .comment-form-comment textarea").toggleClass("active");
  	});
			$(".comment-form-comment textarea" 	&& ".comment-form-author input" && ".comment-form-email input").blur(function() {
    	$(".comment-form-author, .comment-form-email, .comment-notes, .comment-form-cookies-consent, .form-submit, .comment-form-comment textarea").removeClass("active");
  });	
	
});
 $(document).ready(function () {
/*let objToStick = $(".test header"); //Получаем нужный объект
let topOfObjToStick = $(objToStick).offset().top; //Получаем начальное расположение нашего блока
alert (topOfObjToStick);*/
    $(window).scroll(function () {	
        var windowScroll = $(window).scrollTop(); //Получаем величину, показывающую на сколько прокручено окно
		        if (windowScroll < 300) { // Если прокрутили больше, чем расстояние до блока, то приклеиваем его
			$('.search_button').next('.form_search').removeClass('act');
			$('header .logo').removeClass('act');
				}
        if (windowScroll > 280) { // Если прокрутили больше, чем расстояние до блока, то приклеиваем его

			$("header").addClass("topWindow");
			$('.topmenu-container').addClass("fixmenu");
			
	
        } else {
            $("header").removeClass("topWindow");
			$('.topmenu-container').removeClass("fixmenu");

        };
});
	 
	   		$("#commentform .comment-form-comment").click(function(){
    	$(this).siblings().show();
		$(this).addClass("big");
  	});


	rdsMenuEl = $('#menu-item-118984').length > 0 ? $('#menu-item-118984') : $('#menu-item-114224');

	if( rdsMenuEl.length > 0 ) {
		var rdsOpened = false;
		if( $(window).width() < 769 ) {

			rdsMenuEl.find('a, a *').click( function(e) {
				if( ! rdsOpened || e.target.outerHTML.match( /^<i /g )) {
					e.stopPropagation();
					e.preventDefault();
					rdsOpened = true;
					rdsMenuEl.find('.sub-menu').toggleClass('active');
				}
			})
	   		/*rdsMenuEl.find('.sab-menus-iten i').click( function(e) {
	   			console.log( e );
   				e.stopPropagation();   				
   				
	   		});*/

		} else {
			rdsMenuEl.mouseover( function() {
				var elSlideshow = $(this).closest('.owl-stage-outer');
				var bMenuSwiped = elSlideshow.find('.owl-stage').css('transform').match( /\s-\d+/g );
				elSlideshow.css({'overflow':'visible'});
				elSlideshow.find('.owl-item:not(.active)').css('visibility', 'hidden');
				if( ! bMenuSwiped )
					elSlideshow.find('.owl-item.active').last().css('visibility', 'hidden');
			}).mouseout( function() {
				var elSlideshow = $(this).closest('.owl-stage-outer');
				var bMenuSwiped = elSlideshow.find('.owl-stage').css('transform').match( /\s-\d+/g );
				elSlideshow.css({'overflow':'hidden'});
				elSlideshow.find('.owl-item:not(.active)').css('visibility', 'visible');
				if( ! bMenuSwiped )
					elSlideshow.find('.owl-item.active').last().css('visibility', 'visible');
			});
		}		
	}
	
	 parkMenuEl = $('#menu-item-138640').length > 0 ? $('#menu-item-138640') : $('#menu-item-114224');

	if( parkMenuEl.length > 0 ) {
		var parkOpened = false;
		if( $(window).width() < 769 ) {

			parkMenuEl.find('a, a *').click( function(e) {
				if( ! parkOpened || e.target.outerHTML.match( /^<i /g )) {
					e.stopPropagation();
					e.preventDefault();
					parkOpened = true;
					parkMenuEl.find('.sub-menu').toggleClass('active');
				}
			})
	   		/*rdsMenuEl.find('.sab-menus-iten i').click( function(e) {
	   			console.log( e );
   				e.stopPropagation();   				
   				
	   		});*/

		} else {
			parkMenuEl.mouseover( function() {
				var elSlideshow = $(this).closest('.owl-stage-outer');
				var bMenuSwiped = elSlideshow.find('.owl-stage').css('transform').match( /\s-\d+/g );
				elSlideshow.css({'overflow':'visible'});
				elSlideshow.find('.owl-item:not(.active)').css('visibility', 'hidden');
				if( ! bMenuSwiped )
					elSlideshow.find('.owl-item.active').last().css('visibility', 'hidden');
			}).mouseout( function() {
				var elSlideshow = $(this).closest('.owl-stage-outer');
				var bMenuSwiped = elSlideshow.find('.owl-stage').css('transform').match( /\s-\d+/g );
				elSlideshow.css({'overflow':'hidden'});
				elSlideshow.find('.owl-item:not(.active)').css('visibility', 'visible');
				if( ! bMenuSwiped )
					elSlideshow.find('.owl-item.active').last().css('visibility', 'visible');
			});
		}		
	}
	 
	 
});