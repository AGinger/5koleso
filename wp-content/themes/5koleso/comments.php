<?php
/**
 * @package WordPress
 * @subpackage Theme_Compat
 * @deprecated 3.0.0
 *
 * This file is here for backward compatibility with old themes and will be removed in a future version
 */
_deprecated_file(
	/* translators: %s: Template name. */
	sprintf( __( 'Theme without %s' ), basename( __FILE__ ) ),
	'3.0.0',
	null,
	/* translators: %s: Template name. */
	sprintf( __( 'Please include a %s template in your theme.' ), basename( __FILE__ ) )
);

// Do not delete these lines
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && 'comments.php' == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
	die( 'Please do not load this page directly. Thanks!' );
}

if ( post_password_required() ) { ?>
		<p class="nocomments"><?php _e( 'This post is password protected. Enter the password to view comments.' ); ?></p>
	<?php
	return;
}
?>

<!-- You can start editing here. -->
<?php $args = array(
  'title_reply'       => 'Обсуждение',
  'title_reply_to'    => __( 'Leave a Reply to %s' ),
  'cancel_reply_link' => __( 'Отменить' ),
  'label_submit'      => __( 'Post Comment' ),

  'comment_field' =>  '<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" placeholder="Добавить комментарий" required="required" aria-required="true" style="width: 100%;">' .
    '</textarea></p><div style="display: flex;
    order: 6;" class="scaptcha"><div class="g-recaptcha" data-sitekey="6LdqDksaAAAAALehpIGKwgYZgXlFU54Dc-VUCTUu">

    </div></div>',

  'fields' => apply_filters( 'comment_form_default_fields', array(

    'author' =>
      '<p class="comment-form-author">' .
      '<input id="author" placeholder="Имя *" name="author" type="text" required="required" value="' . esc_attr( $commenter['comment_author'] ) .
      '" size="30"' . $aria_req . '  style="maxwidth: 100%;"/></p>',
 
    'email' =>
      '<p class="comment-form-email">' .
      '<input id="email" placeholder="E-mail *" name="email" required="required" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
      '" size="30"' . $aria_req . ' style="maxwidth: 100%;" /></p>',
 
    'url' =>
      '<p class="comment-form-url"><label for="url">' .
      __( 'Website', 'domainreference' ) . '</label>' .
      '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
      '" size="30" /></p>',

    )
  ),
); 
function pluralForm($n, $form1, $form2, $form5)
{
$n = abs($n) % 100;
$n1 = $n % 10;
if ($n > 10 && $n < 20) return $form5;
if ($n1 > 1 && $n1 < 5) return $form2;
if ($n1 == 1) return $form1;
return $form5;
}


?>
	
	<?php comment_form($args); ?>
<?php if ( have_comments() ) : ?>

	<h3 id="comments">
		<?php
		if ( 1 == get_comments_number() ) {?>
			<!--<?printf(
				/* translators: %s: Post title. */
				__( 'One response to %s' ),
				get_the_title() 
			);?>-->
			<?=get_comments_number().pluralForm(get_comments_number(), " комментарий", " комментария", " комментариев");?>
		<?} else {?>
			<!--<?printf(
				/* translators: 1: Number of comments, 2: Post title. */
				_n( '%1$s response to %2$s', '%1$s responses to %2$s', get_comments_number() ),
				number_format_i18n( get_comments_number() ),
				'&#8220;' . get_the_title() . '&#8221;'
			);?>-->
			<?=get_comments_number().pluralForm(get_comments_number(), " комментарий", " комментария", " комментариев");?>
		<?}
		?>
	</h3>

	<?/*<div class="navigation">
		<div class="alignleft"><?php previous_comments_link(); ?></div>
		<div class="alignright"><?php next_comments_link(); ?></div>
	</div>*/?>

	<ol class="commentlist">
	<?php wp_list_comments(); ?>
		</ol>
<? if(get_comments_number() <=10){?>
<style>
.lmb-container{
	display:none;
}
</style>
<?}?>
	<div class="navigation">
	
		<?/*<div class="alignleft"><?php previous_comments_link(); ?></div>*/?>
		<div class="aligncenter"><?php the_comments_pagination(); ?></div>
	<?/*	<div class="alignright"><?php next_comments_link(); ?></div>*/?>
	</div>
	
<?php else : // this is displayed if there are no comments so far ?>

	<?php if ( comments_open() ) : ?>
		<!-- If comments are open, but there are no comments. -->

	<?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="nocomments"><?php _e( 'Comments are closed.' ); ?></p>

	<?php endif; ?>
<?php endif; ?>


