<?php get_header(); ?>

<main class="container">
  <div class="flex">
    <div class="left-col">
      <div class="section-cards-wrapper">
        <h1>Результаты по тегу "<?php single_tag_title();?>"</h1>

        <div class="flex">
          <?php
  					$query = new WP_Query([
  						'tag' => single_tag_title("", 0),
  						'order'   => 'DESC',
  					]);
  					while($query->have_posts()) {
  						$query->the_post();
  					?>
            <a href="<?php the_permalink(); ?>" class="default-link">
              <span class="img-wrapper">
                <img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
              </span>
              <span><?php the_title(); ?></span>
            </a>
  				  <?php
  				}
  				wp_reset_postdata();
  			?>


        </div>

      </div>
    </div>
    <div class="right-col">
      <?php
				if (function_exists('dynamic_sidebar')){
					dynamic_sidebar('right_column');
				}
			?>
    </div>
  </div>
</main>

<?php get_footer(); ?>
