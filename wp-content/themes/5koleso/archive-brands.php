<?php get_header(); ?>

	<main class="container">
		<div class="flex">
			<div class="left-col">
				<div class="section-cards-wrapper">
					<ul class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	          <? bcn_display() ?>
	        </ul>
					<h1><?php the_archive_title(); ?></h1>

						<div class="all_brands">
							<?
							$args = [
								'post_type' => 'brands',
								'posts_per_page' => -1,
								'orderby' => 'title',
								'order'   => 'ASC',
							];
							$query = new WP_Query($args);
							while ($query->have_posts()) {
								$query->the_post();
								?>
								<div class="item">
									<a href="<?php the_permalink(); ?>" class="block">
										<div class="img"><img src="<?=kama_thumb_src('h=50 &crop=center', get_the_post_thumbnail_url())?>" alt="<?php the_title();?>"></div>
										<div class="name"><?php the_title();?></div>
									</a>
								</div>
								<?
							}
							wp_reset_postdata();
							?>
							
						</div>

				</div>
			</div>
			<div class="right-col">
				<?php
					if (function_exists('dynamic_sidebar')){
						dynamic_sidebar('right_column');
					}
					if (function_exists('dynamic_sidebar')){
	          dynamic_sidebar('right_column_inner');
	        }
				?>
    			</div>
		</div>
	</main>

	<section class="default-slider gray">
	  <div class="container">
	    <div class="heading">
	      <h6>Популярное</h6>
	    </div>
	    <div class="owl-carousel">
	     	<?php popular_carousel( $post->ID ); ?>
	    </div>
	  </div>
	</section>
	<div class="mobile_banners">
		<?php
			if (function_exists('dynamic_sidebar')){
				dynamic_sidebar('above_footer_mob');
			}
			?>
	</div>

	<div class="desktop_banners">
		<?php
			if (function_exists('dynamic_sidebar')){
				dynamic_sidebar('above_footer_desk');
			}
			?>
	</div>
<?php get_footer(); ?>
