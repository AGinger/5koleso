<?php
get_header();
?>

<main class="container">
	<div class="flex">
		<div class="left-col">
			<div class="error-code-wrapper">
				<img src="<?=get_template_directory_uri()?>/img/404.png" alt="...">
				<p>Запрашиваемая вами страница не найдена :(</p>
				<a href="/">Вернуться на главную</a>
			</div>
		</div>
		<div class="right-col"></div>
	</div>
</main>


<section class="default-slider gray">
	<div class="container">
		<div class="heading">
			<h6>Популярное</h6>
		</div>
		<div class="owl-carousel">
			<div class="item">
				<a href="#" class="default-link">
					<span class="img-wrapper">
						<img src="<?=get_template_directory_uri()?>/img/d-1.jpg" alt="...">
					</span>
					<span>Seat Altea Freetrack. Снежное испытание</span>
				</a>
			</div>
			<div class="item">
				<a href="#" class="default-link">
					<span class="img-wrapper">
						<img src="<?=get_template_directory_uri()?>/img/d-2.jpg" alt="...">
					</span>
					<span>Brilliance M2. Бюджетное счастье</span>
				</a>
			</div>
			<div class="item">
				<a href="#" class="default-link">
					<span class="img-wrapper">
						<img src="<?=get_template_directory_uri()?>/img/d-3.jpg" alt="...">
					</span>
					<span>Hyundai Tucson 725 900 руб.</span>
				</a>
			</div>
			<div class="item">
				<a href="#" class="default-link">
					<span class="img-wrapper">
						<img src="<?=get_template_directory_uri()?>/img/d-1.jpg" alt="...">
					</span>
					<span>Seat Altea Freetrack. Снежное испытание</span>
				</a>
			</div>
			<div class="item">
				<a href="#" class="default-link">
					<span class="img-wrapper">
						<img src="<?=get_template_directory_uri()?>/img/d-2.jpg" alt="...">
					</span>
					<span>Brilliance M2. Бюджетное счастье</span>
				</a>
			</div>
			<div class="item">
				<a href="#" class="default-link">
					<span class="img-wrapper">
						<img src="<?=get_template_directory_uri()?>/img/d-3.jpg" alt="...">
					</span>
					<span>Hyundai Tucson 725 900 руб.</span>
				</a>
			</div>
		</div>
	</div>
</section>

<section class="partners-slider">
	<div class="container">
		<div class="owl-carousel">
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/bmw.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/mb.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/vw.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/pch.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/ld.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/ol.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/kia.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/st.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/fd.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/ai.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/lx.png" alt="...">
				</a>
			</div>

			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/bmw.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/mb.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/vw.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/pch.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/ld.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/ol.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/kia.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/st.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/fd.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/ai.png" alt="...">
				</a>
			</div>
			<div class="item">
				<a href="#" class="wrapper">
					<img src="<?=get_template_directory_uri()?>/img/lx.png" alt="...">
				</a>
			</div>

		</div>
	</div>
</section>

<?php get_footer(); ?>
