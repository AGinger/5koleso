<?php
/*
 Single Post Template: single-archive
 Description: Шаблон для страниц архива
 */
 ?>
<?php get_header(); ?>
<?php setPostViews(get_the_ID()); // количество просмотров ?>

<main class="container">
  <div class="flex">
    <div class="left-col">
      <div class="archives-col-wrapper section-cards-wrapper">
        <ul class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
          <? bcn_display() ?>
        </ul>
        <h1><? the_title() ?></h1>

        <a href="<?=get_field("archive_pdf", $post->ID)?>" class="archive-card" target="_blank">
          <img src="<?=kama_thumb_src('w=280 &h=365 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
        </a>
        <?
        $cat = get_the_category($post->ID);
        $catid = $cat[0]->term_id;
        ?>

        <h6>Архив номеров</h6>
        <div class="flex-archive-cards">
          <?php
            $categories = get_the_category();
            $exclude_id[] = get_the_ID();
            $query = new WP_Query([
              'cat'   => $catid,
              'posts_per_page' => 12,
              'order'   => 'DESC',
              'post__not_in'   => $exclude_id,
            ]);
            while($query->have_posts()) {
              $query->the_post();
            ?>
            <a href="<?php the_permalink(); ?>" class="archive-card">
              <span class="name"><?php the_title(); ?></span>
              <img src="<?=kama_thumb_src('w=280 &h=365 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
            </a>
            <?php
          }
          wp_reset_postdata();

  ?>
            </div>



        <div class="subscribe-form">
          <h4>Хочу получать самые интересные статьи</h4>
          <?
          $form_widget = new \MailPoet\Form\Widget();
          echo $form_widget->widget(array('form' => 2, 'form_type' => 'php'));
          ?>
        </div>
      </div>
    </div>
    <div class="right-col">
      <?php
				if (function_exists('dynamic_sidebar')){
					dynamic_sidebar('right_column');
				}
        if (function_exists('dynamic_sidebar')){
          dynamic_sidebar('right_column_inner');
        }
			?>
    </div>
  </div>
</main>
<div class="mobile_banners">
  <?php
    if (function_exists('dynamic_sidebar')){
      dynamic_sidebar('above_footer_mob');
    }
    ?>
</div>

<div class="desktop_banners">
  <?php
    if (function_exists('dynamic_sidebar')){
      dynamic_sidebar('above_footer_desk');
    }
    ?>
</div>
<section class="default-slider gray">
  <div class="container">
    <div class="heading">
      <h6>Популярное
    </div>
    <div class="owl-carousel">
      <?php
        $query = new WP_Query([
          'post_type' => 'post',
          'posts_per_page' => 10,
          'order'   => 'DESC',
          'post__not_in'   => [$post->ID],
          'meta_key' => 'post_views_count',
          'orderby' => 'meta_value_num',
          'date_query' => [
            'after' => '2 weeks ago',
          ]
        ]);
        while($query->have_posts()) {
          $query->the_post();
          $title = get_the_title();
          if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
          elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
          else {$vtitle[0] = $title;}
        ?>
        <div class="item">
          <a href="<?php the_permalink(); ?>" class="default-link">
            <span class="img-wrapper">
              <img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
            </span>
            <span><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
          </a>
        </div>
        <?php
        $vtitle[1]='';
      }
      wp_reset_postdata();

    ?>
    </div>
  </div>
</section>

<section class="partners-slider">
  <div class="container-fluid">
    <div class="owl-carousel">
      <?php
        $cat = get_the_category($post->ID);

        $query = new WP_Query([
          'post_type' => 'brands',
          'posts_per_page' => -1,
          'orderby' => 'name',
          'order' => 'ASC',
        ]);
        while($query->have_posts()) {
          $query->the_post();
        ?>
        <div class="item">
          <a href="<?php the_permalink(); ?>" class="wrapper">
            <img src="<?=kama_thumb_src('h=55 &crop=0', get_the_post_thumbnail_url())?>" alt="alt">
          </a>
        </div>
        <?php
        }
        wp_reset_postdata();
      ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>
