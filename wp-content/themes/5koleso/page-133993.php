<?php get_header('rds'); ?>
<div class="mobile_banners">
	<!--AdFox START-->
	<!--spn.ru-->
	<!--Площадка: 5koleso.ru / Сквозной / 5k mobile-->
	<!--Категория: <не задана>-->
	<!--Баннер: <не задана>-->
	<!--Тип баннера: 240x400 и другие-->
	<div id="adfox_157112937405079834"></div>
	<script>
	    window.Ya.adfoxCode.create({
	       ownerId: 175986,
	       containerId: 'adfox_157112937405079834',
	       params: {
	           p1: 'bykch',
	           p2: 'ends',
	           pfc: 'cgiwl',
	           pfb: 'hatkv'
	       }
	   });
	</script>
</div>

<section class="tests container">
	<div class="flex">
		<div class="left">
      <?
        $exclude_id = [];

        // Главный пост
        $index_post_rds = get_field('index_post_avtopark');
        if($index_post_rds){
          // если задан главный пост вручную
          $index_category = get_the_category($index_post_rds[0]);
          $query = new WP_Query([
            'p' => $index_post_rds[0],
            'posts_per_page' => 1,
          ]);
        }
        else {
          $query = new WP_Query([
            'cat' => 25, // тесты
						'posts_per_page' => 1,
						'order'   => 'DESC',
          ]);
        }

				while($query->have_posts()) {
					$query->the_post();

          // категория
          $category_object = get_the_category(get_the_ID());
          $category_name = $category_object[0]->name;

          $exclude_id[] = get_the_ID(); // добавляем записи в исключение
				?>
        <a href="<?php the_permalink(); ?>" class="card" style="background: url(<?=kama_thumb_src('w=875 &h=440 &crop=center', get_field("img_intro", $post->ID))?>) no-repeat center / cover;">
  				<u class="sticker"><?=$index_post ? $index_category[0]->name : $category_name?></u>
  				<div class="text">
  					<h2><?php the_title(); ?></h2>
  				</div>
  			</a>
			  <?php
				}
				wp_reset_postdata();
      ?>
			<div class="mobile_banners">
				<!--AdFox START-->
	<!--spn.ru-->
	<!--Площадка: 5koleso.ru / * / *-->
	<!--Тип баннера: 240x400 и другие-->
	<!--Расположение: 5k mobile-->
	<div id="adfox_157538416759398208"></div>
	<script>
	    window.Ya.adfoxCode.createAdaptive({
	        ownerId: 175986,
	        containerId: 'adfox_157538416759398208',
	        params: {
	            pp: 'llu',
	            ps: 'wsi',
	            p2: 'ends'
	        }
	    }, ['desktop', 'tablet', 'phone'], {
	        tabletWidth: 830,
	        phoneWidth: 480,
	        isAutoReloads: false
	    });
	</script>
			</div>

				<div class="heading">
					<h6>Новости комтранса</h6>

					<a href="<?=get_category_link(3240)?>" class="more">Смотреть все</a>
				</div>
				<div class="flex-child">
        <?php
					$query = new WP_Query([
						'cat' => 3240,
						'posts_per_page' => 6,
						'order'   => 'DESC',
					]);
					while($query->have_posts()) {
						$query->the_post();
            $exclude_id[] = get_the_ID(); // добавляем записи в исключение
					?>

          <div class="col col__news">
  					<a href="<?php the_permalink(); ?>" class="default-link">
							<img src="<?=kama_thumb_src('w=260 &h=155 &crop=center', get_field("img_intro", $post->ID))?>" alt="news_<?=$post->ID?>" />
							<div class="name"><?php the_title(); ?></div></a>
  				</div>
				  <?php
  				}
  				wp_reset_postdata();
  			?>
			</div>
		</div>
		<div class="right">
						<?php
if (function_exists('dynamic_sidebar')) {
    dynamic_sidebar('right_column_main');
}
?>
		</div>
	</div>
</section>
<div class="container main_h1">
	<div class="heading">
		<h1 class='h6'>Журнал о коммерческом транспорте</h1>
		<a href="<?=get_category_link(25)?>" class="more">Смотреть все</a></div>		
	</div>
	<section class="section-links container">
	<div class="flex">
    <?
    
      $index_pilots = get_field('zhurnal_o_kommercheskom_transporte');
      $query = new WP_Query([
      	'cat' => 25,
		'post_type'    => 'post',
        'post__in' => $index_pilots,
        'posts_per_page' => 3,
        'orderby' => 'post__in',
        'post_status' => 'publish',
      ]);
      while($query->have_posts()) {
        $query->the_post();
        $exclude_id[] = get_the_ID(); // добавляем записи в исключение
				if ($post->ID == 89502) continue;
        $category_name = get_the_category(get_the_ID());
				$title = get_the_title();
				if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
				elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
				else {$vtitle[0] = $title;}
      ?>
      <a href="<?php the_permalink(); ?>" class="card" style="background: url(<?=kama_thumb_src('w=875 &h=440 &crop=center', get_field("img_intro", $post->ID))?>) no-repeat center / cover;">
  			<u class="sticker"><?=$category_name[0]->name?></u>
  			<span class="text"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
  		</a>
      <?php
			$vtitle[1]='';
      }
      wp_reset_postdata();
    ?>
	</div>
</section>
<section class="home-main container">
	<div class="heading">
		<h6>Главное</h6>
	</div>
	<div class="flex">
    <div class="left">
    <?php
      $query = new WP_Query([
        'category__not_in' => [2,10,20,28,22],
        'cat' => array(25),
        'posts_per_page' => 7,
        'order'   => 'DESC',
        'post__not_in'   => $exclude_id,
      ]);
      $idx = 1;
      while($query->have_posts()) {
        $query->the_post();
        $exclude_id[] = get_the_ID(); // добавляем записи в исключение
        if($idx == 1){
          ?>
            <a href="<?php the_permalink(); ?>" class="default-link">
              <span class="img-wrapper">
                <img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
              </span>
							<?
							$title = get_the_title();
							if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
							elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
							else {$vtitle[0] = $title;}
							?>
              <span><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
            </a>
            </div>
          <div class="right">

          <?
        }
        else {
          ?>
					<div class="col col__news">
  					<a href="<?php the_permalink(); ?>" class="default-link">
							<img src="<?=kama_thumb_src('w=260 &h=155 &crop=center', get_field("img_intro", $post->ID))?>" alt="news_<?=$post->ID?>" />
							<div class="name"><?php the_title(); ?></div></a>
  				</div>
          <?
        }
      ?>
      <?php
        $idx++;
      }
      wp_reset_postdata();
    ?>

	</div>
	</div>
</section> 

<section class="default-slider gray">
	<div class="container">
		<div class="heading">
			<h6><?=get_cat_name(3241)?></h6>
			<a href="<?=get_category_link(3241)?>" class="more">Смотреть все</a>
		</div>
		<div class="owl-carousel">
      <?php // ТЕСТЫ
        $query = new WP_Query([
          'cat' => 3241,
          'posts_per_page' => 10,
          'order'   => 'DESC',
          'post__not_in'   => $exclude_id,
        ]);
        while($query->have_posts()) {
          $query->the_post();
					$title = get_the_title();
					if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
					elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
					else {$vtitle[0] = $title;}
        ?>
        <div class="item">
          <a href="<?php the_permalink(); ?>" class="default-link">
            <span class="img-wrapper">
              <img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
            </span>
            <span><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
          </a>
        </div>
        <?php
				$vtitle[1]='';
        }
        wp_reset_postdata();
      ?>
		</div>
	</div>
</section>

<section class="partition container">
	<div class="flex">
		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(3242)?></h6>
				<a href="<?=get_category_link(3242)?>" class="more">Смотреть все</a>
			</div>

      <?php // статьи
				$query = new WP_Query([
					'cat' => 3242,
					'posts_per_page' => 1,
					'order' => 'DESC',
          'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
						$title = get_the_title();
					if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
					elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
					else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php 
								if( count( $vtitle ) == 2 ) { echo '<br>'.$vtitle[1]; }
								else { echo '<br><br>'; }
							?></span>
						</a>
						<?
						$vtitle[1]='';
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
				  $idx++;
  			}
  			wp_reset_postdata();
  		?>
		</div>
		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(3243)?></h6>
				<a href="<?=get_category_link(3243)?>" class="more">Смотреть все</a>
			</div>

      <?php // интерв
				$query = new WP_Query([
					'cat' => 3243,
					'posts_per_page' => 1,
					'order' => 'DESC',
          'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
						$title = get_the_title();
					if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
					elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
					else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
						<?
						$vtitle[1]='';
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
				  $idx++;
  			}
  			wp_reset_postdata();
  		?>
		</div>
		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(3244)?></h6>
				<a href="<?=get_category_link(3244)?>" class="more">Смотреть все</a>
			</div>

      <?php // фото гал
				$query = new WP_Query([
					'cat' => 3244,
					'posts_per_page' => 1,
					'order' => 'DESC',
          'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
						$title = get_the_title();
					if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
					elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
					else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
						<?
						$vtitle[1]='';
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
				  $idx++;
  			}
  			wp_reset_postdata();
  		?>
		</div>
	</div>
</section>
<section class="default-slider gray">
	<div class="container">
		<div class="heading">
			<h6><?=get_cat_name(3245)?></h6>
			<a href="<?=get_category_link(3245)?>" class="more">Смотреть все</a>
		</div>
		<div class="owl-carousel">
      <?php // ТЕСТЫ
        $query = new WP_Query([
          'cat' => 3245,
          'posts_per_page' => 10,
          'order'   => 'DESC',
          'post__not_in'   => $exclude_id,
        ]);
        while($query->have_posts()) {
          $query->the_post();
					$title = get_the_title();
					if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
					elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
					else {$vtitle[0] = $title;}
        ?>
        <div class="item">
          <a href="<?php the_permalink(); ?>" class="default-link">
            <span class="img-wrapper">
              <img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
            </span>
            <span><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
          </a>
        </div>
        <?php
				$vtitle[1]='';
        }
        wp_reset_postdata();
      ?>
		</div>
	</div>
</section>
<section class="partition container">
	<div class="flex">
		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(3246)?></h6>
				<a href="<?=get_category_link(3246)?>" class="more">Смотреть все</a>
			</div>

      <?php // статьи
				$query = new WP_Query([
					'cat' => 3246,
					'posts_per_page' => 1,
					'order' => 'DESC',
          'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
						$title = get_the_title();
					if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
					elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
					else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php 
								if( count( $vtitle ) == 2 ) { echo '<br>'.$vtitle[1]; }
								else { echo '<br><br>'; }
							?></span>
						</a>
						<?
						$vtitle[1]='';
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
				  $idx++;
  			}
  			wp_reset_postdata();
  		?>
		</div>
		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(3247)?></h6>
				<a href="<?=get_category_link(3247)?>" class="more">Смотреть все</a>
			</div>

      <?php // интерв
				$query = new WP_Query([
					'cat' => 3247,
					'posts_per_page' => 1,
					'order' => 'DESC',
          'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
						$title = get_the_title();
					if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
					elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
					else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
						<?
						$vtitle[1]='';
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
				  $idx++;
  			}
  			wp_reset_postdata();
  		?>
		</div>
		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(3248)?></h6>
				<a href="<?=get_category_link(3248)?>" class="more">Смотреть все</a>
			</div>

      <?php // фото гал
				$query = new WP_Query([
					'cat' => 3248,
					'posts_per_page' => 1,
					'order' => 'DESC',
          'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
						$title = get_the_title();
					if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
					elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
					else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
						<?
						$vtitle[1]='';
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
				  $idx++;
  			}
  			wp_reset_postdata();
  		?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
<!-- <section class="default-slider gray">
	<div class="container">
		<div class="heading">
			<h6><?=get_cat_name(2082)?></h6>
			<a href="<?=get_category_link(2082)?>" class="more">Смотреть все</a>
		</div>
		<div class="owl-carousel">
				<?php // атомобили
					$query = new WP_Query([
						'cat' => 2082,
						'posts_per_page' => 10,
						'order'   => 'DESC',
            'post__not_in'   => $exclude_id,
					]);
					while($query->have_posts()) {
						$query->the_post();
						$title = get_the_title();
						if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
						elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
						else {$vtitle[0] = $title;}
					?>
					<div class="item">
						<a href="<?php the_permalink(); ?>" class="default-link">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
					</div>
				  <?php
					$vtitle[1]='';
  				}
  				wp_reset_postdata();
  			?>
		</div>
	</div>
</sectio -->

<!-- <section class="partition container wrap">
	<div class="flex">
		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(9)?></h6>
				<a href="<?=get_category_link(9)?>" class="more">Смотреть все</a>
			</div>

			<?php // МОТО
				$sticky = get_option( 'sticky_posts' );
				// $query = new WP_Query( [ 'post__in' => $sticky ] );

				$query = new WP_Query([
					'category_name' => 'moto',
					'posts_per_page' => 4,
					'order' => 'DESC',
          'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
						$title = get_the_title();
						if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
						elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
						else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
						<?
						$vtitle[1]='';
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
				  $idx++;
  			}
  			wp_reset_postdata();
  		?>
		</div>
		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(10)?></h6>
				<a href="<?=get_category_link(10)?>" class="more">Смотреть все</a>
			</div>

      <?php // КОМПАНИИ
				$query = new WP_Query([
					'cat' => 10,
					'posts_per_page' => 4,
					'order' => 'DESC',
          'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
						$title = get_the_title();
						if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
						elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
						else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
						<?
						$vtitle[1]='';
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
				  $idx++;
  			}
  			wp_reset_postdata();
  		?>
		</div>
		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(25)?></h6>
				<a href="<?=get_category_link(25)?>" class="more">Смотреть все</a>
			</div>

			<?php // Автопарк
				$query = new WP_Query([
					'cat' => 25,
					'posts_per_page' => 4,
					'order' => 'DESC',
					'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
						$title = get_the_title();
						if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
						elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
						else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
						<?
						$vtitle[1]='';
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
					$idx++;
				}
				wp_reset_postdata();
			?>
		</div>
	</div>
</section> -->
<div class="mobile_banners">
<!-- Yandex.RTB R-A-53448-6 -->
<div id="yandex_rtb_R-A-53448-6"></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-53448-6",
                renderTo: "yandex_rtb_R-A-53448-6",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script>
</div>
<!-- <section class="partners-slider gray">
  <div class="container-fluid">
    <div class="owl-carousel">
      <?php
        $cat = get_the_category($post->ID);

        $query = new WP_Query([
          'post_type' => 'brands',
          'posts_per_page' => -1,
					'orderby' => 'name',
          'order' => 'ASC',
        ]);
        while($query->have_posts()) {
          $query->the_post();
        ?>
        <div class="item">
          <a href="<?php the_permalink(); ?>" class="wrapper">
            <img src="<?=kama_thumb_src('h=55 &crop=0', get_the_post_thumbnail_url())?>" alt="alt">
          </a>
        </div>
        <?php
        }
        wp_reset_postdata();
      ?>
    </div>
  </div>
					
</section> -->
<!-- <main class="container">
			<div class="flex">
			<div class="left-col">
			<div class="section-cards-wrapper">
				<br>
					<div class="flex">
					<?php
				        $cats = get_the_category($post->ID);
						$page = get_query_var( 'page' );
				       $wp_query = new WP_Query([
				          'post_type' => 'brands',
				        'posts_per_page' => 5,
				        'paged' => ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1,
						'orderby' => 'name',
				          'order' => 'ASC',
				        ]);

					

						while($wp_query->have_posts()) {
							$wp_query->the_post();
						?>
						<a href="<?php the_permalink(); ?>" class="default-link">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro"))?>" alt="<?php the_title(); ?>">
							</span>
							<span><?php the_title(); ?></span>
						</a>
						<?php
						}
						// wp_reset_postdata();
                     // ajax загрузка
					if ($wp_query->max_num_pages > 1) : ?>
							<?php 
							$currpage = get_query_var('paged') ? get_query_var('paged') : 1;
							$lastpage = $wp_query->max_num_pages;
						?>

						<nav class="paging">
							<ul class="paging-links">
								<li class="paging-border">
									<?php echo $currpage == 1 ? 'Первая' : "<a href='". pg_url(1) ."'>Первая</a>" ?>
								</li>
								<li class="paging-numbers">
									<span class="prev"><?php echo $currpage > 1 ? "<a href='". pg_url( $currpage - 1 ) . "'>&#x2039;</a>" : '&#x2039;' ?></span>
									<span class="position">
										<?php echo $currpage ?>&nbsp;из&nbsp;<?php echo $lastpage ?>
									</span>
									<span class="next"><?php echo $currpage < $lastpage ? "<a href='". pg_url( $currpage + 1 )  ."'>&#x203A;</a>" : '&#x203A;' ?></span>
								</li>
								<li class="paging-border">
									<?php echo $currpage == $lastpage ? 'Последняя' : "<a href='". pg_url( $lastpage )  ."'>Последняя</a>" ?>
								</li>
							</ul>
						</nav>
						
					<?php 




					 ?>
						</div>
					<?php
					endif;
						wp_reset_query();
					?>


					</div>

			</div>
			</div>
			</div>
</main>
 -->





<?php get_footer(); ?>