<?php get_header(); ?>


<main class="container">
	<div class="flex">
		<div class="left-col">
			<div class="archives-col-wrapper section-cards-wrapper">
				<h1>Архив номеров журнала «<?php the_archive_title(); ?>»</h1>
				<form id="filterform">
					<div class="select-filter">
						<select id="selectpage" name="sel_page">
							<option value="/archives/5koleso/" selected>5 колесо</option>
							<option value="/archives/autopark/">Автопарк</option>
						</select>
						<div class="year">
							<p>Год выпуска</p>
							<select id="selectyear" name="sel_year">
								<?php for( $year = (int) date('Y'); $year >= 2012; $year -- ) { ?>
									<option <?php echo $year == date('Y') ? ' selected ' : '' ?> value="<?php echo $year ?>"><?php echo $year ?></option>
								<?php } ?>
 							</select>
						</div>
					</div>
				</form>

				<div class="flex-archive-cards">
					<?php
						$postspage = get_query_var('paged') ? get_query_var('paged') : 1;
						$categories = get_the_category();
						$query = new WP_Query([
							'cat'   => $categories[0]->cat_ID,
							'posts_per_page' => 12,
							'offset'		=> ( $postspage - 1 ) * 12,
							'order'   => 'DESC',
						]);
 
						while($query->have_posts()) {
							$query->the_post();
						?>
						<a href="<?php the_permalink(); ?>" class="archive-card">
							<span class="name"><?php the_title(); ?></span>
							<img src="<?=kama_thumb_src('w=280 &h=365 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
						</a>
					  <?php
					}
					wp_reset_postdata();

					// ajax загрузка
					if ($query->max_num_pages > 1) : ?>
							<?php 
							$currpage = get_query_var('paged') ? get_query_var('paged') : 1;
							$lastpage = $query->max_num_pages;
						?>
						
						<script>
							var true_posts = '<?php echo serialize($query->query_vars); ?>';
						</script>

						<nav class="paging">
							<ul class="paging-links">
								<li class="paging-border">
									<?php echo $currpage == 1 ? 'Первая' : "<a href='". pg_url(1) ."'>Первая</a>" ?>
								</li>
								<li class="paging-numbers">
									<span class="prev"><?php echo $currpage > 1 ? "<a href='". pg_url( $currpage - 1 ) . "'>&#x2039;</a>" : '&#x2039;' ?></span>
									<span class="position">
										<?php echo $currpage ?>&nbsp;из&nbsp;<?php echo $lastpage ?>
									</span>
									<span class="next"><?php echo $currpage < $lastpage ? "<a href='". pg_url( $currpage + 1 )  ."'>&#x203A;</a>" : '&#x203A;' ?></span>
								</li>
								<li class="paging-border">
									<?php echo $currpage == $lastpage ? 'Последняя' : "<a href='". pg_url( $lastpage )  ."'>Последняя</a>" ?>
								</li>
							</ul>
						</nav>
						
					<?php
					endif;
				?>
				</div>
				<!-- Yandex.RTB R-A-53448-1 -->
<div id="yandex_rtb_R-A-53448-1"></div>
<script type="text/javascript">
		(function(w, d, n, s, t) {
			 w[n] = w[n] || [];
			 w[n].push(function() {
					 Ya.Context.AdvManager.render({
							 blockId: "R-A-53448-1",
							 renderTo: "yandex_rtb_R-A-53448-1",
							 async: true
					 });
			 });
			 t = d.getElementsByTagName("script")[0];
			 s = d.createElement("script");
			 s.type = "text/javascript";
			 s.src = "//an.yandex.ru/system/context.js";
			 s.async = true;
			 t.parentNode.insertBefore(s, t);
	 })(this, this.document, "yandexContextAsyncCallbacks");
</script>

				<!-- Yandex.RTB R-A-53448-5 -->
				<div id="yandex_rtb_R-A-53448-5"></div>
				<script type="text/javascript">
						(function(w, d, n, s, t) {
							 w[n] = w[n] || [];
							 w[n].push(function() {
									 Ya.Context.AdvManager.render({
											 blockId: "R-A-53448-5",
											 renderTo: "yandex_rtb_R-A-53448-5",
											 async: true
									 });
							 });
							 t = d.getElementsByTagName("script")[0];
							 s = d.createElement("script");
							 s.type = "text/javascript";
							 s.src = "//an.yandex.ru/system/context.js";
							 s.async = true;
							 t.parentNode.insertBefore(s, t);
					 })(this, this.document, "yandexContextAsyncCallbacks");
				</script>
			</div>
		</div>
		<div class="right-col">
			<?php
				if (function_exists('dynamic_sidebar')){
					dynamic_sidebar('right_column');
				}
				if (function_exists('dynamic_sidebar')){
					dynamic_sidebar('right_column_inner');
				}
			?>
		</div>
	</div>
</main>

<section class="default-slider gray">
  <div class="container">
    <div class="heading">
      <h6>Популярное</h6>
    </div>
    <div class="owl-carousel">
      <?php popular_carousel( $post->ID ) ?>
    </div>
  </div>
</section>
<div class="mobile_banners">
<!-- Yandex.RTB R-A-53448-6 -->
<div id="yandex_rtb_R-A-53448-6"></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-53448-6",
                renderTo: "yandex_rtb_R-A-53448-6",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script>
</div>

<section class="partners-slider">
	<div class="container-fluid">
		<div class="owl-carousel">
			<?php
				$cat = get_the_category($post->ID);

				$query = new WP_Query([
					'post_type' => 'brands',
					'posts_per_page' => -1,
					'orderby' => 'name',
					'order' => 'ASC',
				]);
				while($query->have_posts()) {
					$query->the_post();
				?>
				<div class="item">
					<a href="<?php the_permalink(); ?>" class="wrapper">
						<img src="<?=kama_thumb_src('h=55 &crop=0', get_the_post_thumbnail_url())?>" alt="alt">
					</a>
				</div>
				<?php
				}
				wp_reset_postdata();
			?>
		</div>
	</div>
	<div class="container">
		<div class="all_brands_box"><a href="/brands/" class="all_brands_link">Все бренды</div>
	</div>
</section>

<?php get_footer(); ?>
