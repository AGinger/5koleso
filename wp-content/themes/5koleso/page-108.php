<?php get_header();?>

	<?php

$page_title = 'Автомобильный журнал';

if (function_exists('dynamic_sidebar')) {
    dynamic_sidebar('after_header_onmain');
}
?>
<section class="tests container">
	<div class="flex">
		<div class="left">
      <?php
$exclude_id = [];
$cache_main_post_key = 'main_post_block';
$cache_main_post = wp_cache_get( $cache_main_post_key);
$group = 'default';
$expire = 3600;

if( false !== $cache_main_post ){
    $query = $cache_main_post;

}else{
    $index_post = get_field('index_post');
    if ($index_post) {
        // если задан главный пост вручную
        $index_category = get_the_category($index_post[0]);
        $query = new WP_Query([
            'p' => $index_post[0],
            'posts_per_page' => 1,
        ]);
    } else {
        $query = new WP_Query([
            'cat' => 3, // тесты
            'posts_per_page' => 1,
            'order' => 'DESC',
        ]);
    }
    wp_cache_set( $cache_main_post_key, $query, $group, $expire  ); // добавим данные в кэш
}
// Главный пост


while ($query->have_posts()) {
    $query->the_post();

    // категория
    $category_object = get_the_category(get_the_ID());
    $category_name = $category_object[0]->name;

    $exclude_id[] = get_the_ID(); // добавляем записи в исключение?>

        <a href="<?php the_permalink(); ?>" class="card" style='padding: 0'>
        		<img src="<?php echo kama_thumb_src('w=875 &h=440 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>"  style="position: absolute; width: 100%; height: 100%; object-fit: cover;">
      			<div class="card" style="margin: 0">
	  				<u class="sticker"><?php echo $index_post ? $index_category[0]->name : $category_name; ?></u>
	  				<div class="text">
	  					<div class="h2"><?php the_title(); ?></div>
	  				</div>
  				</div>
  			</a>
			  <?php
}

wp_reset_postdata();
?>

			<div class="heading">
				<div class="h6"><?php echo get_cat_name(2); ?></div>
				<a href="<?php echo get_category_link(2); ?>" class="more">Смотреть все</a>
			</div>
			<div class="flex-child">
        <?php
$cache_main_news_key = 'main_news_block';
$cache_main_news = wp_cache_get( $cache_main_news_key);

if( false !== $cache_main_news ){
    $query = $cache_main_news;

}else{
    $group = '0';
$expire = 3600;
  $query = new WP_Query([
    'cat' => 2,
    'posts_per_page' => 6,
    'order' => 'DESC',
]);
    wp_cache_set( $cache_main_news_key, $query,$group,$expire); // добавим данные в кэш
}
while ($query->have_posts()) {
    $query->the_post();
    $exclude_id[] = get_the_ID(); // добавляем записи в исключение?>
					<div class="col col__news">
  					<a href="<?php the_permalink(); ?>" class="default-link">
							<img src="<?php echo kama_thumb_src('w=260 &h=155 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>" />
							<div class="name"><?php the_title(); ?></div></a>
  				</div>
				  <?php
}
wp_reset_postdata();
?>
			</div>
		</div>
		<div class="right">
			<?php
if (function_exists('dynamic_sidebar')) {
    dynamic_sidebar('right_column_main');
}
?>
		</div>
	</div>
</section>
<div class="container main_h1"><h1>Автомобильный журнал</h1></div>
<section class="section-links container">
	<div class="flex">
    <?php
$index_relevant = get_field('index_relevant');
$cache_autoblock_key = 'autoblock_block';
$cache_autoblock = wp_cache_get( $cache_autoblock_key);

if( false !== $cache_autoblock ){
    $query = $cache_autoblock;

}else{
    $group = '012';
    $expire = 3600;
    $query = new WP_Query([
        'post_type' => 'post',
        'post__in' => $index_relevant,
        'posts_per_page' => 3,
        'orderby' => 'post__in',
        'post_status' => 'publish',
    ]);
    wp_cache_set( $cache_autoblock_key, $query, $group, $expire); // добавим данные в кэш
}

while ($query->have_posts()) {
    $query->the_post();
    $exclude_id[] = get_the_ID(); // добавляем записи в исключение
    if ($post->ID == 89502) {
        continue;
    }

    $category_name = get_the_category(get_the_ID());
    $title = get_the_title();
    if (strpos($title, ':')) {
        $vtitle = explode(':', $title);
        $vtitle[0] .= ':';
    } elseif (strpos($title, '.')) {
        $vtitle = explode('.', $title);
        $vtitle[0] .= '.';
    } else {
        $vtitle[0] = $title;
    } ?>
      <a href="<?php the_permalink(); ?>" class="card" style='padding: 0;'>
      		<img src="<?php echo kama_thumb_src('w=875 &h=440 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>"  style="position: absolute; width: 100%; height: 100%; object-fit: cover;">
      		<div class="card" style="margin: 0">
	  			<u class="sticker"><?php echo $category_name[0]->name; ?></u>
	  			<span class="text"><strong><?php echo $vtitle[0]; ?></strong> <?php if (count($vtitle) == 2) {
        echo '<br>' . $vtitle[1];
    } ?></span>
  			</div>
  		</a>
      <?php
$vtitle[1] = '';
}
wp_reset_postdata();
?>
	</div>
</section>
<section class="home-main container">
	<div class="heading">
		<div class="h6">Главное</div>
	</div>
	<div class="flex">
    <div class="left">
    <?php
    $cache_key = 'main_block';
	$cache = wp_cache_get( $cache_key );

	if( false !== $cache ){
        $query = $cache;
    }else{
        $expire = 3600;
        $query = new WP_Query([
            'cat' => array(-3240, -2, 1712, 15, 3, 25, 1442, 1572),
            //  25, 2, 10, 20, 28, 22
            // 'category__not_in' => [25, 2, 10, 20, 28, 22],
            'posts_per_page' => 7,
            'order' => 'DESC',
            //$exclude_id
            'post__not_in' => $exclude_id,
        ]);
        wp_cache_set( $cache_key, $query,$group,$expire ); // добавим данные в кэш
    }


$idx = 1;
while ($query->have_posts()) {
    $query->the_post();
    $exclude_id[] = get_the_ID(); // добавляем записи в исключение
    if ($idx == 1) {
        ?>
            <a href="<?php the_permalink(); ?>" class="default-link">
              <span class="img-wrapper">
                <img src="<?php echo kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>">
              </span>
							<?php
$title = get_the_title();
        if (strpos($title, ':')) {
            $vtitle = explode(':', $title);
            $vtitle[0] .= ':';
        } elseif (strpos($title, '.')) {
            $vtitle = explode('.', $title);
            $vtitle[0] .= '.';
        } else {
            $vtitle[0] = $title;
        } ?>
              <span><strong><?php echo $vtitle[0]; ?></strong> <?php if (count($vtitle) == 2) {
            echo '<br>' . $vtitle[1];
        } ?></span>
            </a>
            </div>
          <div class="right">
          <?php
    } else {
        ?>
					<div class="col col__news">
  					<a href="<?php the_permalink(); ?>" class="default-link">
							<img src="<?php echo kama_thumb_src('w=260 &h=155 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>" />
							<div class="name"><?php the_title(); ?></div></a>
  				</div>
          <?php
    } ?>
      <?php
$idx++;
}
wp_reset_postdata();
?>
    </div>
	</div>
</section>

<section class="default-slider gray">
	<div class="container">
		<div class="heading">
			<div class="h6"><?php echo get_cat_name(3); ?></div>
			<a href="<?php echo get_category_link(3); ?>" class="more">Смотреть все</a>
		</div>
		<div class="owl-carousel">
      <?php // ТЕСТЫ
$cache_test_slider_key = 'test_slider_block';
$cache_test_slider = wp_cache_get( $cache_test_slider_key);

if( false !== $cache_test_slider ){
    $query = $cache_test_slider;
}else{
    $expire = 3600;
  $query = new WP_Query([
    'cat' => 3,
    'posts_per_page' => 10,
    'order' => 'DESC',
    'post__not_in' => $exclude_id,
]);
    wp_cache_set( $cache_test_slider_key, $query,$group,$expire ); // добавим данные в кэш
}
while ($query->have_posts()) {
    $query->the_post();
    $title = get_the_title();
    if (strpos($title, ':')) {
        $vtitle = explode(':', $title);
        $vtitle[0] .= ':';
    } elseif (strpos($title, '.')) {
        $vtitle = explode('.', $title);
        $vtitle[0] .= '.';
    } else {
        $vtitle[0] = $title;
    } ?>
        <div class="item">
          <a href="<?php the_permalink(); ?>" class="default-link">
            <span class="img-wrapper">
              <img src="<?php echo kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>">
            </span>
            <span><strong><?php echo $vtitle[0]; ?></strong> <?php if (count($vtitle) == 2) {
        echo '<br>' . $vtitle[1];
    } ?></span>
          </a>
        </div>
        <?php
$vtitle[1] = '';
}
wp_reset_postdata();
?>
		</div>
	</div>
</section>

<section class="partition container">
	<div class="flex">
		<div class="col">
			<div class="heading">
				<div class="h6"><?php echo get_cat_name(5); ?></div>
				<a href="<?php echo get_category_link(5); ?>" class="more">Смотреть все</a>
			</div>

      <?php // ОТЗЫВЫ
$cache_otzuv_key = 'otzuv_block';
$cache_otzuv = wp_cache_get( $cache_otzuv_key);

if( false !== $cache_otzuv ){
    $query = $cache_otzuv;
}else{
    $expire = 3600;
  $query = new WP_Query([
    'cat' => 5,
    'posts_per_page' => 4,
    'order' => 'DESC',
    'post__not_in' => $exclude_id,
]);
    wp_cache_set( $cache_otzuv_key, $query,$group,$expire ); // добавим данные в кэш
}
$idx = 0;
while ($query->have_posts()) {
    $query->the_post();

    if ($idx == 0) {
        $title = get_the_title();
        if (strpos($title, ':')) {
            $vtitle = explode(':', $title);
            $vtitle[0] .= ':';
        } elseif (strpos($title, '.')) {
            $vtitle = explode('.', $title);
            $vtitle[0] .= '.';
        } else {
            $vtitle[0] = $title;
        } ?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?php echo kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>">
							</span>
							<span class="name"><strong><?php echo $vtitle[0]; ?></strong> <?php if (count($vtitle) == 2) {
            echo '<br>' . $vtitle[1];
        } ?></span>
						</a>
						<?php
$vtitle[1] = '';
    } else {
        ?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?php
    }
    $idx++;
}
wp_reset_postdata();
?>
		</div>
		<div class="col">
			<div class="heading">
				<div class="h6"><?php echo get_cat_name(7); ?></div>
				<a href="<?php echo get_category_link(7); ?>" class="more">Смотреть все</a>
			</div>

      <?php // ШИНЫ
$query = new WP_Query([
    'cat' => 7,
    'posts_per_page' => 4,
    'order' => 'DESC',
    'post__not_in' => $exclude_id,
]);
$idx = 0;
while ($query->have_posts()) {
    $query->the_post();

    if ($idx == 0) {
        $title = get_the_title();
        if (strpos($title, ':')) {
            $vtitle = explode(':', $title);
            $vtitle[0] .= ':';
        } elseif (strpos($title, '.')) {
            $vtitle = explode('.', $title);
            $vtitle[0] .= '.';
        } else {
            $vtitle[0] = $title;
        } ?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?php echo kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>">
							</span>
							<span class="name"><strong><?php echo $vtitle[0]; ?></strong> <?php if (count($vtitle) == 2) {
            echo '<br>' . $vtitle[1];
        } ?></span>
						</a>
						<?php
$vtitle[1] = '';
    } else {
        ?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?php
    }
    $idx++;
}
wp_reset_postdata();
?>
		</div>
		<div class="col">
			<div class="heading">
				<div class="h6"><?php echo get_cat_name(8); ?></div>
				<a href="<?php echo get_category_link(8); ?>" class="more">Смотреть все</a>
			</div>

      <?php // ГАРАЖ
$query = new WP_Query([
    'cat' => 8,
    'posts_per_page' => 4,
    'order' => 'DESC',
    'post__not_in' => $exclude_id,
]);
$idx = 0;
while ($query->have_posts()) {
    $query->the_post();

    if ($idx == 0) {
        $title = get_the_title();
        if (strpos($title, ':')) {
            $vtitle = explode(':', $title);
            $vtitle[0] .= ':';
        } elseif (strpos($title, '.')) {
            $vtitle = explode('.', $title);
            $vtitle[0] .= '.';
        } else {
            $vtitle[0] = $title;
        } ?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?php echo kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>">
							</span>
							<span class="name"><strong><?php echo $vtitle[0]; ?></strong> <?php if (count($vtitle) == 2) {
            echo '<br>' . $vtitle[1];
        } ?></span>
						</a>
						<?php
$vtitle[1] = '';
    } else {
        ?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?php
    }
    $idx++;
}
wp_reset_postdata();
?>
		</div>
	</div>
</section>

<section class="default-slider gray">
	<div class="container">
		<div class="heading">
			<div class="h6"><?php echo get_cat_name(1572); ?></div>
			<a href="<?php echo get_category_link(1572); ?>" class="more">Смотреть все</a>
		</div>
		<div class="owl-carousel">
				<?php // НОВИНКИ
$query = new WP_Query([
    'cat' => 1572,
    'posts_per_page' => 10,
    'order' => 'DESC',
    'post__not_in' => $exclude_id,
]);
while ($query->have_posts()) {
    $query->the_post();
    $title = get_the_title();
    if (strpos($title, ':')) {
        $vtitle = explode(':', $title);
        $vtitle[0] .= ':';
    } elseif (strpos($title, '.')) {
        $vtitle = explode('.', $title);
        $vtitle[0] .= '.';
    } else {
        $vtitle[0] = $title;
    } ?>
					<div class="item">
						<a href="<?php the_permalink(); ?>" class="default-link">
							<span class="img-wrapper">
								<img src="<?php echo kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>">
							</span>
							<span><strong><?php echo $vtitle[0]; ?></strong> <?php if (count($vtitle) == 2) {
        echo '<br>' . $vtitle[1];
    } ?></span>
						</a>
					</div>
				  <?php
$vtitle[1] = '';
}
wp_reset_postdata();
?>
		</div>
	</div>
</section>

<section class="partition container wrap">
	<div class="flex">
		<div class="col">
			<div class="heading">
				<div class="h6"><?php echo get_cat_name(9); ?></div>
				<a href="<?php echo get_category_link(9); ?>" class="more">Смотреть все</a>
			</div>

			<?php // МОТО
$sticky = get_option('sticky_posts');
// $query = new WP_Query( [ 'post__in' => $sticky ] );

$query = new WP_Query([
    'category_name' => 'moto',
    'posts_per_page' => 4,
    'order' => 'DESC',
    'post__not_in' => $exclude_id,
]);
$idx = 0;
while ($query->have_posts()) {
    $query->the_post();

    if ($idx == 0) {
        $title = get_the_title();
        if (strpos($title, ':')) {
            $vtitle = explode(':', $title);
            $vtitle[0] .= ':';
        } elseif (strpos($title, '.')) {
            $vtitle = explode('.', $title);
            $vtitle[0] .= '.';
        } else {
            $vtitle[0] = $title;
        } ?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?php echo kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>">
							</span>
							<span class="name"><strong><?php echo $vtitle[0]; ?></strong> <?php if (count($vtitle) == 2) {
            echo '<br>' . $vtitle[1];
        } ?></span>
						</a>
						<?php
$vtitle[1] = '';
    } else {
        ?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?php
    }
    $idx++;
}
wp_reset_postdata();
?>
		</div>
		<div class="col">
			<div class="heading">
				<div class="h6"><?php echo get_cat_name(10); ?></div>
				<a href="<?php echo get_category_link(10); ?>" class="more">Смотреть все</a>
			</div>

      <?php // КОМПАНИИ
$query = new WP_Query([
    'cat' => array(3240, 10),
    'posts_per_page' => 4,
    'order' => 'DESC',
    'post__not_in' => $exclude_id,
]);
$idx = 0;
while ($query->have_posts()) {
    $query->the_post();

    if ($idx == 0) {
        $title = get_the_title();
        if (strpos($title, ':')) {
            $vtitle = explode(':', $title);
            $vtitle[0] .= ':';
        } elseif (strpos($title, '.')) {
            $vtitle = explode('.', $title);
            $vtitle[0] .= '.';
        } else {
            $vtitle[0] = $title;
        } ?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?php echo kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>">
							</span>
							<span class="name"><strong><?php echo $vtitle[0]; ?></strong> <?php if (count($vtitle) == 2) {
            echo '<br>' . $vtitle[1];
        } ?></span>
						</a>
						<?php
$vtitle[1] = '';
    } else {
        ?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?php
    }
    $idx++;
}
wp_reset_postdata();
?>
		</div>
		<div class="col">
			<div class="heading">
				<div class="h6"><?php echo get_cat_name(25); ?></div>
				<a href="<?php echo get_category_link(25); ?>" class="more">Смотреть все</a>
			</div>

			<?php // Автопарк
$query = new WP_Query([
    'cat' => array(25, -3240),
    'category__not_in' => 3240,
    'posts_per_page' => 4,
    'order' => 'DESC',
    'post__not_in' => $exclude_id,
]);
$idx = 0;
while ($query->have_posts()) {
    $query->the_post();

    if ($idx == 0) {
        $title = get_the_title();
        if (strpos($title, ':')) {
            $vtitle = explode(':', $title);
            $vtitle[0] .= ':';
        } elseif (strpos($title, '.')) {
            $vtitle = explode('.', $title);
            $vtitle[0] .= '.';
        } else {
            $vtitle[0] = $title;
        } ?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?php echo kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>">
							</span>
							<span class="name"><strong><?php echo $vtitle[0]; ?></strong> <?php if (count($vtitle) == 2) {
            echo '<br>' . $vtitle[1];
        } ?></span>
						</a>
						<?php
$vtitle[1] = '';
    } else {
        ?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?php
    }
    $idx++;
}
wp_reset_postdata();
?>
		</div>
	</div>
</section>
<div class="mobile_banners">
	<?php
if (function_exists('dynamic_sidebar')) {
    dynamic_sidebar('above_footer_mob');
}
?>
</div>
<section class="partners-slider gray">
  <div class="container-fluid">
    <div class="owl-carousel">
      <?php
$cat = get_the_category($post->ID);

$query = new WP_Query([
    'post_type' => 'brands',
    'posts_per_page' => -1,
    'orderby' => 'name',
    'order' => 'ASC',
]);
while ($query->have_posts()) {
    $query->the_post(); ?>
        <div class="item">
          <a href="<?php the_permalink(); ?>" class="wrapper">
            <img src="<?php echo kama_thumb_src('h=55 &crop=0', get_the_post_thumbnail_url()); ?>" alt="<?php echo $page_title; ?>">
          </a>
        </div>
        <?php
}
wp_reset_postdata();
?>
    </div>
  </div>

</section>

<?php get_footer();?>
