<?php

$currpost = $post;
$GLOBALS['currpost'] = $currpost;
if (is_front_page() || is_home()) {
    $page_title = 'Автомобильный журнал';
} elseif (is_search()) {
    $page_title = 'Результат поиска по «' . htmlspecialchars($_GET['s']) . '»';
} elseif (is_404()) {
    $page_title = '...';
} elseif (is_archive()) {
    $page_title = get_the_archive_title();
} else {
    $page_title = get_the_title($currpost->ID);
}

?>
<!DOCTYPE html>
<html lang="ru">
<head>
	
	<base href="<?=get_site_url();?>">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="google-site-verification" content="RbVh-22tPjlBGFUNY10XFqCLKSngvuewJmYTefmdu2c" />
	<link rel="apple-touch-icon" sizes="180x180" href="//5koleso.ru/favicon180.png" />
	<link rel="apple-touch-icon" sizes="192x192" href="//5koleso.ru/favicon192.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="//5koleso.ru/favicon76.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="//5koleso.ru/favicon120.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="//5koleso.ru/favicon152.png" />
	<meta name="pmail-verification" content="9d60719eb891d6d7a1fa1d926dfa35de">
	<link rel="icon" type="image/x-icon" href="//5koleso.ru/favicon16.ico" sizes="16x16"/>
	<link rel="shortcut icon" href="//5koleso.ru/favicon48.ico" sizes="48x48" type="image/x-icon">
	<link rel="stylesheet" href="<?=get_template_directory_uri();?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=get_template_directory_uri();?>/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?=get_template_directory_uri();?>/css/style.css?v=as58">
<?php if ($_SERVER['REQUEST_URI'] == '/vote/golosovanie-za-pilotov-rds-2021/'){ ?>
	<link rel="stylesheet" href="/css/reset.css">
	<link rel="stylesheet" href="/css/style.css">
<? } ?>

	<title><?php echo wp_title()."Автомобильный портал 5 Колесо"; ?></title>
	<?php if (is_singular()) {
    wp_enqueue_script('comment-reply');
}
?>
	<?php wp_head();?>



	<!-- Global site tag (gtag.js) - Google Analytics -->
	<?if (!is_front_page()){?>
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
		<?}?>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-63976849-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-63976849-2');

 setTimeout(function(){
     gtag('event', location.pathname, {
     'event_category': 'Посещение без отказа',
  });
 }, 5000);
</script>
	<script src="//yastatic.net/pcode/adfox/loader.js" crossorigin="anonymous"></script>
 <?php include 'robots_exclude.php';?>
 <script>
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
   })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

   ga('create', 'UA-63976849-2', 'auto');
   ga('send', 'pageview');
 </script>
  <!-- Rating Mail.ru counter -->
<script type="text/javascript">
var _tmr = window._tmr || (window._tmr = []);
_tmr.push({id: "364798", type: "pageView", start: (new Date()).getTime()});
(function (d, w, id) {
 if (d.getElementById(id)) return;
 var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
 ts.src = "https://top-fwz1.mail.ru/js/code.js";
 var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
 if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window, "topmailru-code");
</script>
<noscript><div>
<img src="https://top-fwz1.mail.ru/counter?id=364798;js=na" style="border:0;position:absolute;left:-9999px;" alt="Top.Mail.Ru" />
</div></noscript>
 <?php if ($_SERVER['REQUEST_URI'] == '/') {?>
 <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "url": "https://5koleso.ru/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://5koleso.ru/?s={search_term}",
    "query-input": "required name=search_term" }
}
</script>
<script>

	function setFocus()
{
     document.getElementById("s").focus();
}
</script>
<?php }?>
<?if (!is_front_page()){?>

<?}?>
<script>window.yaContextCb = window.yaContextCb || []</script>
<script src="https://yandex.ru/ads/system/context.js" async></script>
</head>
<body
<?php if ($_SERVER['REQUEST_URI'] == '/index-test/') {
    echo ' class="test"';
}
?>>
<style>
@media (max-width: 767px){}
header.container.topWindow {
    padding-top: 3px;
    padding-bottom: 0px;
    box-shadow: 0 5px 10px -4px rgb(0 0 0 / 20%);
    top: 0!important;
}
}
</style>
<!-- <?php body_class();?> -->

<div class="desktop_banners">
<?php
if (function_exists('dynamic_sidebar')) {
    dynamic_sidebar('before_header_desk');
}
?>
</div>
<div class="mobile_banners">
	<?php
if (function_exists('dynamic_sidebar')) {
    dynamic_sidebar('before_header_mob');
}
?>
</div>
<?php
if (function_exists('dynamic_sidebar')) {
    dynamic_sidebar('before_header');
}
?>
<header class="container">
	<div class="text-center logo">
		<button class="toggle-menu">&times;</button>
		<div class="logo-block">
			<a href="/">
				<img src="<?=get_template_directory_uri();?>/img/logo5.jpg" alt="<?php echo $page_title; ?>">
			</a>
			<a href="/avtopark-page/">
				<img class="ap-logo" src="<?=get_template_directory_uri();?>/img/logo-park.jpg" alt="<?php echo $page_title; ?>">
			</a>
		</div>
		<div class="search_button"></div>
		<div class="form_search">
			<form role="search" method="get" id="searchform" action="<?php echo home_url('/'); ?>" >
				<div class="form-wrapper" onload="setFocus()">
					<input type="text" name="s" id="s" value="<?php echo get_search_query(); ?>" placeholder="Введите запрос" required>
					<button id="searchsubmit"></button>
				</div>
			</form>
			<div class="bg_search"></div>
		</div>
	</div>
</header>
	<div class="topmenu-container">
		<div class="container">
		<button class="topmenu-close">&times;</button>
	<?php
$topmenu = wp_nav_menu([
    'echo' => false,
    'theme_location' => 'top',
    'container' => false,
    'menu_class' => 'primary-menu',
]);
$topmenu = str_replace("primary-menu", "primary-menu owl-carousel", $topmenu);
$topmenu = str_replace("...", $page_title, $topmenu);
echo $topmenu;
?>
</div>
</div>

<style>
		i {
    border: solid black;
    border-width: 0 3px 3px 0;
    display: inline-block;
    padding: 3px;
}

.right-sub {
    transform: rotate(-45deg);
    -webkit-transform: rotate(-45deg);
}

.left-sub {
    transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
}

.up-sub {
    transform: rotate(-135deg);
    -webkit-transform: rotate(-135deg);
}

.down-sub {
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
}

.sab-menus-iten{
    text-align: left;
    display: none;
    float: right;
    overflow: hidden;
    margin-right: 0;
    width: 1.7em;
    padding-left: 0.5em;
}




@media (max-width: 400px) {

	.sab-menus-iten{
		display: inline-block;
	}

	.primary-menu li ul.sub-menu {
		max-height: 0;
		padding-top: 0;
		padding-bottom: 0;
		margin: 0;
		border-bottom: 0;
		overflow: hidden;
		transition: .7s all;
	}

	.primary-menu li ul.sub-menu.active {
		max-height: initial;
		padding-top: 10px;
	    margin-bottom: 10px
	}

	.sab-menus-iten {
	    text-align: left;
	    float: right;
	    display: block;
	}
}

@media screen and (max-width: 374px) {
	.sub-menu li:first-child a,
	.sub-menu li:last-child a,
	.sub-menu li a {
		font-size: 1.1em;
	}
}

</style>

<?
if (function_exists('dynamic_sidebar')) {
    dynamic_sidebar('after_header');
}
?>