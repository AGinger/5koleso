<?php get_header(); ?>
<?php $currpost = $post; ?>
	<main class="container">
		<div class="flex">
			<div class="left-col">
				<div class="section-cards-wrapper">
					<ul class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
						<? bcn_display() ?>
					</ul>
					<h1><?php the_title(); ?></h1>

					<div class="flex">
					<?php
						$categories = get_the_category();
						$wp_query = new WP_Query([
							// 'title' => 'bmw',
							// 'post_type' => 'post',
							// 'cat'   => $categories[0]->cat_ID,
							'posts_per_page' => 10,
							'offset' => 10 * ( isset( $_GET['pg'] ) ? $_GET['pg'] - 1 : 0 ),
							'order' => 'DESC',
							'meta_key' => 'car_brand',
				        	'paged' => ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1,
  							'meta_value' => get_the_id(),
						]);

						


							while($wp_query->have_posts()) {
								$wp_query->the_post();
							?>
							<a href="<?php the_permalink(); ?>" class="default-link">
								<span class="img-wrapper">
									<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro"))?>" alt="<?php echo get_the_title( $currpost->ID ); ?>">
								</span>
								<span><?php the_title(); ?></span>
							</a>
							<?php
							}
						 
						// wp_reset_postdata();
                          // ajax загрузка
					if ( $wp_query->max_num_pages > 1) : ?>

						<?php 
							$currpage = isset( $_GET['pg'] ) ? $_GET['pg'] : 1;
							$lastpage = $wp_query->max_num_pages;
							$burl = preg_replace( '@\?.+$@', '', $_SERVER['REQUEST_URI'] );
						?>

						<nav class="paging">
							<ul class="paging-links">
								<li class="paging-border">
									<?php echo "<a   class='loadpage' data-page='1' href='". $burl ."'>Первая</a>" ?>
								</li>
								<li class="paging-numbers">
									<span class="prev"><?php echo   "<a class='loadpage' data-page='". ( $currpage > 1 ? $currpage - 1 : 1) . "' href='" . $burl . ( $currpage > 2 ? "?pg=". ( $currpage - 1 ) : ''  ) ."'>&#x2039;</a>" ?></span>
									<span class="position">
										<span class="currpage_number"><?php echo $currpage ?></span>&nbsp;из&nbsp;<?php echo $lastpage ?>
									</span>
									<span class="next"><?php echo   "<a class='loadpage' data-page='". ( $currpage + 1 ) . "' href='" . $burl . "?pg=". ( $currpage < $lastpage ? $currpage + 1 : $lastpage ) . "'>&#x203A;</a>"  ?></span>
								</li>
								<li class="paging-border">
									<?php echo "<a class='loadpage' data-page='".  $lastpage ."' href='". $burl ."?pg=". $lastpage ."'>Последняя</a>" ?>
								</li>
							</ul>
						</nav>


						<script>
						var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
						var true_posts = '<?php echo serialize($wp_query->query_vars); ?>';
						var current_page = <?php echo ( get_query_var( 'pg' ) ) ? get_query_var( 'pg' ) : 1 ?>;
						var max_pages = '<?php echo $wp_query->max_num_pages; ?>';
						</script>

						<div class="flex w-100">
						<!-- <div id="loadmore" class="more-cards">Загрузить ещё</div> -->
<!-- <?php
						$page = 4;
						$args = array(
							'show_all'     => true, // показаны все страницы участвующие в пагинации
							'end_size'     => 1,     // количество страниц на концах
							'mid_size'     => 2,     // количество страниц вокруг текущей
							'prev_next'    => false,  // выводить ли боковые ссылки "предыдущая/следующая страница".
							'prev_text'    => __('« Previous'),
							'next_text'    => __('Next »'),
							'add_args'     => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
							'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
							'screen_reader_text' => __( 'Posts navigation' ),
						);
						$GLOBALS['wp_query'] = $wp_query;
						the_posts_pagination($args); ?> -->
					<?php 




					 ?>
						</div>
					<?php
					endif;
					wp_reset_query();
					?>
					</div>
					<div class="mobile_banners">
						<?php
							if (function_exists('dynamic_sidebar')){
								dynamic_sidebar('above_footer_mob');
							}
							?>
					</div>

					<div class="desktop_banners">
						<?php
							if (function_exists('dynamic_sidebar')){
								dynamic_sidebar('above_footer_desk');
							}
							?>
					</div>
				</div>
			</div>
			<div class="right-col">
				<?php
					if (function_exists('dynamic_sidebar')){
						dynamic_sidebar('right_column');
					}
					if (function_exists('dynamic_sidebar')){
						dynamic_sidebar('right_column_inner');
					}
				?>
			</div>
		</div>
	</main>

	<section class="default-slider gray">
	  <div class="container">
	    <div class="heading">
	      <h6>Популярное</h6>
	    </div>
	    <div class="owl-carousel">
	      <?php popular_carousel( $post->ID ); ?>
	    </div>
	  </div>
	</section>


	<section class="partners-slider">
		<div class="container-fluid">
			<div class="owl-carousel">
				<?php
					$cat = get_the_category($post->ID);

	        $query = new WP_Query([
	          'post_type' => 'brands',
	          'posts_per_page' => -1,
						'orderby' => 'name',
						'order' => 'ASC',
	        ]);
	        while($query->have_posts()) {
	          $query->the_post();
	        ?>
					<div class="item">
						<a href="<?php the_permalink(); ?>" class="wrapper">
							<img src="<?=kama_thumb_src('h=55 &crop=0', get_the_post_thumbnail_url())?>" alt="<?php echo get_the_title( $currpost->ID ); ?>">
						</a>
					</div>
	        <?php
		      }
		      wp_reset_postdata();
		    ?>

			</div>
		</div>

	</section>


<?php get_footer(); ?>
