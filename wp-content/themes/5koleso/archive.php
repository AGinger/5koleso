<?php get_header(); ?>
<?php
	
 	$current_category = single_cat_title("", false);

?>
	<main class="container">
		<div class="flex">
			<div class="left-col">
				<div class="section-cards-wrapper">
					<ul class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
	          <? bcn_display() ?>
	        </ul>
					<h1><?php the_archive_title(); ?></h1>

					<?php
					if(have_posts()) :
						$idx = 0;
						while ( have_posts() ) : the_post();
							if($idx == 0){
								?>
								<a href="<?php the_permalink(); ?>" class="card lg" style='padding: 0'>
									<img src="<?= wrap_kama_image( get_field("img_intro"),  'w=875 &h=440 &crop=center' ) ?>" alt="<?php the_archive_title() ?>" style="position: absolute; width: 100%; height: 100%; object-fit: cover;">
									<div class='card lg' style='margin: 0'>									
										<div class="text">
											<h2><?php the_title(); ?></h2>
										</div>
									</div>
								</a>

								<div class="flex">
								<?
							}
							else {
								$title = get_the_title();
								if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
								elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
								else {$vtitle[0] = $title;}
								?>
								<a href="<?php the_permalink(); ?>" class="default-link">
									<span class="img-wrapper">
										<img src="<?=wrap_kama_image( get_field("img_intro"),  'w=380 &h=210 &crop=center' )?>" alt="<?php the_archive_title(); ?>">
									</span>
									<span><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
								</a>
								<?
								$vtitle[1]='';
							}


						$idx++;
						endwhile;
						// ajax загрузка
						if ($wp_query->max_num_pages > 1) : ?>
		
						<?php 
							$currpage = get_query_var('paged') ? get_query_var('paged') : 1;
							$lastpage = $wp_query->max_num_pages;
						?>

						<nav class="paging">
							<ul class="paging-links">
								<li class="paging-border">
									<?php echo $currpage == 1 ? 'Первая' : "<a href='". pg_url(1) ."'>Первая</a>" ?>
								</li>
								<li class="paging-numbers">
									<span class="prev"><?php echo $currpage > 1 ? "<a href='". pg_url( $currpage - 1 ) . "'>&#x2039;</a>" : '&#x2039;' ?></span>
									<span class="position">
										<?php echo $currpage ?>&nbsp;из&nbsp;<?php echo $lastpage ?>
									</span>
									<span class="next"><?php echo $currpage < $lastpage ? "<a href='". pg_url( $currpage + 1 )  ."'>&#x203A;</a>" : '&#x203A;' ?></span>
								</li>
								<li class="paging-border">
									<?php echo $currpage == $lastpage ? 'Последняя' : "<a href='". pg_url( $lastpage )  ."'>Последняя</a>" ?>
								</li>
							</ul>
						</nav>
						
						<?php
						endif;

					else :
							echo 'Нет записей';

					endif;
					?>
					</div>

				</div>
			</div>
			<div class="right-col arhive">
				<?php
					if (function_exists('dynamic_sidebar')){
						dynamic_sidebar('right_column');
					}
					if (function_exists('dynamic_sidebar')){
	          dynamic_sidebar('right_column_inner');
	        }
				?>
			</div>
		</div>
	</main>

	<section class="default-slider gray">
	  <div class="container">
	    <div class="heading">
	      <h6>Популярное</h6>
	    </div>
	    <div class="owl-carousel">
	      <?php popular_carousel( $post->ID ); ?>
	    </div>
	  </div>
	</section>
	<div class="mobile_banners">
		<?php
			if (function_exists('dynamic_sidebar')){
				dynamic_sidebar('above_footer_mob');
			}
			?>
	</div>

	<div class="desktop_banners">
		<?php
			if (function_exists('dynamic_sidebar')){
				dynamic_sidebar('above_footer_desk');
			}
			?>
	</div>

	<section class="partners-slider">
		<div class="container-fluid">
			<div class="owl-carousel">
				<?php
					$cat = get_the_category($post->ID);

	        $query = new WP_Query([
	          'post_type' => 'brands',
	          'posts_per_page' => -1,
						'orderby' => 'name',
						'order' => 'ASC',
	        ]);
	        while($query->have_posts()) {
	          $query->the_post();
	        ?>
					<div class="item">
						<a href="<?php the_permalink(); ?>" class="wrapper">
							<img src="<?=kama_thumb_src('h=55 &crop=0', get_the_post_thumbnail_url())?>" alt="<?php echo $current_category ?>">
						</a>
					</div>
	        <?php
		      }
		      wp_reset_postdata();
		    ?>
			</div>
		</div>
		<div class="container">
			<div class="all_brands_box"><a href="/brands/" class="all_brands_link">Все бренды</div>
		</div>
	</section>


<?php get_footer(); 

