<?php get_header();

	$page_title = 'Результат поиска по «' . htmlspecialchars($_GET['s'] ) . '»';
?>

	<main class="container">
		<div class="flex">
			<div class="left-col">
				<div class="section-cards-wrapper">
					<h1><?php echo $page_title ?></h1>

					<div class="flex">
						<?php if (have_posts()) : while (have_posts()) : the_post();
							$url = get_permalink();
							?>
							<a href="<?=$url ?>" class="default-link">
									<? if(strpos($url,'brands/')):?>
									<span class="img-wrapper brands">
									 <img class="brand-img" src="<?=kama_thumb_src('h=50 &crop=center', get_the_post_thumbnail_url())?>" alt="<?php the_title(); ?>">
									<?else:?>
									<span class="img-wrapper">
									<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro"))?>" alt="<?php echo $page_title ?>">
									<?endif;?>
								</span>
								<span><?php the_title(); ?></span>
							</a>

						<?php endwhile; ?>

							<div class="flex search-pagination w-100">
							<?php
							$args = array(
								'show_all'     => false, // показаны все страницы участвующие в пагинации
								'end_size'     => 1,     // количество страниц на концах
								'mid_size'     => 2,     // количество страниц вокруг текущей
								'prev_next'    => false,  // выводить ли боковые ссылки "предыдущая/следующая страница".
								'prev_text'    => __('« Previous'),
								'next_text'    => __('Next »'),
								'add_args'     => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
								'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
								'screen_reader_text' => __( 'Posts navigation' ),
							);
							the_posts_pagination($args); ?>
							</div>
						<?php
							else: ?>
							<p>Поиск не дал результатов.</p>
						<?php endif;?>
					</div>
				</div>
			</div>
			<div class="right-col"></div>
		</div>
	</main>

	<section class="default-slider gray">
	  <div class="container">
	    <div class="heading">
	      <h6>Популярное</h6>
	    </div>
	    <div class="owl-carousel">
	      <?php popular_carousel( $post->ID ); ?>
	    </div>
	  </div>
	</section>


	<section class="partners-slider">
		<div class="container-fluid">
			<div class="owl-carousel">
				<?php
					$cat = get_the_category($post->ID);

	        $query = new WP_Query([
	          'post_type' => 'brands',
	          'posts_per_page' => -1,
						'orderby' => 'name',
	          'order' => 'ASC',
	        ]);
	        while($query->have_posts()) {
	          $query->the_post();
	        ?>
					<div class="item">
						<a href="<?php the_permalink(); ?>" class="wrapper">
							<img src="<?=kama_thumb_src('h=55 &crop=0', get_the_post_thumbnail_url())?>" alt="<?php echo $page_title ?>">
						</a>
					</div>
	        <?php
		      }
		      wp_reset_postdata();
		    ?>

			</div>
		</div>
	</section>


<?php get_footer(); ?>



 <?php get_footer(); ?>
