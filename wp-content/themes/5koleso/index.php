
<?php get_header(); ?>

<section class="tests container">
	<div class="flex">
		<div class="left">
      <?
        $exclude_id = [];

        // Главный пост
        $index_post = get_field('index_post');
        if($index_post){
          // если задан главный пост вручную
          $index_category = get_the_category($index_post[0]);
          $query = new WP_Query([
            'p' => $index_post[0],
            'posts_per_page' => 1,
          ]);
        }
        else {
          $query = new WP_Query([
            'cat' => 3, // тесты
						'posts_per_page' => 1,
						'order'   => 'DESC',
          ]);
        }

				while($query->have_posts()) {
					$query->the_post();

          // категория
          $category_object = get_the_category(get_the_ID());
          $category_name = $category_object[0]->name;

          $exclude_id[] = get_the_ID(); // добавляем записи в исключение
				?>
        	<a href="<?php the_permalink(); ?>" class="card" style='padding: 0'>
        		<img src="<?=kama_thumb_src('w=875 &h=440 &crop=center', get_field("img_intro", $post->ID))?>" alt="<?php echo get_the_title() ?>"
		   		style="position: absolute; width: 100%; height: 100%; object-fit: cover;">    
		   		<span class="card" style='margin: 0'>
			   		<u class="sticker"><?=$index_post ? $index_category[0]->name : $category_name?></u>
	  				<div class="text">
	  					<h2><?php the_title(); ?></h2>
	  				</div>	
		   		</span>  				
  			</a>
			  <?php
				}
				wp_reset_postdata();
      ?>

			<div class="flex-child">
				<div class="heading">
					<h6><?=get_cat_name(2)?></h6>
					<a href="<?=get_category_link(2)?>" class="more">Смотреть все</a>
				</div>
        <?php
					$query = new WP_Query([
						'cat' => 2,
						'posts_per_page' => 3,
						'order'   => 'DESC',
					]);
					while($query->have_posts()) {
						$query->the_post();
            $exclude_id[] = get_the_ID(); // добавляем записи в исключение
					?>
          <div class="col">
  					<a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a>
  				</div>
				  <?php
  				}
  				wp_reset_postdata();
  			?>
			</div>
		</div>
		<div class="right">
			<?php
				if (function_exists('dynamic_sidebar')){
					dynamic_sidebar('right_column');
				}
			?>
		</div>
	</div>
</section>

<section class="section-links container">
	<div class="flex">
    <?
      $index_relevant = get_field('index_relevant');
      $query = new WP_Query([
        'post__in' => $index_relevant,
        'posts_per_page' => 3,
        'orderby' => 'post__in',
        'post_status' => 'publish',
      ]);
      while($query->have_posts()) {
        $query->the_post();
        $exclude_id[] = get_the_ID(); // добавляем записи в исключение
        $category_name = get_the_category(get_the_ID());
				$title = get_the_title();
				if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
				elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
				else {$vtitle[0] = $title;}
      ?>
      <a href="<?php the_permalink(); ?>" class="card" style='padding: 0'>
      		<img src="<?=kama_thumb_src('w=875 &h=440 &crop=center', get_field("img_intro", $post->ID))?>" alt="<?php echo get_the_title() ?>">
      		<div class="card" style="margin: 0">
	  			<u class="sticker"><?=$category_name[0]->name?></u>
	  			<span class="text"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
      		</div>
  		</a>
      <?php
      }
      wp_reset_postdata();
    ?>
	</div>
</section>

<section class="home-main container">
	<div class="heading">
		<h6>Главное</h6>
	</div>
	<div class="flex">
    <div class="left">
    <?php
		  $exclude_id[] = get_the_ID(); // добавляем записи в исключение
      $query = new WP_Query([
        'category__not_in' => [20,22,28],
			//	'cat' => array(-20),
        'posts_per_page' => 10,
        'order'   => 'DESC',
        'post__not_in'   => $exclude_id,
      ]);
      $idx = 1;
      while($query->have_posts()) {
        $query->the_post();
        if($idx == 1){
          ?>
            <a href="<?php the_permalink(); ?>" class="default-link">
              <span class="img-wrapper">
                <img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
              </span>
              <span><?php the_title(); ?></span>
            </a>
            </div>
          <div class="right">
          <?
        }
        else {
          ?>
          <div class="col">
            <a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a>
          </div>
          <?
        }
      ?>
      <?php
        $idx++;
      }
      wp_reset_postdata();
    ?>
    </div>
	</div>
</section>

<section class="default-slider gray">
	<div class="container">
		<div class="heading">
			<h6><?=get_cat_name(3)?></h6>
			<a href="<?=get_category_link(3)?>" class="more">Смотреть все</a>
		</div>
		<div class="owl-carousel">
      <?php // ТЕСТЫ
        $query = new WP_Query([
          'cat' => 3,
          'posts_per_page' => 10,
          'order'   => 'DESC',
          'post__not_in'   => $exclude_id,
        ]);
        while($query->have_posts()) {
          $query->the_post();
					$title = get_the_title();
					if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
					elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
					else {$vtitle[0] = $title;}
        ?>
        <div class="item">
          <a href="<?php the_permalink(); ?>" class="default-link">
            <span class="img-wrapper">
              <img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
            </span>
            <span><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
          </a>
        </div>
        <?php
        }
        wp_reset_postdata();
      ?>
		</div>
	</div>
</section>

<section class="partition container">
	<div class="flex">
		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(6)?></h6>
				<a href="<?=get_category_link(6)?>" class="more">Смотреть все</a>
			</div>

      <?php // ОТЗЫВЫ
				$query = new WP_Query([
					'cat' => 6,
					'posts_per_page' => 4,
					'order' => 'DESC',
          'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
							$title = get_the_title();
						if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
						elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
						else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
						<?
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
				  $idx++;
  			}
  			wp_reset_postdata();
  		?>
		</div>
		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(7)?></h6>
				<a href="<?=get_category_link(7)?>" class="more">Смотреть все</a>
			</div>

      <?php // ШИНЫ
				$query = new WP_Query([
					'cat' => 7,
					'posts_per_page' => 4,
					'order' => 'DESC',
          'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
							$title = get_the_title();
						if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
						elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
						else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
						<?
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
				  $idx++;
  			}
  			wp_reset_postdata();
  		?>
		</div>
		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(8)?></h6>
				<a href="<?=get_category_link(8)?>" class="more">Смотреть все</a>
			</div>

      <?php // ГАРАЖ
				$query = new WP_Query([
					'cat' => 8,
					'posts_per_page' => 4,
					'order' => 'DESC',
          'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
							$title = get_the_title();
						if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
						elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
						else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
						<?
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
				  $idx++;
  			}
  			wp_reset_postdata();
  		?>
		</div>
	</div>
</section>

<section class="default-slider gray">
	<div class="container">
		<div class="heading">
			<h6><?=get_cat_name(4)?></h6>
			<a href="<?=get_category_link(4)?>" class="more">Смотреть все</a>
		</div>
		<div class="owl-carousel">
				<?php // НОВИНКИ
					$query = new WP_Query([
						'cat' => 4,
						'posts_per_page' => 10,
						'order'   => 'DESC',
            'post__not_in'   => $exclude_id,
					]);
					while($query->have_posts()) {
						$query->the_post();
						$title = get_the_title();
						if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
						elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
						else {$vtitle[0] = $title;}
					?>
					<div class="item">
						<a href="<?php the_permalink(); ?>" class="default-link">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
					</div>
				  <?php
  				}
  				wp_reset_postdata();
  			?>
		</div>
	</div>
</section>

<section class="partition container">
	<div class="flex">
		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(9)?></h6>
				<a href="<?=get_category_link(9)?>" class="more">Смотреть все</a>
			</div>

			<?php // МОТО
				$sticky = get_option( 'sticky_posts' );
				// $query = new WP_Query( [ 'post__in' => $sticky ] );

				$query = new WP_Query([
					'category_name' => 'moto',
					'posts_per_page' => 4,
					'order' => 'DESC',
          'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
						$title = get_the_title();
						if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
						elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
						else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
						<?
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
				  $idx++;
  			}
  			wp_reset_postdata();
  		?>
		</div>
		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(10)?></h6>
				<a href="<?=get_category_link(10)?>" class="more">Смотреть все</a>
			</div>

      <?php // КОМПАНИИ
				$query = new WP_Query([
					'cat' => 10,
					'posts_per_page' => 4,
					'order' => 'DESC',
          'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
						$title = get_the_title();
						if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
						elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
						else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
						<?
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
				  $idx++;
  			}
  			wp_reset_postdata();
  		?>
		</div>

		<div class="col">
			<div class="heading">
				<h6><?=get_cat_name(25)?></h6>
				<a href="<?=get_category_link(25)?>" class="more">Смотреть все</a>
			</div>

			<?php // КОМПАНИИ
				$query = new WP_Query([
					'cat' => 25,
					'posts_per_page' => 4,
					'order' => 'DESC',
          'post__not_in'   => $exclude_id,
				]);
				$idx = 0;
				while($query->have_posts()) {
					$query->the_post();

					if($idx == 0) {
						$title = get_the_title();
						if(strpos($title,':')) {$vtitle = explode(':', $title);$vtitle[0].= ':';}
						elseif(strpos($title,'.' )) {$vtitle = explode('.', $title);$vtitle[0].= '.';}
						else {$vtitle[0] = $title;}
						?>
						<a href="<?php the_permalink(); ?>" class="default-link link-card">
							<span class="img-wrapper">
								<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID))?>" alt="alt">
							</span>
							<span class="name"><strong><?php echo $vtitle[0];?></strong> <?php if(count($vtitle)==2) echo '<br>'.$vtitle[1];?></span>
						</a>
						<?
					}
					else {
						?>
						<div class="partition__mini"><a href="<?php the_permalink(); ?>" class="default-link"><?php the_title(); ?></a></div>
						<?
					}
				  $idx++;
  			}
  			wp_reset_postdata();
  		?>
	</div>
</section>

<section class="partners-slider gray">
  <div class="container">
    <div class="owl-carousel">
      <?php
        $cat = get_the_category($post->ID);

        $query = new WP_Query([
          'post_type' => 'brands',
          'posts_per_page' => -1,
          'order' => 'DESC',
        ]);
        while($query->have_posts()) {
          $query->the_post();
        ?>
        <div class="item">
          <a href="<?php the_permalink(); ?>" class="wrapper">
            <img src="<?=kama_thumb_src('h=55 &crop=0', get_the_post_thumbnail_url())?>" alt="alt">
          </a>
        </div>
        <?php
        }
        wp_reset_postdata();
      ?>
    </div>
  </div>
	<div class="container">
		<div class="all_brands_box gray"><a href="/brands/" class="all_brands_link">Все бренды</div>
	</div>
</section>


<?php get_footer(); ?>
