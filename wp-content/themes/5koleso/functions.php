<?php

// function check_url($url)
// {
//   if(preg_match("@^http://@i",$url)) $url = preg_replace("@(http://)+@i",'http://',$url);
//   else if (preg_match("@^https://@i",$url)) $url = preg_replace("@(https://)+@i",'https://',$url);
//   else $url = 'http://'.$url;
 
 
//   if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
//     return false;
// }
// else return $url;
// }
//  // Существование ссылки (URL)
// function open_url($url)
// {
//  $url_c=parse_url($url);
 
//   if (!empty($url_c['host']) and checkdnsrr($url_c['host']))
//   {
//     // Ответ сервера
//     if ($otvet=@get_headers($url)){
//       return substr($otvet[0], 9, 3);
//     }
//   }
//   return false;
// }
function yturbo_sign($content)
{
    $rsspermalink = esc_url(apply_filters('the_permalink_rss', get_permalink()));
    $sign = '<ExtInfoxSgWidget
    data-script-id="24139"
    data-width="auto"
    data-height="250"
/>';

    $content .= $sign;
    return $content;
}
add_filter('yturbo_the_content', 'yturbo_sign');
function getYTblock($category)
{
    global $wpdb;
    $form = $wpdb->get_results("SELECT * FROM wp_neyiron_yt WHERE category_name = '". $category. "' ");
    $forms =[
                'iframe' => $form[0]->{'iframe'},
                'title'  => $form[0]->{'title'},
                'description' => $form[0]->{'description'},
                'video1' => $form[0]->{'video1'},
                'video2' => $form[0]->{'video2'},
                'video3' => $form[0]->{'video3'},
            ];
    echo '
    <section class="home-main container YT" style="background: black">
        <div class="heading">
 	        </div>
             <div  id="yth6" class="h6" style="margin-bottom: 20px;margin-left: 25px;margin-top: -10px;">
             <a href="https://www.youtube.com/c/%D0%96%D1%83%D1%80%D0%BD%D0%B0%D0%BB5%D0%BA%D0%BE%D0%BB%D0%B5%D1%81%D0%BE/featured" style="margin: 0; font-weight: 400; font-size: 30px; font-family: noto_serifbold; font-weight: 400; margin-bottom: 25px; color:#ffffff">Видео</a></div>
             <div class="YT-block">
                 <div class="main-YT-item" style="display: block;margin-left: 30px;">
                     <div class="ytm" style="float: left; margin-left: -8px;">'.wp_unslash($forms["iframe"]).'</div>
                     <span  class="span-YT" style="float: left; margin: 0; font-weight: 400; font-size: 30px; font-family: noto_serifbold; font-weight: 400; margin-bottom: 25px; color: #ffffff; width: 50%; margin-left: 25px;"> 
                         '.wp_unslash($forms["title"]).'
                         <span class="description-YT" style="display: block; font-size: 15px; color: #757575; margin-right: -10px; text-align: left; width: 98%; margin-top: 10px; margin-bottom: 10px;">
                         '.wp_unslash($forms["description"]).'
                         </span>
						 </span>
					 </div>
					 <div class="plan-YT" style=" display: inline-flex; width: 100%; justify-content: space-evenly; padding-top: 10px;    padding-bottom: 13px;"> 
				    <div class="item-o-YT" style="display: block;">'.wp_unslash($forms["video1"]).'</div>
                    <div class="item-t-YT" style="display: block;">'.wp_unslash($forms["video2"]).'</div>
                    <div class="item-f-YT" style="display: block;">'.wp_unslash($forms["video3"]).'</div>
					</div>
			</div>
		</div>
</section>';
}
function rus2translit($string)
{
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'j',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'cz',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '',    'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'J',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    return strtr($string, $converter);
}

function str2url($str)
{
    // переводим в транслит
    $str = rus2translit($str);
    // в нижний регистр
    $str = strtolower($str);
    // заменям все ненужное нам на "-"
    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
    // удаляем начальные и конечные '-'
    $str = trim($str, "-");
    return $str;
}

function mayak_single()
{
    global $wp_query, $post;
    $post_type = get_post_type($post->ID);
    if (in_category('22') || in_category('28')) {
        return TEMPLATEPATH."/single-archive.php";
    } elseif ($post_type == 'brands') {
        return TEMPLATEPATH."/single-brands.php";
    } else {
        return TEMPLATEPATH."/single-post.php";
    }
}
add_filter('single_template', 'mayak_single');


add_action('init', 'unFocus_insensitivity');

function wrap_kama_image($src, $params)
{
    $kama_image = kama_thumb_src($params, $src);
    if (false !== strstr($kama_image, 'stub_')) {
        $objImage = get_post($src);
        if ($objImage) {
            $fixFile = prepare_fix_image($kama_image, $objImage->guid);
            //$objImage->guid
            $kama_image = trim($fixFile, '.');
        } elseif (preg_match('@(jpe?g|png|gif)$@', $src)) {
            $kama_image = prepare_fix_image($kama_image, $src);
        } else {
        }
    }
    return $kama_image;
}

function prepare_fix_image($stub_image, $orig_image)
{
    $stubFile = '.' . str_replace(get_site_url(), '', $stub_image);
    $origImage = str_replace('https:', 'http:', $orig_image);
    $fixFile = dirname($stubFile) . '/' . basename($origImage);
    if (! file_exists($fixFile) || @filesize($fixFile) == 0) {
        $bFile = file_get_contents($origImage);
        if ($bFile) {
            file_put_contents($fixFile, $bFile);
        } else {
            return $stub_image;
        }
    }
    return $fixFile;
}

function unFocus_insensitivity()
{
    if (!is_admin()) {
        if (preg_match('/[A-Z]/', urldecode($_SERVER['REQUEST_URI']))) {
            header('Location: //'.$_SERVER['HTTP_HOST'] . strtolower($_SERVER['REQUEST_URI']), true, 301);
            exit();
        }
    }
}

add_theme_support('post-thumbnails');

add_action('after_setup_theme', 'register_menus');

function register_menus()
{
    register_nav_menus(array(
        'top' => __('Основное меню', 'top'),
        'bottom'  => __('Нижнее меню', 'bottom'),
        'test-top'  => __('Основное меню тест', 'test-top'),
    ));
}


add_action('widgets_init', 'register_my_widgets');
function register_my_widgets()
{
    register_sidebar([
        'name'          => sprintf(__("Баннер над шапкой"), $i),
        'description'   => '',
        'id'   					=> 'before_header',
        'before_widget' => '<section class="container">',
        'after_widget' 	=> '</section>',
        'before_title' 	=> '',
        'after_title'		=> '',
    ]);
    register_sidebar([
        'name'          => sprintf(__("Баннер над шапкой десктоп"), $i),
        'description'   => '',
        'id'   					=> 'before_header_desk',
        'before_widget' => '<section class="banner-1200x288 container">',
        'after_widget' 	=> '</section>',
        'before_title' 	=> '',
        'after_title'		=> '',
    ]);
    register_sidebar([
        'name'          => sprintf(__("Баннер над шапкой мобильный"), $i),
        'description'   => '',
        'id'   					=> 'before_header_mob',
        'before_widget' => '<section class="container">',
        'after_widget' 	=> '</section>',
        'before_title' 	=> '',
        'after_title'		=> '',
    ]);
    register_sidebar([
        'name'          => sprintf(__("Баннер под меню"), $i),
        'description'   => '1200x105',
        'id'   					=> 'after_header',
        'before_widget' => '<section class="banner-1200x105 container">',
        'after_widget' 	=> '</section>',
        'before_title' 	=> '',
        'after_title'		=> '',
    ]);
    register_sidebar([
        'name'          => sprintf(__("Баннер справа на главной"), $i),
        'description'   => '300x600, 240x400, 300x300',
        'id'  					=> 'right_column_main',
        'before_widget' => '<div class="banner-300x600">',
        'after_widget' 	=> '</div>',
        'before_title' 	=> '',
        'after_title'		=> '',
    ]);
    register_sidebar([
        'name'          => sprintf(__("Баннер под шапкой на главной "), $i),
        'description'   => '',
        'id'  					=> 'after_header_onmain',
        'before_widget' => '',
        'after_widget' 	=> '',
        'before_title' 	=> '',
        'after_title'		=> '',
    ]);
    register_sidebar([
        'name'          => sprintf(__("Баннер справа на внутренних"), $i),
        'description'   => '300x600, 240x400, 300x300',
        'id'  					=> 'right_column_inner',
        'before_widget' => '<div class="banner-300x600">',
        'after_widget' 	=> '</div>',
        'before_title' 	=> '',
        'after_title'		=> '',
    ]);
    register_sidebar([
        'name'          => sprintf(__("Баннер перед подвалом десктоп"), $i),
        'description'   => '',
        'id'  					=> 'above_footer_desk',
        'before_widget' => '',
        'after_widget' 	=> '',
        'before_title' 	=> '',
        'after_title'		=> '',
    ]);
    register_sidebar([
        'name'          => sprintf(__("Баннер перед подвалом мобилка"), $i),
        'description'   => '',
        'id'  					=> 'above_footer_mob',
        'before_widget' => '',
        'after_widget' 	=> '',
        'before_title' 	=> '',
        'after_title'		=> '',
    ]);
    register_sidebar([
        'name'          => sprintf(__("Статьи под введением десктоп"), $i),
        'description'   => '',
        'id'  					=> 'under_intro_desk',
        'before_widget' => '',
        'after_widget' 	=> '',
        'before_title' 	=> '',
        'after_title'		=> '',
    ]);
    register_sidebar([
        'name'          => sprintf(__("Статьи под введением мобилка"), $i),
        'description'   => '',
        'id'  					=> 'under_intro_mob',
        'before_widget' => '',
        'after_widget' 	=> '',
        'before_title' 	=> '',
        'after_title'		=> '',
    ]);
    register_sidebar([
        'name'          => sprintf(__("Баннер перед тегами десктоп"), $i),
        'description'   => '',
        'id'  					=> 'banner_page_after_teg_desk',
        'before_widget' => '',
        'after_widget' 	=> '',
        'before_title' 	=> '',
        'after_title'		=> '',
    ]);
    register_sidebar([
        'name'          => sprintf(__("Баннер перед тегами мобильный"), $i),
        'description'   => '',
        'id'  					=> 'banner_page_after_teg_mob',
        'before_widget' => '',
        'after_widget' 	=> '',
        'before_title' 	=> '',
        'after_title'		=> '',
    ]);
}
// удалить заголовки виджетов если в название заголовка добавить ! (восклицательный знак)
add_filter('widget_title', 'hide_widget_title');
function hide_widget_title($title)
{
    if (empty($title)) {
        return '';
    }
    if ($title[0] == '!') {
        return '';
    }
    return $title;
}


## Удаляет "Рубрика: ", "Метка: " и т.д. из заголовка архива
add_filter('get_the_archive_title', function ($title) {
    return preg_replace('~^[^:]+: ~', '', $title);
});


// ajax загрузка контента
function true_load_posts()
{
    $args = unserialize(stripslashes($_POST['query']));

    if ($args['cat'] == 22 || $args['cat'] == 28) {
        $pages = 12;
    } else {
        $pages = 10;
        $args['offset'] = $_POST['page']*$pages + 0;
    }

    $args['paged'] = $_POST['page'] + 1; // следующая страница
    $args['post_status'] = 'publish';
    $args['posts_per_page'] = $pages;

    $q = new WP_Query($args);

    if ($q->have_posts()) {
        while ($q->have_posts()) {
            $q->the_post();
            if ($args['cat'] == 22 || $args['cat'] == 28) {
                ?>
				<a href="<?=get_field("archive_pdf", $post->ID); ?>" class="archive-card" target="_blank">
				<span class="name"><?php the_title(); ?></span>
				<img src="<?=kama_thumb_src('w=280 &h=365 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="alt">
			</a>
		<?php
            } else { ?>
			<a href="<?php the_permalink(); ?>" class="default-link">
				<span class="img-wrapper">
					<img src="<?=kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro")); ?>" alt="<?php the_title(); ?>">
				</span>
				<span><?php the_title(); ?></span>
			</a>
      <?php } ?>
   <?php
        }
    }
    wp_reset_postdata();
    die();
}


add_action('wp_ajax_loadmore', 'true_load_posts');
add_action('wp_ajax_nopriv_loadmore', 'true_load_posts');



function ajaxfilter()
{
    $args = unserialize(stripslashes($_POST['query']));

    $q = new WP_Query([
        'cat'   => $args['cat'],
        'posts_per_page' => 12,
        'order'   => 'DESC',
        'meta_query' =>array(
            array(
                'key' => 'year',
                'value' => $_POST["selectyear"]
            )
        )
        ]);

    if ($q->have_posts()) {
        while ($q->have_posts()) {
            $q->the_post(); ?>
					<a href="<?=get_field("archive_pdf", $post->ID); ?>" class="archive-card" target="_blank">
					<span class="name"><?php the_title(); ?></span>
					<img src="<?=kama_thumb_src('w=280 &h=365 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="alt">
				</a>
				<?php
        }
    }
       
    wp_reset_postdata();
    // ajax загрузка
    if ($q->max_num_pages > 1) { ?>
			<script>
			var ajaxurl = '<?php echo site_url(); ?>/wp-content/themes/5koleso/new_ajax.php';
			var true_posts = '<?php echo serialize($q->query_vars); ?>';
			var current_page = 1;
			var max_pages = '<?php echo $q->max_num_pages; ?>';
			</script>
			<div class="flex w-100">
			<div id="loadmore" class="more-cards">Загрузить ещё</div>
			<?php
            $args = array(
                'show_all'     => true, // показаны все страницы участвующие в пагинации
                'end_size'     => 1,     // количество страниц на концах
                'mid_size'     => 1,     // количество страниц вокруг текущей
                'prev_next'    => false,  // выводить ли боковые ссылки "предыдущая/следующая страница".
                'prev_text'    => __('« Previous'),
                'next_text'    => __('Next »'),
                'add_args'     => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
                'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
                'screen_reader_text' => __('Posts navigation'),
            );
            the_posts_pagination($args); ?>
			</div>
		<?php
        }
    wp_die();
}

 

add_action('wp_ajax_yearfilter', 'ajaxfilter');
add_action('wp_ajax_nopriv_yearfilter', 'ajaxfilter');


// ПРОСМОТРЫ
function getPostViews($postID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count=='') {
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}
function setPostViews($postID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count=='') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

// кол-во просмотров в админке
add_filter('manage_posts_columns', 'posts_column_views');
add_action('manage_posts_custom_column', 'posts_custom_column_views', 5, 2);
function posts_column_views($defaults)
{
    $defaults['post_views'] = __('просмотров');
    return $defaults;
}
function posts_custom_column_views($column_name, $id)
{
    if ($column_name === 'post_views') {
        echo getPostViews(get_the_ID());
    }
}



// Добавление шорткода в пост
add_shortcode('gallery', 'gallery_post_shortcode');
function gallery_post_shortcode()
{
    $images = get_field('gallery');

    global $post;
    $image_alt = '';
    if ($post) {
        $image_alt = get_the_title($post->ID);
    }

    ob_start();

    if ($images) {
        ?>
	 <div class="owl-carousel">
		 <?php foreach ($images as $image) { ?>
		 <div class="item">
			 <img src="<?=kama_thumb_src('w=1200 &h=620 &crop=center', $image['url']); ?>" alt="<?= $image_alt ? $image_alt : $image['alt']; ?>">
		 </div>
		 <?php } ?>
	 </div>

	<?php
    }

    return ob_get_clean();
}


// Добавление шорткода в пост
add_shortcode('display-post', 'displayPost_post_shortcode');
function displayPost_post_shortcode($atts)
{
    $params = shortcode_atts(array(
        'id' => 0,
    ), $atts);

    $url = get_permalink($params['id']);
    $title = get_the_title($params['id']);
    $img = get_field('img_intro', $params['id']);
    $category = get_the_category($params['id']);

    return "<a href='{$url}' class='card' style='background: url({$img}) no-repeat center / cover; width: 386.5px; height: 250px;float:left;margin: 0 15px 15px 0;'><u class='sticker'>{$category[0]->cat_name}</u><span class='text'><strong>{$title}</strong></span></a>";
}


// Удалить атрибуты ширины и высоты из вставляемых изображений
add_filter('post_thumbnail_html', 'remove_width_attribute', 10);
add_filter('image_send_to_editor', 'remove_width_attribute', 10);

function remove_width_attribute($html)
{
    $html = preg_replace('/(width|height)="\d*"\s/', "", $html);
    return $html;
}


function remove_footer_admin()
{
    echo 'Создание сайта <a href="https://neyiron.ru/" target="_blank">Digital агентство Neyiron</a>.';
}
add_filter('admin_footer_text', 'remove_footer_admin');




add_action('init', 'register_post_types');
function register_post_types()
{
    register_post_type('brands', array(
        'label'  => null,
        'labels' => array(
            'name'               => 'Бренды', // основное название для типа записи
            'singular_name'      => 'Бренды', // название для одной записи этого типа
            'add_new'            => 'Добавить бренд', // для добавления новой записи
            'add_new_item'       => 'Добавление бренда', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Редактирование бренда', // для редактирования типа записи
            'new_item'           => 'Новое', // текст новой записи
            'view_item'          => 'Смотреть бренд', // для просмотра записи этого типа.
            'search_items'       => 'Искать бренд', // для поиска по этим типам записи
            'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
            'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
            'menu_name'          => 'Бренды', // название меню
        ),
        'public'              => true,
        'hierarchical'        => false,
        'supports'            => array('title', 'thumbnail', 'page-attributes', 'custom-fields'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
        'has_archive'         => true,
        'menu_icon'  => 'dashicons-admin-links',
    ));
}

// пагинация
add_filter('navigation_markup_template', 'my_navigation_template', 10, 2);
function my_navigation_template($template, $class)
{
    return '
	<nav class="navigation %1$s" role="navigation">
		<div class="select_page">'.(get_query_var('paged')?:1).'</div>
		<div class="nav-links">%3$s</div>
		<div class="bg_pagination"></div>
	</nav>
	';
}

function koleso_remove_page_canonical($canonical)
{
    return preg_replace('@page/\d+/$@', '', $canonical);
}

add_filter('wpseo_canonical', 'koleso_remove_page_canonical', 10, 4);


    if (preg_match('@page/\d+/$@', $_SERVER['REQUEST_URI'])) {
        add_filter('wpseo_next_rel_link', '__return_false');
        add_filter('wpseo_prev_rel_link', '__return_false');
    }

    if (isset($_GET['page'])) {
        add_filter('wpseo_next_rel_link', '__return_false');
    }

    add_filter('wpseo_title', 'koleso_seo_title_paging');
    add_filter('wpseo_opengraph_title', 'koleso_seo_title_paging');
    function koleso_seo_title_paging($title)
    {
        global $post;
        $nPage = koleso_get_page();

        if ($nPage > 0) {
            $page_title = get_the_archive_title();
            if (false !== strstr($_SERVER['REQUEST_URI'], '/brands/')) {
                $page_title = get_the_title();
            }
            return 'Страница ' . $nPage . ' – ' . $page_title . ' – 5koleso.ru';
        }

        return $title;
    }

    add_filter('wpseo_metadesc', 'koleso_seo_description_paging');
    add_filter('wpseo_opengraph_desc', 'koleso_seo_description_paging');
    function koleso_seo_description_paging($title)
    {
        global $post;
        
        $nPage = koleso_get_page();
        if ($nPage > 0) {
            $page_title = get_the_archive_title();
            if (false !== strstr($_SERVER['REQUEST_URI'], '/brands/')) {
                $page_title = get_the_title();
            }
            return 'Страница ' . $nPage . ' – ' . $page_title . ' – Последние новости и события автомира с обзорами и ценами. Читайте прямо сейчас!';
        }

        return $title;
    }

    function koleso_get_page()
    {
        $nPage = 0;

        if (isset($_GET['page'])) {
            $nPage = $_GET['page'];
        } elseif (isset($_GET['pg'])) {
            $nPage = $_GET['pg'];
        } elseif (preg_match('@page/(\d+)@', $_SERVER['REQUEST_URI'], $pgmatch)) {
            $nPage = $pgmatch[1];
        }

        return $nPage;
    }



    


add_action('wp_enqueue_scripts', 'add_my_scripts');
function add_my_scripts()
{
    $path = get_bloginfo('template_directory') . "/";//получаем путь к выбранной теме
    if (!is_admin()) {//это означает, что в админке не нужно подключать и заменять jquery так как может нарушится встроенный функционал
        wp_deregister_script('jquery');//отключаем стандартный jquery
        wp_register_script('jquery', ($path . "/js/jquery-2.2.4.min.js"), false, '2.2.4');//прописываем свой путь к своему jquery
        wp_enqueue_script('jquery');//Подключаем jquery
    }
    wp_enqueue_script('myJs', false, array(), false, true);
}

function return_canon()
{
    $canon_page = get_pagenum_link(1);
    return $canon_page;
}

function canon_paged()
{
    if (is_paged()) {
        add_filter('wpseo_canonical', 'return_canon');
    }
}
add_filter('wpseo_head', 'canon_paged');

add_filter('wpseo_xml_sitemap_img', '__return_false');


function add_acf_columns($columns)
{
    $columns['car_brand'] = 'Бренд';
    return $columns;
}
add_filter('manage_post_posts_columns', 'add_acf_columns');

add_action('manage_posts_custom_column', 'rachel_carden_populating_my_posts_columns', 10, 2);
function rachel_carden_populating_my_posts_columns($column_name, $post_id)
{
    switch ($column_name) {
case 'car_brand':
$brand_name = '';
$brand_id = '';
if (get_post_meta($post_id, "car_brand", true)!='') {
    $brand_name = get_the_title(get_post_meta($post_id, "car_brand", true));
    $brand_id = get_post_meta($post_id, "car_brand", true);
}
echo '<div id="brand-' . $post_id . '" data-brand-id="'. $brand_id .'">' . $brand_name . '</div>';
break;
}
}

add_action('bulk_edit_custom_box', 'rachel_carden_add_to_bulk_quick_edit_custom_box', 10, 2);
//add_action( 'quick_edit_custom_box', 'rachel_carden_add_to_bulk_quick_edit_custom_box', 10, 2 );

function rachel_carden_add_to_bulk_quick_edit_custom_box($column_name, $post_type)
{
    switch ($column_name) {
 case 'car_brand':
 ?><fieldset class="inline-edit-col-right">
 <div class="inline-edit-group">
 <label>
 <span class="title">Бренд</span>
 <select name="car_brand" size="1">
	<?php $args = [
            'post_type' => 'brands',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order'   => 'ASC',
        ];
        $query = new WP_Query($args);
        while ($query->have_posts()) {
            $query->the_post(); ?>
	 <option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
	 <?php
        }
 wp_reset_postdata();
 ?>
 </select>
 </label>
 </div>
 </fieldset><?php
 break;
 }
}

add_action('admin_print_scripts-edit.php', 'rachel_carden_enqueue_edit_scripts');
function rachel_carden_enqueue_edit_scripts()
{
    wp_enqueue_script('rachel-carden-admin-edit', get_bloginfo('stylesheet_directory') . '/quick_edit.js', array( 'jquery', 'inline-edit-post' ), '', true);
}

 
add_action('save_post', 'rachel_carden_save_post', 10, 2);
function rachel_carden_save_post($post_id, $post)
{
    // don't save for autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }
    // dont save for revisions
    if (isset($post->post_type) && $post->post_type == 'revision') {
        return $post_id;
    }
    if (array_key_exists('car_brand', $_POST)) {
        update_post_meta($post_id, 'car_brand', $_POST[ 'car_brand' ]);
    }
}

add_action('wp_ajax_rachel_carden_save_bulk_edit', 'rachel_carden_save_bulk_edit');
function rachel_carden_save_bulk_edit()
{
    // get our variables
    $post_ids = (isset($_POST[ 'post_ids' ]) && !empty($_POST[ 'post_ids' ])) ? $_POST[ 'post_ids' ] : array();
    $brand = (isset($_POST[ 'car_brand' ]) && !empty($_POST[ 'car_brand' ])) ? $_POST[ 'car_brand' ] : null;
    // if everything is in order
    if (!empty($post_ids) && is_array($post_ids) && !empty($brand)) {
        foreach ($post_ids as $post_id) {
            update_post_meta($post_id, 'car_brand', $brand);
        }
    }
}

add_action('template_redirect', 'Sheensay_HTTP_Headers_Last_Modified');

function Sheensay_HTTP_Headers_Last_Modified()
{
    if ((defined('DOING_AJAX') && DOING_AJAX) || (defined('XMLRPC_REQUEST') && XMLRPC_REQUEST) || (defined('REST_REQUEST') && REST_REQUEST) || (is_admin())) {
        return;
    }

    $last_modified = '';


    // Для страниц и записей
    if (is_singular()) {
        global $post;

        // Если пост запаролен - пропускаем его
        if (post_password_required($post)) {
            return;
        }

        if (!isset($post -> post_modified_gmt)) {
            return;
        }

        $post_time = strtotime($post -> post_modified_gmt);
        $modified_time = $post_time;

        // Если есть комментарий, обновляем дату
        if (( int ) $post -> comment_count > 0) {
            $comments = get_comments(array(
                'post_id' => $post -> ID,
                'number' => '1',
                'status' => 'approve',
                'orderby' => 'comment_date_gmt',
                    ));
            if (!empty($comments) && isset($comments[0])) {
                $comment_time = strtotime($comments[0] -> comment_date_gmt);
                if ($comment_time > $post_time) {
                    $modified_time = $comment_time;
                }
            }
        }

        $last_modified = str_replace('+0000', 'GMT', gmdate('r', $modified_time));
    }


    // Cтраницы архивов: рубрики, метки, даты и тому подобное
    if (is_archive() || is_home()) {
        global $posts;

        if (empty($posts)) {
            return;
        }

        $post = $posts[0];

        if (!isset($post -> post_modified_gmt)) {
            return;
        }

        $post_time = strtotime($post -> post_modified_gmt);
        $modified_time = $post_time;

        $last_modified = str_replace('+0000', 'GMT', gmdate('r', $modified_time));
    }


    // Если заголовки уже отправлены - ничего не делаем
    if (headers_sent()) {
        return;
    }

    if (!empty($last_modified)) {
        header('Last-Modified: ' . $last_modified);

        if (!is_user_logged_in()) {
            if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $modified_time) {
                $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1');
                header($protocol . ' 304 Not Modified');
            }
        }
    }
}

// запретить ссылки в комментариях WordPress.
function remove_link_comment($link_text)
{
    return strip_tags($link_text);
}
add_filter('pre_comment_content', 'remove_link_comment');
add_filter('comment_text', 'remove_link_comment');

if (! is_admin()) {
    function my_search_filter($query)
    {
        if ($query->is_main_query() && $query->is_search) {
            $query->set('posts_per_page', 10);
        }
        return $query;
    }
    add_filter('pre_get_posts', 'my_search_filter');
}

//Фильтр внешних ссылок add_filter('the_content', 'my_filter_function'); function my_filter_function($content){if(! is_admin() ) {$pat = '%<img\s([^>])*src="\/image\/gA(\S)+.jpg"([^>])*>%'; //$ext = '%(<a\shref="((https?:)?\/\/(?!(www\.)?5koleso\.ru)(\/?([^<])*)))(<\/a>)%i'; $ext = '%<a\shref="(https?:)?\/\/(?!((www\.)?5koleso\.ru|(www\.)?facebook\.com)|(www\.)?google\.ru|(www\.)?vk\.com)\/?([^>])*>(.*)<\/a>%i'; $content = preg_replace($pat, '', $content); $content = preg_replace($ext, '$8', $content); if ($_SERVER['REQUEST_URI']!='/feed/zen/') $content = str_replace('https://','//', $content); $content = str_replace('http://','//', $content); } return $content; }

    function popular_carousel($post_id)
    {
        $cat = get_the_category($post->ID);
        $query = new WP_Query([
             'post_type' => 'post',
             'posts_per_page' => 10,
             'order' => 'DESC',
             'post__not_in' => [$post->ID, 116827],
             'category__not_in' => $cat[0]->term_id,
             'meta_key' => 'popular_article',
             'meta_value' => 1,
        ]);
    
        show_popular_posts($query);
    
        $query = new WP_Query([
            'post_type' => 'post',
            'posts_per_page' => 10,
            'order'   => 'DESC',
            'post__not_in'   => [ $post_id , 116827 ],
            'meta_key'   => 'post_views_count',
            'orderby'   => 'meta_value_num',
            'date_query' => [ 'after' => '2 weeks ago' ]
        ]);
    
        show_popular_posts($query);
    }

function show_popular_posts($query)
{
    while ($query->have_posts()) {
        $query->the_post();
        $title = get_the_title();
        if (strpos($title, ':')) {
            $vtitle = explode(':', $title);
            $vtitle[0].= ':';
        } elseif (strpos($title, '.')) {
            $vtitle = explode('.', $title);
            $vtitle[0].= '.';
        } else {
            $vtitle[0] = $title;
        } ?>
      <div class="item">
        <a href="<?php the_permalink(); ?>" class="default-link">
          <span class="img-wrapper">
            <img src="<?php echo kama_thumb_src('w=380 &h=210 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo get_the_title($currpost->ID); ?>">
          </span>
          <span><strong><?php echo $vtitle[0]; ?></strong> <?php if (count($vtitle)==2) {
            echo '<br>'.$vtitle[1];
        } ?></span>
        </a>
      </div>
      <?php
      $vtitle[1]='';
    }
    wp_reset_postdata();
}

function og_change($img)
{
    $og = '/((https?:)?\/\/5koleso.ru)*\/image\/gA(\S)+.jpg/';
    $img = preg_replace($og, '', $img);
    return $img;
}
add_filter('wpseo_opengraph_image', 'og_change', 10, 1);

function json_change($data)
{
    if ((isset($data['@type'])) && ($data['@type'] == 'ImageObject')) {
        $data = array();
    }
    return $data;
}
//add_filter( 'wpseo_json_ld_output', 'json_change', 10, 1 );
// fix
function remove_page_from_query_string($query_string)
{
    if (isset($query_string['name']) && $query_string['name'] == 'page' && isset($query_string['page'])) {
        unset($query_string['name']);
        // 'page' in the query_string might look like '/2', so i'm spliting it out
        $pagePart = explode('/', $query_string['page']);
        $query_string['paged'] = end($pagePart);
    }
    return $query_string;
}
// I will kill you if you remove this. I died two days for this line
add_filter('request', 'remove_page_from_query_string');

// following are code adapted from Custom Post Type Category Pagination Fix by jdantzer
function fix_category_pagination($qs)
{
    if (isset($qs['category_name']) && isset($qs['paged'])) {
        $qs['post_type'] = get_post_types(array(
            'public'   => true,
            '_builtin' => false
        ));
        array_push($qs['post_type'], 'post');
    }
    return $qs;
}
add_filter('request', 'fix_category_pagination');
//Embed Video Fix
function add_secure_video_options($html)
{
    if (strpos($html, "<iframe") !== false) {
        $search = array('src="http://www.youtu','src="http://youtu');
        $replace = array('src="https://www.youtu','src="https://youtu');
        $html = str_replace($search, $replace, $html);

        return $html;
    } else {
        return $html;
    }
}
add_filter('the_content', 'add_secure_video_options', 10);


function image_alt_title($content)
{
    if (is_singular()) {
        global $post;
        if (isset($GLOBALS['currpost'])) {
            $post = $GLOBALS['currpost'];
        }
       
        global $page_title;
        if ($page_title) {
            $alt_text = $page_title;
        } elseif ($post) {
            $alt_text = get_the_title($post->ID);
        } else {
            $alt_text = '...';
        }

        if ($post) {
            $content = preg_replace('@ alt=""@', ' alt="'. $alt_text .'" ', $content);
            $content = preg_replace('@ alt="."@', ' alt="..." ', $content);
        }

        preg_match_all('@<img[^>]+>@simU', $content, $imgs);
        if ($imgs) {
            foreach ($imgs[0] as $img) {
                if (false === stristr($img, 'alt=')) {
                    $content = str_replace($img, preg_replace('@>$@', ' alt="'. $alt_text .'">', $img), $content);
                }
            }
        }
    }
    return $content;
}



add_filter('the_content', 'image_alt_title');

/*
function rss_post_thumbnail($content) {
    global $post;

    $img_main = get_field('img_main', $post->ID );
    if( $img_main ) {
        if( is_numeric( $img_main )) {
            $featured_image = wrap_kama_image(get_field('img_main'), 'w=1200 &h=580 &crop=center');
        } else {
            $featured_image = $img_main;
        }

        $content = '<p><img src="'. $featured_image . '" alt="'. get_the_title() .'"></p>' . $content;
    }

    return $content;
}

add_filter('the_content_feed', 'rss_post_thumbnail');*/

    
add_theme_support('responsive-embeds');
//fix shortlink ?p=97082
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);



    function pg_url($n)
    {
        $rePg = '@page/\d+/@sim';
        $aUrl = explode('?', $_SERVER['REQUEST_URI']);
        $clUrl = preg_replace($rePg, '', $aUrl[0]);

        $getPars = (isset($aUrl[1]) ? '?' . $aUrl[1] : '');
        $url = rtrim($clUrl, '/') . '/page/' . $n . '/';

        $url = str_replace('/page/1/', '/', $url);
        $url = preg_replace('@/+@', '/', $url);
        return $url  . $getPars;
    }



add_action('init', 'mailru_feed');
function mailru_feed()
{
    add_feed('mailrufeed', 'mailRuRssFeed');
}
 
function mailRuRssFeed()
{
    $recentDays = 3;
    $postCount = 30; // The number of posts to show in the feed
    $postType = 'post'; // post type to display in the feed
    query_posts(array( 'post_type' => $postType, 'showposts' => $postCount ));
    $charset = get_option('blog_charset');
    header('Content-Type: ' . feed_content_type('rss-http') . '; charset=' . $charset, true);

    echo '<?xml version="1.0" encoding="' . $charset . '"?>'; ?>
<rss version="2.0" 
xmlns:content="http://purl.org/rss/1.0/modules/content/" 
xmlns:wfw="http://wellformedweb.org/CommentAPI/" 
xmlns:dc="http://purl.org/dc/elements/1.1/" 
xmlns:atom="http://www.w3.org/2005/Atom" 
xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" 
xmlns:slash="http://purl.org/rss/1.0/modules/slash/" 
<?php do_action('rss2_ns'); ?>>
<channel>
<title><?php bloginfo_rss('name'); ?> Новости</title>
<atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
<link><?php bloginfo_rss('url'); ?></link>
<description><?php bloginfo_rss('description'); ?></description>
<lastBuildDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_lastpostmodified('GMT'), false); ?></lastBuildDate>
<language><?php echo get_option('rss_language'); ?></language>
<sy:updatePeriod><?php echo apply_filters('rss_update_period', 'hourly'); ?></sy:updatePeriod>
<sy:updateFrequency><?php echo apply_filters('rss_update_frequency', '1'); ?></sy:updateFrequency>
<?php do_action('rss2_head'); ?>
<?php while (have_posts()) {
        the_post();

        $posttime = get_post_time('Y-m-d H:i:s', true);
        $postimage =  kama_thumb_src('w=875 &h=440 &crop=center', get_field("img_intro", get_the_ID()));
        if (! $postimage || time() - strtotime($posttime) > 3600 * 24 * $recentDays) {
            continue;
        }

        if (! get_the_excerpt()) {
            continue;
        } ?>
<item>
<title><?php the_title_rss(); ?></title>
<link><?php the_permalink_rss(); ?></link>
<enclosure url="<?php echo esc_url($postimage); ?>" type="image/jpeg" /> 
<pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', $posttime, false); ?></pubDate>
<dc:creator><?php the_author(); ?></dc:creator>
<guid isPermaLink="false"><?php the_guid(); ?></guid>
<description><![CDATA[<?php the_excerpt_rss(); ?>]]></description>
<content:encoded><![CDATA[<?php the_excerpt_rss(); ?>]]></content:encoded>
<?php rss_enclosure(); ?>
<?php do_action('rss2_item'); ?>
</item>
<?php
    } ?>
</channel>
</rss>
 
<?php
    
    wp_reset_query();
}

function getNewsBlock()
{
    global $wpdb;
    $dbnews = $wpdb->get_col("SELECT * FROM wp_post_blocks_item ", 6);
    return  implode(",", $dbnews);
}


function footer_enqueue_scripts()
{
    remove_action('wp_head', 'wp_print_scripts');
    remove_action('wp_head', 'wp_print_head_scripts', 9);
    remove_action('wp_head', 'wp_enqueue_scripts', 1);
    add_action('wp_footer', 'wp_print_scripts', 5);
    add_action('wp_footer', 'wp_enqueue_scripts', 5);
    add_action('wp_footer', 'wp_print_head_scripts', 5);
}
add_action('after_setup_theme', 'footer_enqueue_scripts');

?>