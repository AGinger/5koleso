<?php

$currpost = $post;
if (is_front_page() || is_home()) {
    $page_title = 'Автомобильный журнал';
} elseif (is_404()) {
    $page_title = '...';
} elseif (is_search()) {
    $page_title = 'Результат поиска по «' . htmlspecialchars($_GET['s']) . '»';
} elseif (is_archive()) {
    $page_title = get_the_archive_title();
} else {
    $page_title = get_the_title($currpost->ID);
}

?>
<footer>
  <script>
     setTimeout(function(){
    $('#agree').on('change', function() {
   $('button').prop('disabled', !this.checked);
}).change();
}, 3000);
  </script>
  <div class="container">
    <div class="flex">

      <div class="left">
        <a href="<?=get_category_link(22);?>" class="logo">
          <img src="<?=get_template_directory_uri();?>/img/logo-footer.png" alt="<?php echo $page_title; ?>">
        </a>
        <div class="magazine-wrapper">
          <?php
$query = new WP_Query([
    'post_type' => 'post',
    'posts_per_page' => 1,
    'order' => 'DESC',
    'cat' => 22,
]);
while ($query->have_posts()) {
    $query->the_post(); ?>
            <div>
              <a href="<?=get_field("archive_pdf", $post->ID); ?>" target="_blank"><img src="<?=kama_thumb_src('w=143 &h=194 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>"></a>
            </div>
            <div>
              <a href="<?=get_category_link(22); ?>" class="archive">Архив</a>
              <a href="<?=get_field("archive_pdf", $post->ID); ?>" class="d-magaz" target="_blank">Скачать новый журнал</a>
              <ul>
                <!-- <li>
                  <a rel="noreferrer noopener" href="//appsto.re/ru/EB9_5.i"><img src="<?=get_template_directory_uri(); ?>/img/app-a.png" alt="<?php echo $page_title; ?>"></a>
                </li>
                <li>
                  <a rel="noreferrer noopener" href="//play.google.com/store/apps/details?id=reklamotiv.koleso5"><img src="<?=get_template_directory_uri(); ?>/img/app-ii.png" alt="<?php echo $page_title; ?>"></a>
                </li> -->
              </ul>
            </div>
            <?php
}
wp_reset_postdata();
?>
        </div>
        <p><a href="<?=get_page_link(16941);?>" style="color:#ababab">Рекламодателям Журнал &laquo;5 Колесо&raquo;</a></p>
        <a href="https://ru.depositphotos.com/bgremover.html" rel="nofollow">
        <img  style="margin-top: 20px;" src="/wp-content/uploads/bkb.png" alt=""></a>
      </div>

      <!-- мобильный вид -->
      <div class="right xs">
        <a href="<?=get_category_link(28);?>" class="logo">
          <img src="<?=get_template_directory_uri();?>/img/logo-footer-2.png" alt="<?php echo $page_title; ?>">
        </a>
        <div class="magazine-wrapper">
          <?php
$query = new WP_Query([
    'post_type' => 'post',
    'posts_per_page' => 1,
    'order' => 'DESC',
    'cat' => 28,
]);
while ($query->have_posts()) {
    $query->the_post(); ?>
            <div>
              <a href="<?=get_field("archive_pdf", $post->ID); ?>" target="_blank"><img src="<?=kama_thumb_src('w=143 &h=194 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>"></a>
            </div>
            <div>
              <a href="<?=get_category_link(28); ?>" class="archive">Архив</a>
              <a href="<?=get_field("archive_pdf", $post->ID); ?>" class="d-magaz" target="_blank">Скачать новый журнал</a>
              <!-- <ul>
                <li><a href="#"><img src="<?=get_template_directory_uri(); ?>/img/app-a.png" alt="<?php echo $page_title; ?>"></a></li>
                <li><a href="#"><img src="<?=get_template_directory_uri(); ?>/img/app-ii.png" alt="<?php echo $page_title; ?>"></a></li>
              </ul> -->
            </div>
            <?php
}
wp_reset_postdata();
?>
        </div>
        <p><a href="<?=get_page_link(16943);?>" style="color:#ababab">Рекламодателям Журнал &laquo;Автопарк&raquo;</a></p>
      </div>
      <!-- ./ мобильный вид -->
      <div class="center">
        <div class="socials">
          <p>Присоединяйтесь к нам:</p>
          <ul>
            <li>
              <a href="//instagram.com/5koleso.ru/" target="_blank" rel="noreferrer noopener">
                <svg xmlns="//www.w3.org/2000/svg" xmlns:xlink="//www.w3.org/1999/xlink" width="18pt" height="18pt" viewBox="0 0 18 18" version="1.1">
                  <path style=" stroke:none;fill-rule:nonzero;fill:rgb(171, 171, 171);fill-opacity:1;" d="M 13.039062 0 L 4.96875 0 C 2.230469 0 0 2.230469 0 4.96875 L 0 13.039062 C 0 15.777344 2.230469 18.007812 4.96875 18.007812 L 13.039062 18.007812 C 15.777344 18.007812 18.007812 15.777344 18.007812 13.039062 L 18.007812 4.96875 C 18.007812 2.230469 15.777344 0 13.039062 0 Z M 16.410156 13.039062 C 16.410156 14.898438 14.898438 16.410156 13.039062 16.410156 L 4.96875 16.410156 C 3.109375 16.410156 1.597656 14.898438 1.597656 13.039062 L 1.597656 4.96875 C 1.597656 3.109375 3.109375 1.597656 4.96875 1.597656 L 13.039062 1.597656 C 14.894531 1.597656 16.410156 3.109375 16.410156 4.96875 Z M 16.410156 13.039062 "/>
                  <path style=" stroke:none;fill-rule:nonzero;fill:rgb(171, 171, 171);fill-opacity:1;" d="M 9.003906 4.363281 C 6.445312 4.363281 4.363281 6.445312 4.363281 9.003906 C 4.363281 11.5625 6.445312 13.644531 9.003906 13.644531 C 11.5625 13.644531 13.644531 11.5625 13.644531 9.003906 C 13.644531 6.445312 11.5625 4.363281 9.003906 4.363281 Z M 9.003906 12.046875 C 7.324219 12.046875 5.960938 10.679688 5.960938 9.003906 C 5.960938 7.324219 7.324219 5.960938 9.003906 5.960938 C 10.679688 5.960938 12.046875 7.324219 12.046875 9.003906 C 12.046875 10.679688 10.679688 12.046875 9.003906 12.046875 Z M 9.003906 12.046875 "/>
                  <path style=" stroke:none;fill-rule:nonzero;fill:rgb(171, 171, 171);fill-opacity:1;" d="M 13.835938 3.007812 C 13.53125 3.007812 13.226562 3.132812 13.011719 3.351562 C 12.792969 3.570312 12.667969 3.871094 12.667969 4.179688 C 12.667969 4.488281 12.792969 4.789062 13.011719 5.007812 C 13.226562 5.226562 13.53125 5.351562 13.835938 5.351562 C 14.148438 5.351562 14.449219 5.226562 14.667969 5.007812 C 14.882812 4.789062 15.007812 4.488281 15.007812 4.179688 C 15.007812 3.871094 14.882812 3.570312 14.667969 3.351562 C 14.449219 3.132812 14.148438 3.007812 13.835938 3.007812 Z M 13.835938 3.007812 "/>
                </svg>
              </a>
            </li>
            <li>
              <a href="//www.facebook.com/5koleso" target="_blank" rel="noreferrer noopener">
                <svg data-icon="facebook-f" aria-hidden="true" focusable="false" data-prefix="fab" class="svg-inline--fa fa-facebook-f fa-w-10" role="img" xmlns="//www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path></svg>
              </a>
            </li>
            <li>
              <a href="//vk.com/5kolesoru" target="_blank" rel="noreferrer noopener">
                <svg data-icon="vk" aria-hidden="true" focusable="false" data-prefix="fab" class="svg-inline--fa fa-vk fa-w-18" role="img" xmlns="//www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M545 117.7c3.7-12.5 0-21.7-17.8-21.7h-58.9c-15 0-21.9 7.9-25.6 16.7 0 0-30 73.1-72.4 120.5-13.7 13.7-20 18.1-27.5 18.1-3.7 0-9.4-4.4-9.4-16.9V117.7c0-15-4.2-21.7-16.6-21.7h-92.6c-9.4 0-15 7-15 13.5 0 14.2 21.2 17.5 23.4 57.5v86.8c0 19-3.4 22.5-10.9 22.5-20 0-68.6-73.4-97.4-157.4-5.8-16.3-11.5-22.9-26.6-22.9H38.8c-16.8 0-20.2 7.9-20.2 16.7 0 15.6 20 93.1 93.1 195.5C160.4 378.1 229 416 291.4 416c37.5 0 42.1-8.4 42.1-22.9 0-66.8-3.4-73.1 15.4-73.1 8.7 0 23.7 4.4 58.7 38.1 40 40 46.6 57.9 69 57.9h58.9c16.8 0 25.3-8.4 20.4-25-11.2-34.9-86.9-106.7-90.3-111.5-8.7-11.2-6.2-16.2 0-26.2.1-.1 72-101.3 79.4-135.6z"></path></svg>
              </a>
            </li>
            <li>
								<a href="//www.youtube.com/channel/UC83-asZ8Crh28RVcdqKYx0w"  rel="noreferrer noopener">
									<svg data-icon="youtube" aria-hidden="true" focusable="false" data-prefix="fab" class="svg-inline--fa fa-youtube fa-w-18" role="img" xmlns="//www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"></path></svg>
								</a>
							</li>
              <li>
                  <a href="//twitter.com/5koleso"  rel="noreferrer noopener">
                    <svg data-icon="twitter" aria-hidden="true" focusable="false" data-prefix="fab" class="svg-inline--fa fa-tw fa-w-18" role="img" xmlns="//www.w3.org/2000/svg" viewBox="0 0 576 512">
                      <path fill="currentColor" d="M512,97.248c-19.04,8.352-39.328,13.888-60.48,16.576c21.76-12.992,38.368-33.408,46.176-58.016
	c-20.288,12.096-42.688,20.64-66.56,25.408C411.872,60.704,384.416,48,354.464,48c-58.112,0-104.896,47.168-104.896,104.992
	c0,8.32,0.704,16.32,2.432,23.936c-87.264-4.256-164.48-46.08-216.352-109.792c-9.056,15.712-14.368,33.696-14.368,53.056
	c0,36.352,18.72,68.576,46.624,87.232c-16.864-0.32-33.408-5.216-47.424-12.928c0,0.32,0,0.736,0,1.152
	c0,51.008,36.384,93.376,84.096,103.136c-8.544,2.336-17.856,3.456-27.52,3.456c-6.72,0-13.504-0.384-19.872-1.792
	c13.6,41.568,52.192,72.128,98.08,73.12c-35.712,27.936-81.056,44.768-130.144,44.768c-8.608,0-16.864-0.384-25.12-1.44
	C46.496,446.88,101.6,464,161.024,464c193.152,0,298.752-160,298.752-298.688c0-4.64-0.16-9.12-0.384-13.568
	C480.224,136.96,497.728,118.496,512,97.248z"/></path></svg>
                  </a>
                </li>
          </ul>
        </div>
        <?php

wp_nav_menu([
    'theme_location' => 'bottom',
    'container' => false,
    'menu_class' => 'footer-menu',
]);
?>
        <div class="copyright">
          © 2022. 5 Колесо. Зарегистрирован в Роскомнадзоре 07.04.2021 г. номер свидетельства ЭЛ № ФС 77-80775. Учредитель — ООО «5К». Все права защищены. 16+ При полном или частичном использовании материалов сайта «5 Колесо» ссылка на 5koleso.ru обязательна. Редакция не несет ответственности за мнения, высказывания в комментариях читателей.
        </div>
      </div>


      <div class="right large">
        <a href="<?=get_category_link(28);?>" class="logo">
          <img src="<?=get_template_directory_uri();?>/img/logo-footer-2.png" alt="<?php echo $page_title; ?>">
        </a>
        <div class="magazine-wrapper">
          <?php
$query = new WP_Query([
    'post_type' => 'post',
    'posts_per_page' => 1,
    'order' => 'DESC',
    'cat' => 28,
]);
while ($query->have_posts()) {
    $query->the_post(); ?>
            <div>
              <a href="<?=get_field("archive_pdf", $post->ID); ?>" target="_blank"><img src="<?=kama_thumb_src('w=143 &h=194 &crop=center', get_field("img_intro", $post->ID)); ?>" alt="<?php echo $page_title; ?>"></a>
            </div>
            <div>
              <a href="<?=get_category_link(28); ?>" class="archive">Архив</a>
              <a href="<?=get_field("archive_pdf", $post->ID); ?>" class="d-magaz" target="_blank">Скачать новый журнал</a>
              <!-- <ul>
                <li><a href="#"><img src="<?=get_template_directory_uri(); ?>/img/app-a.png" alt="<?php echo $page_title; ?>"></a></li>
                <li><a href="#"><img src="<?=get_template_directory_uri(); ?>/img/app-ii.png" alt="<?php echo $page_title; ?>"></a></li>
              </ul> -->
            </div>
            <?php
}
wp_reset_postdata();
?>
        </div>
        <p><a href="<?=get_page_link(16943);?>" style="color:#ababab">Рекламодателям Журнал &laquo;Автопарк&raquo;</a></p>
      </div>
    </div>
  </div>
</footer>

<script src="<?=get_template_directory_uri();?>/js/jquery-2.2.4.min.js"></script>
<script src="<?=get_template_directory_uri();?>/js/owl.carousel.min.js"></script>
<script src="<?=get_template_directory_uri();?>/js/bootstrap.min.js"></script>
<script src="<?=get_template_directory_uri();?>/js/scripts.js?v=434ds123sd54"></script>
<script>
  pageParam = '<?php echo isset($_GET["pg"]) ? $_GET["pg"] : ''; ?>';
</script>

<?php wp_footer();?>


<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
  (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
  m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
  (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

  ym(46407636, "init", {
        clickmap:true,
       trackLinks:true,
       accurateTrackBounce:true,
       webvisor:true
  });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/46407636" style="position:absolute; left:-9999px;" alt="<?php echo $page_title; ?>" /></div></noscript>
<!-- /Yandex.Metrika counter -->




<!--LiveInternet counter--><script type="text/javascript" defer="defer">
document.write('<a style="display:none;" href="//www.liveinternet.ru/click" '+
'target="_blank"><img src="//counter.yadro.ru/hit?t26.6;r'+
escape(document.referrer)+((typeof(screen)=='undefined')?'':
';s'+screen.width+'*'+screen.height+'*'+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+';u'+escape(document.URL)+
';h'+escape(document.title.substring(0,150))+';'+Math.random()+
'" alt="<?php echo $page_title; ?>" title="LiveInternet: показано число посетителей за'+
' сегодня" '+
'border="0" width="88" height="15"><\/a>')
</script><!--/LiveInternet-->
<?php if (!wp_is_mobile()): ?>
  
<?php endif;?>
</body>
</html>
