<?php
/*
Plugin Name: Plugin_YT_Neyiron
Plugin URI: Plugin_YT_Neyiron
Description: Neyiron plugin Youtube
Version: 1.0
Author: keks
Author URI: Plugin_YT_Neyiron
License: GPLv2 or later
*/

class Plugin_YT_Neyiron
{
    private static $yt_neyiron_instance;

    private function __construct()
    {
        $this->constants(); // Defines any constants used in the plugin
        $this->init();   // Sets up all the actions and filters
    }

    public static function getInstance()
    {
        if (!self::$yt_neyiron_instance) {
            self::$yt_neyiron_instance = new Plugin_YT_Neyiron();
        }

        return self::$yt_neyiron_instance;
    }

    private function constants()
    {
        define('NEYIRON_VERSION', '1.0');
        define('NEYIRON_TEXT_DOMAIN', 'neyiron');
    }

    private function init()
    {
        // Register the options with the settings API
        add_action('admin_init', array( $this, 'neyiron_register_settings' ));

        // Add the menu page
        add_action('admin_menu', array( $this, 'neyiron_setup_admin' ));

        add_filter('the_content', array( $this, 'neyiron_add_like' ));
        remove_action('wp_head', 'wp_generator');
        add_action('wp_head', array( $this, 'alter_header' ));
        define('NEYIRON__PLUGIN_DIR', plugin_dir_path(__FILE__));
        require_once(NEYIRON__PLUGIN_DIR . 'class.db_init.php');
        register_activation_hook(__FILE__, 'jal_install');
    }

    public function alter_header()
    {
        if (is_single()) {
            // echo '<rel name="og:link" content="'. get_permalink() .'" />';
        }
    }

    public function neyiron_add_like($content)
    {
        if (get_option('neyiron_enable') && is_single()) {
            $content .= '';
        }

        return $content;
    }

    public function neyiron_register_settings()
    {
        register_setting('neyiron-update-options', 'neyiron_category');
        register_setting('neyiron-update-options', 'neyiron_enable2');
        register_setting('neyiron-update-options', 'neyiron_enable3');
        register_setting('neyiron-update-options', 'RDS-main-URL');
        register_setting('neyiron-update-options', 'RDS-on-URL');
    }

    public function neyiron_setup_admin()
    {
        // Add our Menu Area
        add_options_page(
            __('Plugin_YT_Neyiron', NEYIRON_TEXT_DOMAIN),
            __('Plugin_YT_Neyiron', NEYIRON_TEXT_DOMAIN),
            'administrator',
            'neyiron-settings',
            array( $this, 'neyiron_admin_page' )
        );
    }

    public function neyiron_admin_page()
    {
        ?>
		<div class="wrap">
			<div id="icon-options-general" class="icon32"></div><h2>Youtube Category Block NEYIRON</h2>
			<!-- <form method="post" action="options.php">
				<?php wp_nonce_field('neyiron-update-options'); ?>
				<table class="form-table">
					<tr valign="top">
						<th scope="row"><?php _e('Options:', NEYIRON_TEXT_DOMAIN); ?><br />
							<span style="font-size: x-small;"><?php _e('Category-main', CKWPPE_TEXT_DOMAIN); ?></span>
						</th>
						<td>
							<input type="checkbox" name="neyiron_enable" value="1" <?php if (get_option('neyiron_enable')) {?>checked="checked"<?php ;} ?> /> <?php _e('Включите  плагин 1', CKWPPE_TEXT_DOMAIN); ?>
						</td>
						<th scope="row"><?php _e('Options:', NEYIRON_TEXT_DOMAIN); ?><br />
							<span style="font-size: x-small;"><?php _e('Category-RDS', CKWPPE_TEXT_DOMAIN); ?></span>
						</th>
						<td>
							<input type="checkbox" name="neyiron_enable2" value="1" <?php if (get_option('neyiron_enable2')) {?>checked="checked"<?php ;} ?> /> <?php _e('Включите  плагин 2', CKWPPE_TEXT_DOMAIN); ?>
						</td>
						<th scope="row"><?php _e('Options:', NEYIRON_TEXT_DOMAIN); ?><br />
							<span style="font-size: x-small;"><?php _e('Category-auto', NEYIRON_TEXT_DOMAIN); ?></span>
						</th>
						<td>
							<input type="checkbox" name="neyiron_enable3" value="1" <?php if (get_option('neyiron_enable3')) {?>checked="checked"<?php ;} ?> /> <?php _e('Включите  плагин 3', CKWPPE_TEXT_DOMAIN); ?>
						</td>
					</tr>
					<tr>
						<th scope="row">
						<label for="blogname">Название сайта</label></th>
						
						<td>
						<textarea name="RDS-main-URL" id="" cols="30" rows="10" value="<?=get_option('RDS-main-URL')?>">
						<?=get_option('RDS-main-URL')?>
						</textarea>
						</td>
						</tr>
					<input type="hidden" name="action" value="update" />
					<input type="hidden" name="page_options" value="ckwppe_primary_setting" />

					<?php settings_fields('ckwppe-update-options'); ?>
				</table>
				<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
			</form> -->
      
         <style>
         body, html {
  margin: 0;
  paddin: 0;
  background: #F5F5F5;
  width: 100%;
}

.application-info-div {
  margin-left: auto !important;
  margin-right: auto !important;
  width: 800px;
  height: 120px;
  background: #FFFFFF;
  margin: 5% 0 0 0;
}

.main-connection-div {
  margin: 5px 0 0 0;
  padding: 20px;
  background: #FFFFFF;
  width:800px;
  height:350px;
  margin-left: auto !important;
  margin-right: auto !important;
}

.icon-service {
  border-right: 125px solid #F5F5F5;
  width: 100%;
  margin: 0;
  padding: 0;
}

.text-application-info-div {
  position: relative;
  top: -110px;
  left: 135px;
}

.title-text-application-info-div {
  margin: 0;
  padding: 0;
  font-family: 'Roboto', sans-serif;
  font-size: 23px;
  font-weight: 300;
  width: 82%;
}

.description-text-application-info-div {
  margin: 0;
  padding: 0;
  font-family: 'Roboto', sans-serif;
  font-weight: 300;
  font-size; 16px;
  width: 82%;
}

.application-status {
  margin: 0;
  padding: 0;
  font-family: 'Roboto', sans-serif;
  font-weight: 300;
  font-size: 13px;
}

.green-text {
  color: #2ecc71;
}

.cookies-used {
  width: 800px;
  background: rgba(0, 0, 0, 0.8);
  margin-left: auto !important;
  margin-right: auto !important;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  color: #FFF;
  margin: 5px 0 5% 0;
  font-weight: 300;
  padding: 20px;
  transition: .4s background;
  display: none;
}

.cookies-used:hover {
  background: rgba(0, 0, 0, 0.75);
}

.log-in-main-connection-div {
  width: 50%;
  left: 0;
  height: 100%;
  float: left;
}

.register-main-connection-div {
  width: 50%;
  right: 0;
  height: 100%;
  float: left;
}

.title-log-in-main-connection-div {
  font-family: 'Roboto', sans-serif;
  font-size: 24px;
  font-weight: 300;
  margin: 5px 0 0 15px;
  padding: 0;
}

.title-register-main-connection-div {
  font-family: 'Roboto', sans-serif;
  font-size: 24px;
  font-weight: 300;
  margin: 5px 0 0 15px;
  padding: 0;
}

 .main-connection-div .input-field {
   width: 80%;
   margin: 20px 0 0 30px;
 }
   
 /* label focus color */
.main-connection-div .input-field input[type=text]:focus + label {
  color: #0072ff;
  left: 0.1rem;
}

.input-field label.active {
  left: 0.1rem;
}

 /* label underline focus color */
 .main-connection-div .input-field input[type=text]:focus {
   border-bottom: 1px solid #0072ff;
   box-shadow: 0 1px 0 0 #0072ff;
 } 

/* valid color */
 .main-connection-div .input-field input[type=text].valid {
   border-bottom: 1px solid #2ecc71;
   box-shadow: 0 1px 0 0 #2ecc71;
 }

 /* invalid color */
 .main-connection-div .input-field input[type=text].invalid {
   border-bottom: 1px solid #e74c3c;
   box-shadow: 0 1px 0 0 #e74c3c;
 }

.password-forgotten {
  font-size: 13px;
  font-family: 'Roboto', sans-serif;
  font-weight: 300;
  margin: 15px 0 0 30px;
  width: 100%;
  height: 100%;
}

.remember-me-check {
  position: relative;
  top: 10px;
  margin: 0 0 0 10px;
}

.password-reset-div {
  display: none;
}

.black-complete-background {
  position: fixed;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.6);
  top: 0;
  z-index: 10000;
}

.content-password-reset-div {
  position: fixed;
  background: #F5F5F5;
  width: 40%;
  height: 240px;
  min-width: 600px;
  padding: 20px;
  top: 0;
  bottom: 0;
  margin-left: auto !important;
  margin-right: auto !important;
  left: 0;
  right: 0;
  z-index: 100000;
  margin: auto;
}

.title-content-password-reset-div {
  font-family: 'Roboto', sans-serif;
  font-size: 24px;
  font-weight: 300;
}

.content-content-password-reset-div {
  margin: 20px 0 0 0;
  width: 100%;
}

.text-content-content-password-reset-div {
  font-family: 'Roboto', sans-serif;
  font-weight: 300;
  width: 100%;
}

 .content-content-password-reset-div .input-field {
   width: 50%;
   margin: 20px 0 0 0;
 }
   
 /* label focus color */
.content-content-password-reset-div .input-field input[type=text]:focus + label {
   color: #0072ff;
}

 /* label underline focus color */
 .content-content-password-reset-div .input-field input[type=text]:focus {
   border-bottom: 1px solid #0072ff;
   box-shadow: 0 1px 0 0 #0072ff;
 } 

/* valid color */
 .content-content-password-reset-div .input-field input[type=text].valid {
   border-bottom: 1px solid #2ecc71;
   box-shadow: 0 1px 0 0 #2ecc71;
 }

 /* invalid color */
 .content-content-password-reset-div .input-field input[type=text].invalid {
   border-bottom: 1px solid #e74c3c;
   box-shadow: 0 1px 0 0 #e74c3c;
 }

.options-alert {
  position: absolute;
  bottom: -60px;
  height: 60px;
  background: #F5F5F5;
  width: 100%;
  left: 0;
  padding: 20px 20px 40px 20px;
}

.right-options-alert {
  position: absolute;
  right: 0;
}

.options-alert a {
  text-transform: uppercase;
  font-family: 'Roboto', sans-serif;
  color: #777;
  padding: 8px 20px;
  margin-right: 10px;
  border-radius: 2px;
}

.options-alert a:hover {
  background: rgba(0, 0, 0, 0.1);
}

.options-alert a:active {
  background: rgba(0, 0, 0, 0.2);
}

[type="checkbox"]:checked+label:before {
  border-right: 2px solid #0072ff;
   border-bottom: 2px solid #0072ff;
}

a {
  color: #0072ff;
}
.form {
    display: inline-block;
    width: 340px;
    height: 640px;
    background: #e6e6e6;
    border-radius: 8px;
    box-shadow: 0 0 17px -10px #000;
    margin: 100px 40px 10px 40px;
    padding: 20px 30px;
    max-width: calc(100vw - 40px);
    box-sizing: border-box;
    font-family: 'Montserrat',sans-serif;
    position: relative;
}
.h2{margin:10px 0;padding-bottom:10px;width:180px;color:#78788c;border-bottom:3px solid #78788c;text-align: left;}
input{width:100%;padding:10px;box-sizing:border-box;background:none;outline:none;resize:none;border:0;font-family:'Montserrat',sans-serif;transition:all .3s;border-bottom:2px solid #bebed2}
input:focus{border-bottom:2px solid #78788c}
.p:before{content:attr(type);display:block;margin:28px 0 0;font-size:14px;color:#5a5a5a}
textarea{ width: 100%; height: 100%;}

</style>
<?php
global $wpdb;
        $form = $wpdb->get_results("SELECT * FROM wp_neyiron_yt"); ?>
         <div class="application-info-div" id="application-info-div">
   <div class="icon-service" id="icon-service">
     <img src="https://i.gyazo.com/e979ec500531fff73e4de0eabd69dc73.png" height="120" width="120" title="Intert" class="img-icon-service">
    </div>
    
    <div class="text-application-info-div" id="text-application-info-div">
      <p class="title-text-application-info-div">Yutube | 5koleso.ru </p>
  
      <p class="description-text-application-info-div">
         Модуль предназначен  для вывода блоков видео на  Страницах: RDS | Главная | Автопарке
        </p>
      
      <p class="application-status"><span class="green-text">
        Разработано Агентством <a href="https://neyiron.ru/"> Neyiron</a>
        </span></p>
    </div>
</div>
 <div class="center" style="display: block; text-align: center;">
<form class="form" action="actionform.php" method="post">
  <h2 class="h2">Главная</h2>
  <p class="p" type="Главное  видео "><input name="ifarame" type="text" value="<?=htmlspecialchars(wp_unslash($form[0]->{'iframe'})); ?>" placeholder=""></input></p>
  <p class="p" type="Название главного видео"><input name="title"  value="<?=htmlspecialchars(wp_unslash($form[0]->{'title'})); ?>"></input></p>
  <p class="p" type="Описание главного видео"><input name="description"  value="<?=htmlspecialchars(wp_unslash($form[0]->{'description'})); ?>"></input></p>
  <p class="p" type="Видео 1"><input name="video1"  value="<?=htmlspecialchars(wp_unslash($form[0]->{'video1'})); ?>"></input></p>
  <p class="p" type="Видео 2"><input name="video2" value="<?=htmlspecialchars(wp_unslash($form[0]->{'video2'})); ?>"></input></p>
  <p class="p" type="Видео 3"><input name="video3" value="<?=htmlspecialchars(wp_unslash($form[0]->{'video3'})); ?>"></input></p>
  <input style="display: none;" name="category_name" type="hidden" value="Главная">
  <button type="submit">Сохранить</button>
</form>
<form class="form" action="actionform.php" method="post">
  <h2 class="h2">RDS</h2>
  <p class="p" type="Главное  видео "><input name="ifarame" type="text" value="<?=htmlspecialchars(wp_unslash($form[1]->{'iframe'})); ?>" placeholder=""></input></p>
  <p class="p" type="Название главного видео"><input name="title"  value="<?=htmlspecialchars(wp_unslash($form[1]->{'title'})); ?>"></input></p>
  <p class="p" type="Описание главного видео"><input name="description"  value="<?=htmlspecialchars(wp_unslash($form[1]->{'description'})); ?>"></input></p>
  <p class="p" type="Видео 1"><input name="video1"  value="<?=htmlspecialchars(wp_unslash($form[1]->{'video1'})); ?>"></input></p>
  <p class="p" type="Видео 2"><input name="video2" value="<?=htmlspecialchars(wp_unslash($form[1]->{'video2'})); ?>"></input></p>
  <p class="p" type="Видео 3"><input name="video3" value="<?=htmlspecialchars(wp_unslash($form[1]->{'video3'})); ?>"></input></p>
  <input style="display: none;" name="category_name" type="hidden" value="RDS">
  <button type="submit">Сохранить</button>
</form>
<form class="form" action="actionform.php" method="post">
  <h2 class="h2">Автопарк</h2>
  <p class="p" type="Главное  видео "><input name="ifarame" type="text" value="<?=htmlspecialchars(wp_unslash($form[2]->{'iframe'})); ?>" placeholder=""></input></p>
  <p class="p" type="Название главного видео"><input name="title"  value="<?=htmlspecialchars(wp_unslash($form[2]->{'title'})); ?>"></input></p>
  <p class="p" type="Описание главного видео"><input name="description"  value="<?=htmlspecialchars(wp_unslash($form[2]->{'description'})); ?>"></input></p>
  <p class="p" type="Видео 1"><input name="video1"  value="<?=htmlspecialchars(wp_unslash($form[2]->{'video1'})); ?>"></input></p>
  <p class="p" type="Видео 2"><input name="video2" value="<?=htmlspecialchars(wp_unslash($form[2]->{'video2'})); ?>"></input></p>
  <p class="p" type="Видео 3"><input name="video3" value="<?=htmlspecialchars(wp_unslash($form[2]->{'video3'})); ?>"></input></p>
  <input style="display: none;" name="category_name" type="hidden" value="Автопарк">
  <button type="submit">Сохранить</button>
</form>
</div>
  
</div>
		<?php
    }
}


$neyiron = Plugin_YT_Neyiron::getInstance();
