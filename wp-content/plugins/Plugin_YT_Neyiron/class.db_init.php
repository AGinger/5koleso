<?php
global $jal_db_version;
$jal_db_version = "1.0";

function jal_install()
{
    global $wpdb;
    global $jal_db_version;

    $table_name = $wpdb->prefix . "neyiron_yt";
    if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
        $sql = "CREATE TABLE " . $table_name . " (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            time bigint(11) DEFAULT '0' NOT NULL,
            category_name tinytext NOT NULL,
            title tinytext NOT NULL,
            description tinytext NOT NULL,
            iframe text NOT NULL,
            video1 text NOT NULL,
            video2 text NOT NULL,
            video3 text NOT NULL,
            UNIQUE KEY id (id)
	);";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        $rows_affected = $wpdb->insert($table_name, array( 'time' => current_time('mysql'), 'name' => $welcome_name, 'text' => $welcome_text ));
 
        add_option("jal_db_version", $jal_db_version);
    }
}
