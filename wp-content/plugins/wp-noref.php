<?php
/*
Plugin Name: WP-NoRef
Version: 0.4
Plugin URI: http://sablinov.ru/projects/wp-noref
Description: Плагин предназначен для помещения всех <strong>внешних</strong> ссылок в опубликованных на блоге постах и комментариях в тег <code>noindex</code> и добавления к ним атрибутов <code>nofollow</code> для скрытия их от поисковых систем. Таким образом можно значительно уменьшить количество внешних ссылок, что может повысить доходность от продаж ссылок, к примеру, на бирже <a href="http://www.sape.ru/r.47c4b726e6.php">Sape</a> в несколько раз..
Author: Александр Блинов
Author URI: http://sablinov.ru/
*/

$data = array(
	'wpnoref_url'		=> '',
	'wpnoref_class'		=> '',
);
$wpnoref_flash = '';

add_option('wpnoref_settings',$data,'WP-NoRef Options');

$wpnoref_settings = get_option('wpnoref_settings');

function wpnoref_is_hash_valid($form_hash) {
	$ret = false;
	$saved_hash = wpnoref_retrieve_hash();
	if ($form_hash === $saved_hash) {
		$ret = true;
	}
	return $ret;
}

function wpnoref_generate_hash() {
	return md5(uniqid(rand(), TRUE));
}

function wpnoref_store_hash($generated_hash) {
	return update_option('wpnoref_token',$generated_hash,'WP-NoRef Security Hash');
}

function wpnoref_retrieve_hash() {
	$ret = get_option('wpnoref_token');
	return $ret;
}

function wpnoref_is_authorized() {
	global $user_level;
	if (function_exists("current_user_can")) {
		return current_user_can('activate_plugins');
	} else {
		return $user_level > 5;
	}
}

function add_wpnoref_options_page() {
	if (function_exists('add_options_page')) {
		add_options_page('WP-NoRef', 'WP-NoRef', 8, basename(__FILE__), 'wpnoref_options_subpanel');
	}
}

function wpnoref_options_subpanel() {
	global $wpnoref_flash, $wpnoref_settings, $_POST, $wp_rewrite;
	if (wpnoref_is_authorized()) {
		if(isset($_POST['wpnoref_url'])) {
			if(wpnoref_is_hash_valid($_POST['token'])) {
				if (isset($_POST['wpnoref_url'])) { 
					$wpnoref_settings['wpnoref_url'] = $_POST['wpnoref_url'];
					update_option('wpnoref_settings',$wpnoref_settings);
					$wpnoref_flash = "Ваши настройки сохранены.";
				}
			} else {
				$wpnoref_flash = "Ошибка кэша безопасности.";
			} // endif wpnoref_is_hash_valid
		} // endif isset(wpnoref_url)
		if(isset($_POST['wpnoref_class'])) {
			if(wpnoref_is_hash_valid($_POST['token'])) {
				if (isset($_POST['wpnoref_class'])) { 
					$wpnoref_settings['wpnoref_class'] = $_POST['wpnoref_class'];
					update_option('wpnoref_settings',$wpnoref_settings);
					$wpnoref_flash = "Ваши настройки сохранены.";
				}
			} else {
				$wpnoref_flash = "Ошибка кэша безопасности.";
			} // endif wpnoref_is_hash_valid
		} // endif isset(wpnoref_class)
	} else {
		$wpnoref_flash = "У вас недостаточно прав доступа.";
	}
	
	if ($wpnoref_flash != '') echo '<div id="message" class="updated fade"><p>' . $wpnoref_flash . '</p></div>';
	
	if (wpnoref_is_authorized()) {
		$temp_hash = wpnoref_generate_hash();
		wpnoref_store_hash($temp_hash);
		echo '<div class="wrap">';
		echo '<h2>WP-NoRef</h2>';
		echo '<p>Плагин предназначен для помещения всех <strong>внешних</strong> ссылок в опубликованных на блоге постах и комментариях в тег <code>noindex</code> и добавления к ним атрибутов <code>nofollow</code> для скрытия их от поисковых систем. Таким образом можно значительно уменьшить количество внешних ссылок, что может повысить доходность от продаж ссылок, к примеру, на бирже <a href="http://www.sape.ru/r.47c4b726e6.php">Sape</a> в несколько раз.</p>
		<form action="" method="post">
		<input type="hidden" name="redirect" value="true" />
		<input type="hidden" name="token" value="' . wpnoref_retrieve_hash() . '" />
		<p>Перечислите здесь список доменов-исключений, которые не надо скрывать от поисковых систем через запятую.
		<br/>Например, site1.ru, site2.ru, site3.ru (без www)
		<br/><textarea name="wpnoref_url" cols="100" rows="5">'.htmlentities($wpnoref_settings['wpnoref_url']).'</textarea></p>
		<p>Перечислите здесь список классов-исключений, которые не надо скрывать от поисковых систем через запятую.
		<br/>Например, class1, class2, class3
		<br/><textarea name="wpnoref_class" cols="100" rows="5">'.htmlentities($wpnoref_settings['wpnoref_class']).'</textarea></p>
		<p><input type="submit" value="Сохранить" /></p></form>';
		echo '</div>';
	} else {
		echo '<div class="wrap"><p>Извините, у вас нет прав доступа к этой странице.</p></div>';
	}

}

function preg_callback2($matches) {
 global $wpnoref_settings;
 
  $site_url = preg_replace('@(https?://)(www.)?(.*)@i', '$3', get_option('siteurl'));
  $url_list = $wpnoref_settings['wpnoref_url'];
  if ($url_list !='')
	$url_list = $url_list.', '.$site_url;
  else
    $url_list = $site_url;

  $url = explode(':', $matches[5]);
  if (($url[0] == 'http') || ($url[0] == 'https')) {
    $before = '<noindex>'.$matches[1].' rel="nofollow"';
	$after = $matches[6].'</noindex>';

	$domens = preg_split('/[\s,]+/', $url_list);
	for ($i=0; $i<count($domens); $i++) {
		if (stripos($matches[5], $domens[$i]) !== false)
			break;
	}
	
	$class_list = $wpnoref_settings['wpnoref_class'];
	if ($class_list !='') {
		$classes = preg_split('/[\s,]+/', $class_list);
		for ($j=0; $j<count($classes); $j++) {
			if (stripos($matches[4], $classes[$j]) !== false)
				break;
		}
	}
	
    if ( is_array( $domens ) && is_array( $classes ) && $i >= count($domens) && $j >= count($classes)) {
          $matches[1] = $before;
	  $matches[6] = $after;
    }
  }
  return $matches[1].$matches[2].$matches[6];
} 

function removeLinks($content) {
  if (! is_feed()) {
   $content = preg_replace_callback('@(<a)(([^>]*class="([^>"]*)")?[^>]*href="([^>"]*)"[^>]*>.*?)(</a>)@i', "preg_callback2", $content);
   $content = preg_replace_callback('@(<a)(([^>]*class=\'([^>\']*)\')?[^>]*href=\'([^>\']*)\'[^>]*>.*?)(</a>)@i', "preg_callback2", $content);
  }
  return $content;
}

function noindexAuthorLink($author) {
  if (preg_match("~href~",$author))
	return "<noindex>".$author."</noindex>";
  else
	return $author;
} 

add_action('admin_menu', 'add_wpnoref_options_page');

remove_filter('the_content', 'wptexturize');
add_filter('the_content', 'removeLinks', 1);
remove_filter('the_excerpt', 'wptexturize');
add_filter('the_excerpt', 'removeLinks', 1);
add_filter('comment_text', 'removeLinks');
add_filter('get_comment_author_link', 'noindexAuthorLink');

?>