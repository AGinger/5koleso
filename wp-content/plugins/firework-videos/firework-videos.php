<?php
/*
Plugin Name: Short Video Gallery by Firework
Description: Enables infinite storytelling using short videos across the internet
Version:     2.0.7
Author:      Loop Now Technologies, Inc.
Plugin URI:  https://fireworktv.com/
Author URI:  https://fireworktv.com/
Text Domain: firework-videos
License:     GPL-2.0+
Domain Path: /languages

 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

//define some essentials
if ( !defined('FIREWORK_VIDEOS_SLUG' ) ) {
	define( 'FIREWORK_VIDEOS_SLUG', 'firework-videos' );
	define( 'FIREWORK_VIDEOS_NAMESPACE', 'Firework_Videos' );
	define( 'FIREWORK_VIDEOS_DIR', __DIR__ );
	define( 'FIREWORK_VIDEOS_PATH', plugin_dir_path( __FILE__ ) );
	define( 'FIREWORK_VIDEOS_URL', plugin_dir_url( __FILE__ ) );
	define( 'FIREWORK_VIDEOS_FILE', __FILE__ );
	define( 'FIREWORK_VIDEOS_VERSION', '2.0.7' );
	define( 'FIREWORK_VIDEOS_MIN_PHP', '5.4.0' ); // Minimum of PHP 5.4 required for autoloading, namespaces, etc
	define( 'FIREWORK_VIDEOS_MIN_WP', '5.0.0' );  // Minimum of WordPress 4.4 required
}

//include common global functions
require_once( FIREWORK_VIDEOS_PATH . 'includes/functions.php' );

//check minimum requirements before loading the plugin
if ( require_once FIREWORK_VIDEOS_PATH . 'includes/startup-checks.php' ) {

	//start autoloader
	require_once( FIREWORK_VIDEOS_PATH . 'vendor/autoload.php' );

	spl_autoload_register( 'firework_videos_autoloader' );

	//hook in activation
	register_activation_hook( __FILE__, array( 'Firework_Videos\Activation', 'activate' ) );

	//start the plugin!
	new Firework_Videos\Init();
}
