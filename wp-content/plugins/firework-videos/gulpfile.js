const gulp = require('gulp'),
	gutil = require( 'gulp-util' ),
	filter = require("gulp-filter"),
	notify = require("gulp-notify"),
	sourcemaps = require('gulp-sourcemaps'),
	plumber = require('gulp-plumber'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	minifyCss = require('gulp-clean-css'),
	babel = require('gulp-babel'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	livereload = require('gulp-livereload');

const library = './library',
paths = {
	scss: {
		src: library+'/scss/*.scss',
		dest: library+'/css',
		watch: library+'/scss/**/*.scss'
	},
	scripts: {
		watch: library+'/js/**/*.js'
	},
	developmentScripts: library+'/scripts',
	productionScripts: library+'/scripts/production',
};


gulp.task('movescripts', () => {
	return gulp.src([
		'../**/library/scripts/production/*.js'
	])
		.pipe(rename({ dirname: '' }))
		.pipe(gulp.dest(paths.developmentScripts));
});

gulp.task('sass', () => {
	return gulp.src(paths.scss.src)
		.pipe(sourcemaps.init())
		.pipe(plumber())
		.pipe(sass({
			errLogToConsole: false
		}))
		.on('error', function (err) {
			notify().write(err);
			this.emit('end');
		})
		.pipe(autoprefixer())
		.pipe(minifyCss())
		.pipe(livereload())
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest(paths.scss.dest));
});

function buildJS(taskname, source, filename ) {
	gulp.task(taskname, () => {
		return gulp.src(source)
			.pipe(plumber())
			.pipe(sourcemaps.init())
			.pipe(babel({
				presets: ['@babel/env']
			}))
			.pipe(concat(filename))
			.pipe(rename({ suffix: '.min' }))
			.pipe(gulp.dest(paths.developmentScripts))
			.pipe(uglify())
			.pipe(livereload())
			.pipe(gulp.dest(paths.productionScripts));
	});
}

// Build Scripts
buildJS('admin', library+'/js/admin.js', 'admin.js' );
buildJS('wizard', library+'/js/wizard.js', 'wizard.js' );

gulp.task('watch', () => {
	const watch = [
		paths.scss.watch,
		paths.scripts.watch
	];

	livereload.listen(35729);
	gulp.watch(watch, gulp.series('build'));
});


gulp.task('build', gulp.series('sass', 'admin', 'wizard'));
gulp.task('default', gulp.series('build', gulp.parallel('watch')));
gulp.task('production', gulp.series('build', 'movescripts'));
gulp.task('deploy', gulp.series('build', 'movescripts'));

var buildInclude = [ '**/*', '!package*.json', '!./{node_modules,node_modules/**/*}', '!./{dist,dist/**/*}', '!./{library/css,library/scss/**/*}', '!./{library/js,library/js/**/*}' ],
	zip = require('gulp-zip'),
	packageJSON = require('./package.json'), fileName = packageJSON.name, fileVersion = packageJSON.version;

gulp.task('zip', function () {
	return gulp.src( buildInclude, {base: './'})
		.pipe(zip(fileName + '.v' + fileVersion + '.zip'))
		.pipe(gulp.dest('dist/'))
		.pipe(notify({message: 'Zip task complete', onLast: true}));
});