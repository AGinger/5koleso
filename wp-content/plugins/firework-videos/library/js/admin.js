/* eslint-disable no-undef */
/*eslint-env jquery*/
const FW = {
	cache : {},
	activeCategories : 0,
	postTitle : '',
	updateCache : function() {
		FW.cache = {
			body: 	jQuery('body'),
			settings: jQuery('#fireworks_admin_settings'),
			postFeedOptions : jQuery('#firework_video_feed_settings'),
			postPublishButton : jQuery('#publish'),
			postTitle : jQuery('#title'),
			postCategoryCheckbox : jQuery('.js-post-category'),
			postCategoriesWrapper : jQuery('#fireworks_video_categories')
		};

		FW.postTitle = FW.cache.postTitle.val();
		FW.activeCategories = FW.cache.postCategoriesWrapper.attr('data-categories-count');
	},
	init : function() {
		'use strict';

		FW.updateCache();

		FW.cache.settings.on('click', '.js-sync-videos', function () {
			FW.sync( jQuery(this) );
		});

		FW.cache.settings.on('click', '.js-clear-auth', function () {
			var message = jQuery(this).data('confirm');
			if ( message && confirm( message ) ) {
				FW.clearAuth(jQuery(this));
			}
		});


		// On video feed post edit load
		// -----------------------------------------------------------
		FW.cache.postPublishButton.attr('disabled', true);
		// Validate the post
		FW.validatePostFields();


		// On Title field update
		// -----------------------------------------------------------
		FW.cache.postTitle.on('input', function () {
			var $el 	= jQuery(this),
				value 	= $el.val();

			FW.postTitle = value;
			FW.validatePostFields();
		});

		// On Category choices
		// -----------------------------------------------------------
		FW.cache.postCategoryCheckbox.on('input', function () {
			var $el 	= jQuery(this),
				checked = $el.is(':checked');

			if( checked ) {
				FW.activeCategories++;
			} else {
				FW.activeCategories--;
			}
			FW.validatePostFields();
		});


		// Field inputs - Add new feed page
		// -----------------------------------------------------------
		FW.cache.postFeedOptions.on('input', '.js-video-count', function () {
			var $el 	= jQuery(this),
				name 	= $el.attr('name'),
				value 	= $el.val(),
				checked = $el.is(':checked');

			switch (name) {
				case '_video_feed_settings[video_count]':
					FW.clearInputText("_video_feed_settings[video_count_custom]");
				break;

				case '_video_feed_settings[video_count_custom]':
					if( value.length > 3 ) {
						value = value.slice(0,3);
					}
					FW.setInputText(name, value)
					FW.clearCheckboxes("_video_feed_settings[video_count]")
				break;
			}

		});

	},
	validatePostFields : function () {
		let disabled = false;

		if(FW.postTitle === '') {
			disabled = true;
		} else {
			if(FW.activeCategories > 0) {
				disabled = false;
			} else {
				disabled = true;
			}
		}

		if(disabled) {
			FW.cache.postPublishButton.attr('disabled', true);
		} else {
			FW.cache.postPublishButton.attr('disabled', false);
		}
	},
	setInputText : function(input, value) {
		return jQuery('input[name="'+input+'"]').val(value);
	},
	clearCheckboxes : function(input) {
		jQuery('input[name="'+input+'"]').attr('checked', false);
	},
	clearInputText : function(input) {
		jQuery('input[name="'+input+'"]').val('');
	},
	clearAuth : function($button) {
		var data = {
			'action' : 'firework_videos_settings_clear_auth',
			'firework_videos_settings_clear_auth_nonce' : jQuery('#firework_videos_settings_clear_auth_nonce').val(),
			'_wp_http_referer' : encodeURIComponent( jQuery('input[name="_wp_http_referer"]').val() )
		};

		FW.cache.settings.attr('data-admin-is-loading', true);
		$button.attr('disabled', true);

		jQuery.ajax({
			type: "POST",
			url: ajaxurl,
			data: data,
			success: function() {
				location.reload();
			}
		});
	},
	sync : function($button) {
		var data = {
				'action' : 'firework_videos_settings_sync',
				'firework_videos_settings_sync_nonce' : jQuery('#firework_videos_settings_sync_nonce').val(),
				'_wp_http_referer' : encodeURIComponent( jQuery('input[name="_wp_http_referer"]').val() )
			};

		FW.cache.settings.attr('data-admin-is-loading', true);
		$button.attr('disabled', true);


		jQuery.ajax({
			type: "POST",
			url: ajaxurl,
			data: data,
			success: function(html) {
				$button.attr('disabled', false);
				FW.cache.settings.attr('data-admin-is-loading', false);
				jQuery('#sync-videos-results').html(html);
			}
		});
	}
};

jQuery(function() {
	'use strict';
	FW.init();
});