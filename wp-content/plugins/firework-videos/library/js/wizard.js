/* eslint-disable no-undef */
/*eslint-env jquery*/
const FW = {
	panelID : 1,
	wizard : {
		feedId : '',
		name : '',
		categories : [],
		postTypes : [],
		placement : 'above',
		number : 12,
		layout : 'row'
	},
	cache : {},
	updateCache : function() {
		FW.cache = {
			body: 	jQuery('body'),
			wizard: jQuery('#fireworks_wizard'),
			wizardPanelsWrapper: jQuery('#wizard-panels-wrapper')
		};

		// Update which panel to update
		FW.panelID = FW.cache.wizard.attr('data-wizard-panel');

		// For testing - set to a particular slide
		// FW.panelID = 2;
		// FW.setCurrentPanel(FW.panelID);

	},
	init : function() {
		'use strict';

		FW.updateCache();
		FW.fireWorkAuth();

		// Login to Firework
		// -----------------------------------------------------------
		FW.cache.wizard.on('click', '.js-login', function() {
			var data = {
				'action' : 'firework_videos_wizard_login',
				'firework_videos_wizard_login_nonce' : jQuery('#firework_videos_wizard_login_nonce').val(),
				'_wp_http_referer' : encodeURIComponent( jQuery('input[name="_wp_http_referer"]').val() )
			};

			jQuery(this).attr('disabled', true);

			jQuery('.js-login-redirect-msg').show();

			jQuery.ajax({
				type: "POST",
				url: firework_videos_wizard.ajaxurl,
				data: data,
				success: function(response) {
					if (response.success) {
						window.location.replace(response.data.url);
					} else {
						jQuery('.js-login-redirect-msg').text(response.data).addClass('is-error');
						jQuery(this).attr('disabled', false);
					}
				}
			});
		});


		// Next buttons
		// -----------------------------------------------------------
		FW.cache.wizard.on('click', '.js-wizard-proceed', function() {
			var $el = jQuery(this),
				data = $el.data();

			switch (data.button) {
				// case 'Existing feed':
				// 	FW.panelID++; // Skip an additional panel
				// 	FW.updateCategoriesFromApi()
				// break;

				case 'New feed':
					FW.wizard.name = '';
					FW.wizard.feedId = '';
					FW.panelID++;
				break;

				case 'Feed Setup':
					FW.wizard.name = FW.getInputText("wizard[new_feed_name]");
					FW.panelID++;
				break;

				case 'Complete - Widget':
					FW.completeWizard(data.button);
				break;

				case 'Complete - Global':
					FW.completeWizard(data.button);
				break;
			}

			FW.setCurrentPanel(FW.panelID);
		});


		// Back buttons
		// -----------------------------------------------------------
		FW.cache.wizard.on('click', '.js-wizard-back', function() {
			var $el = jQuery(this),
				data = $el.data();

			switch (data.button) {
				case 'Embed':
					FW.panelID--; // Skip an additional panel
				break;

				case 'Feed Setup' :
					FW.clearCategories();
					FW.clearExistingFeeds();
				break;
			}

			FW.panelID--;
			FW.setCurrentPanel(FW.panelID);
		});


		// Field inputs
		// -----------------------------------------------------------
		FW.cache.wizard.on('input', '.js-wizard-input', function () {
			var $el 	= jQuery(this),
				name 	= $el.attr('name'),
				value 	= $el.val(),
				checked = $el.is(':checked');

			switch (name) {
				case 'wizard[new_feed_name]':
					FW.wizard.name = value;

					if( FW.wizard.categories.length > 0 && FW.wizard.name.length > 0 ) {
						FW.enableButton("Feed Setup");
					} else {
						FW.disableButton("Feed Setup");
					}
				break;

				case 'wizard[category]':
					if( checked ) {
						FW.wizard.categories.push(value);
					} else {
						FW.wizard.categories.pop(value);
					}

					if( FW.wizard.categories.length > 0 && FW.wizard.name.length > 0 ) {
						FW.enableButton("Feed Setup");
					} else {
						FW.disableButton("Feed Setup");
					}
				break;

				case 'wizard[posttype]':
					if( checked ) {
						FW.wizard.postTypes.push(value);
					} else {
						FW.wizard.postTypes.pop(value);
					}

					if( FW.wizard.postTypes.length > 0 ) {
						FW.enableButton("Complete - Global");
					} else {
						FW.disableButton("Complete - Global");
					}

				break;

				case 'wizard[existing_feed]':
					FW.wizard.feedId = parseInt( value );
					FW.wizard.name = '';

					FW.panelID++;
					FW.panelID++; // Skip two panels
					FW.setCurrentPanel(FW.panelID);
					// FW.enableButton("Existing feed");
				break;

				case 'wizard[placement]':
					FW.wizard.placement = value;
				break;

				case 'wizard[number]':
					FW.wizard.number = value;
					FW.clearInputText("wizard[customnumber]");
				break;

				case 'wizard[customnumber]':
					if( value.length > 3 ) {
						value = value.slice(0,3);
					}
					FW.setInputText(name, value)
					FW.wizard.number = value;
					FW.clearCheckboxes("wizard[number]")
				break;

				case 'wizard[layout]':
					FW.wizard.layout = value;
				break;
			}

			console.table(FW.wizard);

		});

	},
	fireWorkAuth : function() {
		if( FW.cache.wizard.attr('data-wizard-is-loading') !== 'true' ) return false;

		var data = {
			'action' : 'firework_videos_wizard_sync',
			'firework_videos_wizard_sync_nonce' : jQuery('#firework_videos_wizard_sync_nonce').val(),
			'_wp_http_referer' : encodeURIComponent( jQuery('input[name="_wp_http_referer"]').val() )
		};

		jQuery.ajax({
			type: "POST",
			url: firework_videos_wizard.ajaxurl,
			data: data,
			success: function(html) {
				FW.cache.wizardPanelsWrapper.html(html);
				FW.cache.wizard.attr('data-wizard-is-loading', 'false');
				FW.panelID=2;
				FW.setCurrentPanel(FW.panelID);
			}
		});
	},
	setCurrentPanel : function (number) {
		FW.cache.wizard.attr('data-wizard-panel', number);
	},
	enableButton : function(button) {
		jQuery('.js-wizard-proceed[data-button="'+button+'"]').attr('disabled', false);
	},
	disableButton : function(button) {
		jQuery('.js-wizard-proceed[data-button="'+button+'"]').attr('disabled', true);
	},
	clearCheckboxes : function(input) {
		jQuery('.js-wizard-input[name="'+input+'"]').attr('checked', false);
	},
	clearInputText : function(input) {
		jQuery('.js-wizard-input[name="'+input+'"]').val('');
	},
	getInputText : function(input) {
		return jQuery('.js-wizard-input[name="'+input+'"]').val();
	},
	setInputText : function(input, value) {
		return jQuery('.js-wizard-input[name="'+input+'"]').val(value);
	},
	updateCategoriesFromApi : function() {
		FW.clearCategories();
		// Get new Categories from API
	},
	clearCategories : function () {
		FW.wizard.categories = [];
		FW.wizard.name = '';

		FW.clearCheckboxes("wizard[category]");
		FW.clearInputText("wizard[new_feed_name]");
		FW.disableButton('Feed Setup');
	},
	clearExistingFeeds : function() {
		FW.wizard.name = '';
		FW.clearCheckboxes("wizard[existing_feed]");
		FW.disableButton('Existing feed');
	},
	completeWizard : function(button) {
		var data = {
			'action' : 'firework_videos_wizard_finish',
			'firework_videos_wizard_finish_nonce' : jQuery('#firework_videos_wizard_finish_nonce').val(),
			'_wp_http_referer' : encodeURIComponent( jQuery('input[name="_wp_http_referer"]').val() ),
			'wizardData' : FW.wizard,
			'buttonAction' : button
		};

		FW.cache.wizard.attr('data-wizard-is-loading', 'true');

		jQuery.ajax({
			type: "POST",
			url: firework_videos_wizard.ajaxurl,
			data: data,
			success: function(response) {
				if (response.success) {
					window.location.replace(response.data.url);
				} else {
					//error!
					alert(response.data);
					FW.cache.wizard.attr('data-wizard-is-loading', 'false');
				}
			}
		});
	}
};

jQuery(function() {
	'use strict';
	FW.init();
});