<?php

namespace Firework_Videos;

/**
 * Firework Auto Embed logic class
 */

if ( ! class_exists( 'Firework_Videos\AutoEmbed' ) ) {

	class AutoEmbed {
		public function __construct() {
			add_action( 'firework_videos_video_feed_save', array( $this, 'after_save_determine_auto_embeds' ), 10, 2 );

			add_action( 'trashed_post', array( $this, 'after_trash_delete_determine_auto_embeds' ), 10 );
			add_action( 'untrashed_post', array( $this, 'after_trash_delete_determine_auto_embeds' ), 10 );

			add_filter( 'the_content', array( $this, 'add_feeds' ) );
		}

		/**
		 * After a video feed post is trashed, determine the new auto-embeds
		 *
		 * @param $post_id
		 */
		public function after_trash_delete_determine_auto_embeds( $post_id ) {
			if ( get_post_type( $post_id ) === FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE ) {
				$this->determine_auto_embeds();
			}
		}

		/**
		 * After feed has saved, determine all auto-embeds and save to options for easy extraction
		 *
		 * @param $post_id
		 * @param $settings
		 */
		public function after_save_determine_auto_embeds( $post_id, $settings ) {
			$this->determine_auto_embeds();
		}

		/**
		 * Determine all auto-embeds and save to options for easy extraction
		 */
		public function determine_auto_embeds() {
			$auto_embeds = array();

			//get all published feeds
			$feeds = firework_videos_get_all_feeds();

			foreach ( $feeds as $feed ) {
				if ( isset( $feed->firework_app_id ) ) {
					$placement_post_types = $feed->placement_post_types;
					if ( ! empty( $placement_post_types ) ) {
						foreach ( $placement_post_types as $post_type ) {
							$auto_embeds[ $post_type ][] = array(
								'id'         => $feed->ID,
								'placement'  => $feed->placement_location,
								'app_id'     => $feed->firework_app_id,
								'mode'       => $feed->layout,
								'max_videos' => $feed->video_count,
								'title'      => $feed->name
							);
						}
					}
				}
			}

			Options::set_auto_embeds( $auto_embeds );
		}

		public function add_feeds( $content ) {
			if ( ! in_the_loop() ) {
				return $content;
			}

			if ( ! is_singular() ) {
				return $content;
			}

			if ( ! is_main_query() ) {
				return $content;
			}

			//get all feeds that are auto-embedded
			$auto_embeds = Options::get_auto_embeds();

			foreach ( $auto_embeds as $post_type => $feeds ) {
				if ( is_singular( $post_type ) ) {

					//auto embed the feed
					firework_videos_enqueue_feed_script();

					$before = $after = '';

					foreach ( $feeds as $feed ) {
						if ( isset( $feed['app_id'] ) ) {
							if ( $feed['placement'] === 'below' ) {
								$after .= ScriptGenerator::generate( $feed['app_id'], $feed['mode'], $feed['max_videos'], '', true );
							} else {
								$before .= ScriptGenerator::generate( $feed['app_id'], $feed['mode'], $feed['max_videos'], '', 'top', true );
							}
						}
					}

					return $before . $content . $after;
				}
			}

			return $content;
		}
	}

}