<?php
namespace Firework_Videos;

/*
 * Firework Video Feed Custom Post Type Registration class
 */

if ( !class_exists( 'Firework_Videos\PostTypes' ) ) {

	class PostTypes {

		function __construct() {
			//register the post types
			add_action( 'init', array( $this, 'register' ) );

			//update post type messages
			add_filter( 'post_updated_messages', array( $this, 'update_messages' ) );

			//update post bulk messages
			add_filter( 'bulk_post_updated_messages', array( $this, 'update_bulk_messages' ), 10, 2 );
		}

		function register() {
			//allow extensions to override the gallery post type
			$args = apply_filters( 'firework_videos_video_feed_posttype_register_args',
				array(
					'labels'        => array(
						'name'               => __( 'Video Feeds', 'firework-videos' ),
						'singular_name'      => __( 'Video Feed', 'firework-videos' ),
						'add_new'            => __( 'Add Video Feed', 'firework-videos' ),
						'add_new_item'       => __( 'Add New Video Feed', 'firework-videos' ),
						'edit_item'          => __( 'Edit Video Feed', 'firework-videos' ),
						'new_item'           => __( 'New Video Feed', 'firework-videos' ),
						'view_item'          => __( 'View Video Feed', 'firework-videos' ),
						'search_items'       => __( 'Search Video Feeds', 'firework-videos' ),
						'not_found'          => __( 'No Video Feeds found', 'firework-videos' ),
						'not_found_in_trash' => __( 'No Video Feeds found in Trash', 'firework-videos' ),
						'menu_name'          => __( 'Short Video Gallery', 'firework-videos' ),
						'all_items'          => __( 'Video Feeds', 'firework-videos' )
					),
					'hierarchical'  => false,
					'public'        => false,
					'rewrite'       => false,
					'show_ui'       => true,
					'show_in_menu'  => true,
					'menu_icon'     => FIREWORK_VIDEOS_URL . 'library/svg/fireworklogo.svg',
					'supports'      => array( 'title', 'post_content' ),
				)
			);

			register_post_type( FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE, $args );
		}

		/**
		 * Customize the update messages for a video feed
		 *
		 * @global object $post     The current post object.
		 *
		 * @param array   $messages Array of default post updated messages.
		 *
		 * @return array $messages Amended array of post updated messages.
		 */
		public function update_messages( $messages ) {

			global $post;

			// Add our gallery messages
			$messages[FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE] =
				array(
					0  => '',
					1  => __( 'Video Feed updated.', 'firework-videos' ),
					2  => __( 'Video Feed custom field updated.', 'firework-videos' ),
					3  => __( 'Video Feed custom field deleted.', 'firework-videos' ),
					4  => __( 'Video Feed updated.', 'firework-videos' ),
					5  => isset($_GET['revision']) ? sprintf( __( 'Video Feed restored to revision from %s.', 'firework-videos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
					6  => __( 'Video Feed published.', 'firework-videos' ),
					7  => __( 'Video Feed saved.', 'firework-videos' ),
					8  => __( 'Video Feed submitted.', 'firework-videos' ),
					9  => sprintf( __( 'Video Feed scheduled for: <strong>%1$s</strong>.', 'firework-videos' ), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ) ),
					10 => __( 'Video Feed draft updated.', 'firework-videos' )
				);

			return $messages;

		}

		/**
		 * Customize the bulk update messages for a video feed
		 *
		 * @param array $bulk_messages Array of default bulk updated messages.
		 * @param array $bulk_counts   Array containing count of posts involved in the action.
		 *
		 * @return array mixed            Amended array of bulk updated messages.
		 */
		function update_bulk_messages( $bulk_messages, $bulk_counts ) {

			$bulk_messages[FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE] =
				array(
					'updated'   => _n( '%s Video Feed updated.', '%s Video Feeds updated.', $bulk_counts['updated'], 'firework-videos' ),
					'locked'    => _n( '%s Video Feed not updated, somebody is editing it.', '%s Video Feeds not updated, somebody is editing them.', $bulk_counts['locked'], 'firework-videos' ),
					'deleted'   => _n( '%s Video Feed permanently deleted.', '%s Video Feeds permanently deleted.', $bulk_counts['deleted'], 'firework-videos' ),
					'trashed'   => _n( '%s Video Feed moved to the Trash.', '%s Video Feeds moved to the Trash.', $bulk_counts['trashed'], 'firework-videos' ),
					'untrashed' => _n( '%s Video Feed restored from the Trash.', '%s Video Feeds restored from the Trash.', $bulk_counts['untrashed'], 'firework-videos' ),
				);

			return $bulk_messages;
		}
	}
}
