<?php
/**
 * Contains all the Global constants used throughout the plugin
 */

//Firework authentication
define( 'FIREWORK_VIDEOS_CLIENT_ID', '7ThRtKugX4jQiqEnig5Au77BZmj1BzWi' );

//URLs
define( 'FIREWORK_VIDEOS_URL_AUTH', 'https://fireworktv.com/oauth2/auth' );
define( 'FIREWORK_VIDEOS_URL_AUTH_CALLBACK', 'https://business.fireworktv.com/wp_callback' );
define( 'FIREWORK_VIDEOS_URL_API', 'https://fireworktv.com/api/' );

//options
define( 'FIREWORK_VIDEOS_OPTION_DATA', 'firework_videos_data' );

//transients
define( 'FIREWORK_VIDEOS_TRANSIENT_UPDATED', 'fv_updated' );
define( 'FIREWORK_VIDEOS_TRANSIENT_ACTIVATION_REDIRECT', 'fv_activation_redirect' );
define( 'FIREWORK_VIDEOS_TRANSIENT_GUEST_ACCESS_TOKEN', 'fv_guest_token' );
define( 'FIREWORK_VIDEOS_TRANSIENT_LOGIN_ACCESS_TOKEN', 'fv_login_token' );

//Custom Post Types
define( 'FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE', 'firework_video_feed' );
define( 'FIREWORK_VIDEOS_VIDEO_FEED_META_SETTINGS', '_video_feed_settings' );
define( 'FIREWORK_VIDEOS_VIDEO_FEED_META_ID' , '_video_firework_id' );      //The ID of the feed stored in Firework e.g. EoQpR5
define( 'FIREWORK_VIDEOS_VIDEO_FEED_META_UID', '_video_firework_uid' );  //The app ID or UID of the feed stored in Firework e.g. 7jX4Ybjt3Bh1R3GJaycpHSaSonDd_D1e
define( 'FIREWORK_VIDEOS_VIDEO_FEED_META_DEBUG', '_video_feed_debug' );

//Other
define( 'FIREWORK_VIDEOS_WIZARD_PAGE', 'firework-videos-wizard' );
define( 'FIREWORK_VIDEOS_DEFAULT_FEED_LAYOUT_MODE', 'pinned' );