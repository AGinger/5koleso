<?php

namespace Firework_Videos;

use DateTime;

/**
 * Options Class for storing and retrieving common data
 */

if ( ! class_exists( 'Firework_Videos\Options' ) ) {

	class Options {

		/**
		 * Get the main option from the WP options table
		 * @return bool|mixed|void
		 */
		public static function get_option() {
			return get_option( FIREWORK_VIDEOS_OPTION_DATA );
		}

		/**
		 * Set the main option
		 *
		 * @param $option
		 */
		public static function set_option( $option ) {
			update_option( FIREWORK_VIDEOS_OPTION_DATA, $option );
		}

		/**
		 * Returns true if we have auth data stored
		 * @return bool
		 */
		public static function has_auth() {
			return ! empty( self::get_access_token() );
		}

		/**
		 * Get auth
		 * @return mixed
		 */
		public static function get_access_token() {
			return self::get_transient( FIREWORK_VIDEOS_TRANSIENT_LOGIN_ACCESS_TOKEN, false );
		}

		/**
		 * Get auth expiry
		 * @return mixed
		 */
		public static function get_expires_in() {
			$option = Options::get_option();

			return firework_videos_get_from_array( 'expires_in', $option, false );
		}

		/**
		 * Clears the auto data
		 */
		public static function clear_auth() {
			$option = Options::get_option();
			unset( $option['access_token'] );
			unset( $option['expires_in'] );
			Options::set_option( $option );

			self::delete_transient( FIREWORK_VIDEOS_TRANSIENT_LOGIN_ACCESS_TOKEN );
		}

		/**
		 * Set auth data back to the option table
		 *
		 * @param $access_token
		 * @param $expires_in
		 */
		public static function set_auth( $access_token, $expires_in ) {
			$option                 = Options::get_option();
			$option['access_token'] = $access_token;
			$option['expires_in']   = $expires_in;
			Options::set_option( $option );

			self::set_transient( FIREWORK_VIDEOS_TRANSIENT_LOGIN_ACCESS_TOKEN, $access_token, $expires_in );
		}

		/**
		 * Get business id
		 * @return mixed
		 */
		public static function get_business_id() {
			$option = Options::get_option();

			return firework_videos_get_from_array( 'business_id', $option, false );
		}

		/**
		 * Set auth data back to the option table
		 *
		 * @param $business_id
		 */
		public static function set_business_id( $business_id ) {
			$option                = Options::get_option();
			$option['business_id'] = $business_id;
			Options::set_option( $option );
		}

		/**
		 * Get all categories
		 * @return mixed
		 */
		public static function get_categories() {
			$option = Options::get_option();

			return firework_videos_get_from_array( 'categories', $option, array() );
		}

		/**
		 * Set all categories back to the option table
		 *
		 * @param $categories
		 */
		public static function set_categories( $categories ) {
			$option               = Options::get_option();
			$option['categories'] = $categories;
			Options::set_option( $option );
		}

		/**
		 * Get all auto embeds
		 * @return mixed
		 */
		public static function get_auto_embeds() {
			$option = Options::get_option();

			return firework_videos_get_from_array( 'auto_embeds', $option, array() );
		}

		/**
		 * Set all auto embeds back to the option table
		 *
		 * @param $auto_embeds
		 */
		public static function set_auto_embeds( $auto_embeds ) {
			$option               = Options::get_option();
			$option['auto_embeds'] = $auto_embeds;
			Options::set_option( $option );
		}

		/**
		 * Deletes a transient in the options
		 */
		public static function delete_transient( $key ) {
			$option = Options::get_option();
			if ( array_key_exists( $key. '_TRANSIENT', $option ) ) {
				unset( $option[ $key . '_TRANSIENT' ] );
				Options::set_option( $option );
			}
		}

		/**
		 * Sets a transient in the options
		 */
		public static function set_transient( $key, $value, $seconds ) {
			$now = new DateTime();
			$option = Options::get_option();
			$option[$key . '_TRANSIENT'] = array(
				'value' => $value,
				'timestamp' => intval( $now->getTimestamp() ),
				'expires_in' => intval( $seconds )
			);
			Options::set_option( $option );
		}

		/**
		 * Gets a transient that we have stored internally
		 *
		 * @param $key
		 * @param $default
		 *
		 * @return mixed
		 */
		public static function get_transient( $key, $default ) {
			$option = Options::get_option();
			$transient = firework_videos_get_from_array( $key. '_TRANSIENT', $option, false );
			if ( $transient !== false && is_array( $transient ) ) {
				$timestamp = intval( $transient['timestamp'] );
				$expires_in = intval( $transient['expires_in'] );
				$expiry_timestamp = $timestamp + $expires_in;
				$now = new DateTime();
				$now_timestamp = $now->getTimestamp();

				//only if it has not expired then return
				if ( $now_timestamp <= $expiry_timestamp ) {
					return $transient['value'];
				} else {
					//expired! Clear it out
					unset( $option[$key. '_TRANSIENT'] );
					Options::set_option( $option );
				}
			}

			return $default;
		}
	}
}