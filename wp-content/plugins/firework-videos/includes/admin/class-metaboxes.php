<?php
namespace Firework_Videos\Admin;

use Firework_Videos\ScriptGenerator;
use Firework_Videos\VideoFeed;

/**
 * Firework Video Feed Metabox Class
 */

if ( !class_exists( 'Firework_Videos\Admin\Metaboxes' ) ) {

	class Metaboxes {

		/**
		 * Init constructor.
		 */
		function __construct() {
			//add our video feed metaboxes
			add_action( 'add_meta_boxes_' . FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE, array( $this, 'add_meta_boxes' ) );
			// Add admin stylesheet
			add_action( 'admin_enqueue_scripts', array( $this, 'add_admin_stylesheet' ) );

			//save extra post data for a video feed
			add_action( 'save_post', array( $this, 'save_video_feed' ) );
		}


		/**
		 * Adds the admin stylesheet
		 */
		public function add_admin_stylesheet() {
			global $post_type;
    		if( FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE == $post_type ) {
			    wp_enqueue_style( 'firework-videos-admin', FIREWORK_VIDEOS_URL . 'library/css/admin.css', array(), FIREWORK_VIDEOS_VERSION );

			    firework_videos_enqueue_feed_script();
			}
		}

		/**
		 * Adds the metaboxes to the video feed
		 * @param $post
		 */
		public function add_meta_boxes( $post ) {
			if( firework_videos_is_logged_in() ) {
				add_meta_box(
					FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE . '_settings',
					__( 'Video Feed Settings', 'firework-videos' ),
					array( $this, 'render_settings_metabox' ),
					FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE,
					'normal',
					'default'
				);

				add_meta_box(
					FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE . '_placement',
					__( 'Video Feed Placement', 'firework-videos' ),
					array( $this, 'render_placement_metabox' ),
					FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE,
					'normal',
					'low'
				);

				add_meta_box(
					FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE . '_preview',
					__( 'Video Feed Preview', 'firework-videos' ),
					array( $this, 'render_preview_metabox' ),
					FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE,
					'normal',
					'low'
				);
			}
			else {
				add_meta_box(
					FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE . '_wizard',
					__( 'You are not signed into Firework', 'firework-videos' ),
					array( $this, 'render_login_metabox' ),
					FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE,
					'normal',
					'low'
				);
			}



			if ( defined( 'FIREWORK_VIDEOS_DEBUG' ) && FIREWORK_VIDEOS_DEBUG === true  ) {
				add_meta_box(
					FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE . '_app_id',
					__( 'Video Feed App Id', 'firework-videos' ),
					array( $this, 'render_app_id_metabox' ),
					FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE,
					'normal',
					'low'
				);

				add_meta_box(
					FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE . '_debug',
					__( 'Video Feed Debug', 'firework-videos' ),
					array( $this, 'render_debug_metabox' ),
					FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE,
					'normal',
					'low'
				);
            }
		}

		/**
		 * Render the Settings Metabox
		 *
		 * @param $post
		 */
		public function render_settings_metabox( $post ) {
			$feed = $this->get_feed( $post );
			include FIREWORK_VIDEOS_PATH . "includes/admin/views/_metabox-settings.php";
		}

		/**
		 * Render the Placement Metabox
		 *
		 * @param $post
		 */
		public function render_placement_metabox( $post ) {
			$feed = $this->get_feed( $post );
			include FIREWORK_VIDEOS_PATH . "includes/admin/views/_metabox-placement.php";
		}

		/**
         * Render the Preview Metabox
         *
		 * @param $post
		 */
		public function render_preview_metabox( $post ) {
			$feed = $this->get_feed( $post );
			?>
			<p>
				<?php __( 'Below is a preview of what the video feed will look like on your website', 'firework-videos' ); ?>
			</p>
			<?php

            if ( !empty( $feed->firework_app_id ) ) {
	            echo ScriptGenerator::generate( $feed->firework_app_id, $feed->layout, $feed->video_count, $feed->name ); // @codingStandardsIgnoreLine
            }
		}

		/**
         * Render the metabox for the app id
         *
		 * @param $post
		 */
		public function render_app_id_metabox( $post ) {
			$feed = $this->get_feed( $post );
			?>
            <p class="mb-2 strong">
				<?php _e('Feed App Id', 'firework-videos'); ?>
            </p>
            <input class="js-wizard-input" type="text" name="firework_app_id" value="<?php echo esc_attr( $feed->firework_app_id ); ?>">
            <?php
		}

		/**
         * Render the metabox for the sign in wizard button
         *
		 * @param $post
		 */
		public function render_login_metabox( $post ) {
			include FIREWORK_VIDEOS_PATH . "includes/admin/views/_login.php";
		}

		/**
         * Render debug info about the feed
         *
		 * @param $post
		 */
		public function render_debug_metabox( $post ) {
		    $debug_response = get_post_meta( $post->ID, FIREWORK_VIDEOS_VIDEO_FEED_META_DEBUG, true );
			var_dump($debug_response);

			echo '<br /><hr /><br />';

			$feed = $this->get_feed( $post );
			var_dump($feed);
		}

		/**
		 * @param $post
		 *
		 * @return VideoFeed
		 */
		private function get_feed( $post ) {
			global $current_firework_feed;

			if ( !isset( $current_firework_feed ) || $current_firework_feed->ID !== $post->ID ) {
				$current_firework_feed = firework_videos_get_video_feed( $post );
			}

			return $current_firework_feed;
		}

		/**
         * Save the data captured for the video feed and sync with the Firework API
         *
		 * @param $post_id
		 *
		 * @return mixed
		 */
		public function save_video_feed( $post_id ) {
			// check autosave
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
				return $post_id;
			}

			//verify nonce
			$nonce = firework_videos_get_from_array( FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE . '_nonce', $_POST, '' ); // @codingStandardsIgnoreLine

			if ( !empty( $nonce ) && wp_verify_nonce( $nonce, FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE ) ) {

			    //extract data from $_POST
                $settings = firework_videos_get_from_array( FIREWORK_VIDEOS_VIDEO_FEED_META_SETTINGS, $_POST, false );
                if ( $settings !== false ) {
                    //apply a filter to let others override
	                $settings = apply_filters( 'firework_videos_video_feed_save_settings', $settings, $post_id, $_POST );
                    update_post_meta( $post_id, FIREWORK_VIDEOS_VIDEO_FEED_META_SETTINGS, $settings );
                }

				if ( defined( 'FIREWORK_VIDEOS_DEBUG' ) && FIREWORK_VIDEOS_DEBUG === true ) {
				    $firework_app_id = firework_videos_get_from_array( 'firework_app_id', $_POST, false );

					if ( $firework_app_id !== false ) {
						update_post_meta( $post_id, FIREWORK_VIDEOS_VIDEO_FEED_META_UID, $firework_app_id );
					}
				}

                //get the Video Feed Object
                $feed = \Firework_Videos\VideoFeed::get_by_id( $post_id );

				//Sync the feed to API
                $sync = new \Firework_Videos\Sync();
				$sync->sync_single_feed( $feed );

				do_action( 'firework_videos_video_feed_save', $post_id, $settings );
			}
		}
	}
}
