<?php

namespace Firework_Videos\Admin;

use Firework_Videos\Login;
use Firework_Videos\Sync;
use Firework_Videos\VideoFeed;
use WP_Error;

/**
 * Firework Wizard Class
 */

if ( ! class_exists( 'Firework_Videos\Admin\Wizard' ) ) {

	class Wizard {

		/**
		 * Current step
		 *
		 * @var string
		 */
		private $step = '';

		/**
		 * Steps for the setup wizard
		 *
		 * @var array
		 */
		private $steps = array();

		/**
		 * Init constructor.
		 */
		function __construct() {
			add_action( 'admin_menu', array( $this, 'admin_menus' ) );
			add_action( 'admin_init', array( $this, 'redirect_on_activation' ) );
			add_action( 'admin_init', array( $this, 'setup_wizard' ), 11 );
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

			//ajax call for doing the login
			add_action( 'wp_ajax_firework_videos_wizard_login', array( $this, 'ajax_login' ) );

			//ajax call for doing the API Sync
			add_action( 'wp_ajax_firework_videos_wizard_sync', array( $this, 'ajax_sync' ) );

			//ajax call for finishing the wizard
			add_action( 'wp_ajax_firework_videos_wizard_finish', array( $this, 'ajax_finish' ) );
		}

		/**
		 * Add admin menus/screens for wizard
		 */
		public function admin_menus() {
			add_dashboard_page( '', '', 'manage_options', FIREWORK_VIDEOS_WIZARD_PAGE, '' );
		}

		/**
		 * Do the redirect if the plugin was recently activated
		 */
		function redirect_on_activation() {
			//bail if activating from network, or bulk
			if ( is_network_admin() || isset( $_GET['activate-multi'] ) ) {
				return;
			}

			//bail if no activation redirect
			if ( ! get_transient( FIREWORK_VIDEOS_TRANSIENT_ACTIVATION_REDIRECT ) ) {
				return;
			}

			//bail if doing ajax calls, or user is not administrator
			if ( wp_doing_ajax() || ! current_user_can( 'manage_options' ) ) {
				return;
			}

			//delete the redirect transient
			delete_transient( FIREWORK_VIDEOS_TRANSIENT_ACTIVATION_REDIRECT );

			$current_page = isset( $_GET['page'] ) ? sanitize_key( wp_unslash( $_GET['page'] ) ) : false;
			//bail if we are on the wizard already
			if ( FIREWORK_VIDEOS_WIZARD_PAGE === $current_page ) {
				return;
			}

			//redirect to the wizard
			wp_safe_redirect( firework_videos_admin_wizard_url() );
			exit;
		}

		/**
		 * Returns true if the wizard is currently running
		 *
		 * @return bool
		 */
		private function wizard_is_running() {
			//bail if doing ajax calls, or user is not administrator
			if ( wp_doing_ajax() || ! current_user_can( 'manage_options' ) ) {
				return false;
			}

			if ( isset( $_POST ) && !empty( $_POST['action'] ) && 'heartbeat' === $_POST['action'] ) {
				return false;
            }

			if ( ! empty( $_GET['page'] ) && FIREWORK_VIDEOS_WIZARD_PAGE === $_GET['page'] ) {
				return true;
			}

			return false;
		}

		/**
		 * Show the setup wizard.
		 */
		public function setup_wizard() {
			if ( ! $this->wizard_is_running() ) {
				return;
			}

            Login::handle_auth_callback();

			$this->setup_wizard_steps();

			ob_start();
			$this->setup_wizard_header();
			$this->setup_wizard_content();
			$this->setup_wizard_footer();
			exit;
		}

		/**
		 * Setup the wizard steps
		 */
		private function setup_wizard_steps() {
			$this->steps = apply_filters( 'firework_videos_wizard_steps', array(
				'1' => array(
					'include' => FIREWORK_VIDEOS_PATH . 'includes/admin/views/_wizard-panel-1.php'
				),
				'2' => array(
					'include' => FIREWORK_VIDEOS_PATH . 'includes/admin/views/_wizard-panel-2.php'
				),
				'3' => array(
					'include' => FIREWORK_VIDEOS_PATH . 'includes/admin/views/_wizard-panel-3.php'
				),
				'4' => array(
					'include' => FIREWORK_VIDEOS_PATH . 'includes/admin/views/_wizard-panel-4.php'
				)
			) );

			$this->step = isset( $_GET['step'] ) ? sanitize_key( $_GET['step'] ) : current( array_keys( $this->steps ) );
		}

		/**
		 * Register/enqueue scripts and styles for the Setup Wizard.
		 *
		 * Hooked onto 'admin_enqueue_scripts'.
		 */
		public function enqueue_scripts() {
			if ( ! $this->wizard_is_running() ) {
				return;
			}

			wp_enqueue_style( FIREWORK_VIDEOS_WIZARD_PAGE, FIREWORK_VIDEOS_URL . 'library/css/wizard.css', array(
				'dashicons',
				'wp-admin'
			), FIREWORK_VIDEOS_VERSION );
			wp_register_script( FIREWORK_VIDEOS_WIZARD_PAGE, FIREWORK_VIDEOS_URL . 'library/scripts/wizard.min.js', array(
				'jquery',
				'wp-util'
			), FIREWORK_VIDEOS_VERSION, true );

			wp_localize_script(
				FIREWORK_VIDEOS_WIZARD_PAGE,
				'firework_videos_wizard',
				array(
					'ajaxurl'      => admin_url( 'admin-ajax.php' ),
					'categories'   => firework_videos_get_feed_categories(),
					'current_step' => isset( $this->steps[ $this->step ] ) ? $this->step : false,
					'i18n'         => array(
						'test' => __( 'Test Translatable String', 'firework-videos' ),
					),
				)
			);
		}

		/**
		 * Setup Wizard Header.
		 */
		public function setup_wizard_header() {
			// same as default WP from wp-admin/admin-header.php.
			set_current_screen();
			?>
            <!DOCTYPE html>
            <html <?php language_attributes(); ?>>
            <head>
                <meta name="viewport" content="width=device-width"/>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <title><?php esc_html_e( 'Firework &rsaquo; Setup Wizard', 'firework-videos' ); ?></title>
				<?php do_action( 'admin_enqueue_scripts' ); ?>
				<?php wp_print_scripts( FIREWORK_VIDEOS_WIZARD_PAGE ); ?>
				<?php do_action( 'admin_print_styles' ); ?>
				<?php do_action( 'admin_head' ); ?>
            </head>
            <body class="fireworks__wrapper <?php echo esc_attr( 'firework-videos-wizard-step__' . $this->step ); ?>"><?php
		}

		/**
		 * Setup Wizard Footer.
		 */
		public function setup_wizard_footer() {
			?>
			<?php wp_print_scripts(); ?>
            </body>
            </html><?php
		}

		/**
		 * Output the content for the current step.
		 */
		public function setup_wizard_content() {

			$feeds = firework_videos_get_all_feeds();

			$feed_count = count( $feeds );
			$is_loading = 'false';
			$hide_steps = firework_videos_get_from_array( 'auth', $_GET, '' ) === 'sync';

			if ( $hide_steps ) {
				$is_loading = 'true';
			}

			if ( $feed_count === 0 && $this->step === 2 ) {
				$this->step ++;
			} // Increase Step number so we go to add new feed, if already logged in and have existing feeds
			?>
            <div class="wizard__wrapper container" id="fireworks_wizard"
                 data-wizard-is-loading="<?php echo $is_loading ?>"
                 data-wizard-panel="<?php echo esc_attr( $this->step ); ?>">

				<?php include FIREWORK_VIDEOS_PATH . "includes/admin/views/_wizard-header.php" ?>
				<?php include FIREWORK_VIDEOS_PATH . "includes/admin/views/_wizard-breadcrumbs.php" ?>

                <main class="wizard__body">
                    <div class="wizard__panel-wrapper" id="wizard-panels-wrapper">
						<?php
						if ( $hide_steps ) {
							include FIREWORK_VIDEOS_PATH . "includes/admin/views/_wizard-loading.php";
						} else {
							$this->setup_wizard_content_steps();
						}
						?>
                    </div>
                    <div class="firework-spinner"></div>
                </main>

				<?php include FIREWORK_VIDEOS_PATH . "includes/admin/views/_wizard-footer.php" ?>

            </div>
			<?php
		}

		/**
		 * Output the wizard steps only
		 */
		private function setup_wizard_content_steps() {
			foreach ( $this->steps as $step ) {
				include $step['include'];
			}
		}

		/**
		 * Ajax handler for doing the login
		 */
		public function ajax_login() {
			if ( check_admin_referer( 'firework_videos_wizard_login', 'firework_videos_wizard_login_nonce' ) ) {

                $url = Login::build_login_url();

                if ( is_wp_error( $url ) ) {
	                wp_send_json_error( $url );
                } else if ( $url === false ) {
	                wp_send_json_error( __('Firework login failed!', 'firework-videos' ) );
                } else {
	                wp_send_json_success( array(
		                'url' => $url
	                ) );
                }
			} else {
				wp_send_json_error( __('Firework login called incorrectly - invalid nonce!', 'firework-videos' ) );
			}
		}

		/**
		 * Ajax handler for doing the full sync
		 */
		public function ajax_sync() {
			if ( check_admin_referer( 'firework_videos_wizard_sync', 'firework_videos_wizard_sync_nonce' ) ) {
			    
				//do the full sync
				$sync = new Sync();
				if ( !$sync->run_full_sync() ) {
					echo '<section id="wizard_panel_1" class="wizard__panel"></section>';
				    echo '<section id="wizard_panel_2" class="wizard__panel">';
				    echo '<h2>' . __('Something went wrong!', 'firework-videos') . '</h2>';
				    _e( 'There were some problems while trying to sync data with Firework:' ,'firework-videos' );

				    //we had some problems, we need to show this to the user
                    foreach ( $sync->errors as $error ) {
                        echo '<br />' . $error;
                    }
					echo '</section>';
					echo '<section id="wizard_panel_3" class="wizard__panel"></section>';
					echo '<section id="wizard_panel_4" class="wizard__panel"></section>';
				} else {

					//return the wizard steps HTML
					$this->setup_wizard_steps();
					$this->setup_wizard_content_steps();
				}
				die();
			} else {
				wp_die( __( 'Firework Sync Called Incorrectly!', 'firework-videos' ), __( 'Firework Sync Not Run!', 'firework-videos' ) );
			}
		}

		/**
		 * Ajax handler for finishing the wizard
		 */
		public function ajax_finish() {
			if ( check_admin_referer( 'firework_videos_wizard_finish', 'firework_videos_wizard_finish_nonce' ) ) {

			    //get the wizard object
                $wizard_state = firework_videos_get_from_array( 'wizardData', $_POST, false );

                if ( $wizard_state !== false ) {
                    //did we choose an existing feed?
	                $feed_id = intval( firework_videos_get_from_array( 'feedId', $wizard_state, 0) );

	                //create a new feed if needed
	                if ( $feed_id === 0 ) {
		                $feed_id = $this->create_new_video_feed_custom_post_type( $wizard_state );

		                $feed = VideoFeed::get_by_id( $feed_id );

		                //sync the new feed
		                $sync = new Sync();
		                if ( !$sync->sync_single_feed( $feed ) ) {
			                wp_send_json_error( __('Firework wizard failed - could not sync feed!', 'firework-videos' ) );
		                }
	                }

	                if ( $feed_id !== false ) {
		                //set the placement options on the feed
		                $settings                         = get_post_meta( $feed_id, FIREWORK_VIDEOS_VIDEO_FEED_META_SETTINGS, true );
		                $settings['placement_post_types'] = firework_videos_get_from_array( 'postTypes', $wizard_state, array() );
		                $settings['placement_location']   = firework_videos_get_from_array( 'placement', $wizard_state, '' );
		                update_post_meta( $feed_id, FIREWORK_VIDEOS_VIDEO_FEED_META_SETTINGS, $settings );

		                //update global auto-embeds for the site
		                $auto_embed = new \Firework_Videos\AutoEmbed();
		                $auto_embed->determine_auto_embeds();
	                } else {
		                wp_send_json_error( __('Firework wizard failed - could not create feed post!', 'firework-videos' ) );
	                }
                } else {
	                wp_send_json_error( __('Firework wizard finish had no data!', 'firework-videos' ) );
                }

                $button_action = firework_videos_get_from_array( 'buttonAction', $_POST, 'Complete' );
                if ( $button_action === 'Complete - Widget' ) {
	                $url = admin_url( 'widgets.php' );
                } else {
	                $url = admin_url( 'edit.php?post_type=firework_video_feed' );
                }

			    //success!
				wp_send_json_success( array(
					'url' => $url
				) );

			} else {
				wp_send_json_error( __('Firework wizard finish called incorrectly - invalid nonce!', 'firework-videos' ) );
			}
		}

		/**
         * Creates a new Video Feed Custom Post Type based on the wizard data
         *
		 * @param $wizard_state
		 *
		 * @return bool|int|WP_Error
		 */
		private function create_new_video_feed_custom_post_type( $wizard_state ) {
			$categories = firework_videos_get_from_array( 'categories', $wizard_state, array() );
			$name = firework_videos_get_from_array( 'name', $wizard_state, '' );
			$max_videos = intval( firework_videos_get_from_array('number', $wizard_state, 12 ) );
			$layout = firework_videos_get_from_array( 'layout', $wizard_state, 'row' );

			$sync = new Sync();

			$feed_id = $sync->create_feed_custom_post_type( $name );

			if ( !is_wp_error( $feed_id ) ) {
				$settings = array(
                    'category' => $categories,
                    'layout' => $layout,
                    'video_count' => $max_videos
                );

				update_post_meta( $feed_id, FIREWORK_VIDEOS_VIDEO_FEED_META_SETTINGS, $settings );

				return $feed_id;
			}

			return false;
		}
	}
}
