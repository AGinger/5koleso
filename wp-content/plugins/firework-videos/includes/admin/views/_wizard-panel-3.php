<section id="wizard_panel_3" class="wizard__panel">

	<h1>
		<?php _e('Feed Setup', 'firework-videos'); ?>
	</h1>


	<div class="components-base-control mb-3">
		<div class="components-base-control__field">
			<div class="components-base-control__field">
				<label class="components-base-control__label" for="wizard[new_feed_name]"><?php _e('Name of the feed', 'firework-videos'); ?></label>
				<input class="js-wizard-input / components-text-control__input" type="text" name="wizard[new_feed_name]" id="wizard[new_feed_name]">
			</div>
		</div>
	</div>

	<p class="mb-2 strong"><?php _e('Choose your feed categories', 'firework-videos'); ?></p>

	<ol class="optionsmenu__list columns-4">
		<?php $categories = firework_videos_get_feed_categories(); ?>
		<?php
		foreach ($categories as $key => $value) :
		?>
		<li class="optionsmenu__item">
			<label class="optionsmenu__label">
				<input class="js-wizard-input" type="checkbox" name="wizard[category]" value="<?php echo esc_attr( $value['id'] ); ?>">
				<span class="fakeinput"></span>
				<?php echo $value['display_name']; ?>
			</label>
		</li>
		<?php endforeach; ?>
	</ol>




	<?php
		// Number of Videos
		// -----------------------
	?>
	<p class="mb-1 strong"><?php _e('Max videos (Grid only)', 'firework-videos'); ?></p>
	<ol class="optionsmenu__list columns-auto align-items-center">
		<li class="optionsmenu__item">
			<label class="optionsmenu__label">
				<input checked class="js-wizard-input" type="radio" name="wizard[number]" value="12">
				<span class="fakeinput"></span>
				12
			</label>
		</li>
		<li class="optionsmenu__item">
			<label class="optionsmenu__label">
				<input class="js-wizard-input" type="radio" name="wizard[number]" value="16">
				<span class="fakeinput"></span>
				16
			</label>
		</li>
		<li class="optionsmenu__item">
			<label class="optionsmenu__label">
				<input class="js-wizard-input" type="radio" name="wizard[number]" value="24">
				<span class="fakeinput"></span>
				24
			</label>
		</li>
		<li class="optionsmenu__item">
			<label class="optionsmenu__label">
				<input class="js-wizard-input" type="radio" name="wizard[number]" value="0">
				<span class="fakeinput"></span>
				<?php _e('Infinite', 'firework-videos'); ?>
			</label>
		</li>
		<li class="optionsmenu__item">
			<input class="js-wizard-input" placeholder="Custom amount" type="number" name="wizard[customnumber]">
		</li>
	</ol>


	<?php
		// Layout
		// -----------------------
	?>
	<p class="mb-1 strong"><?php _e('Layout of your feed', 'firework-videos'); ?></p>

	<ol class="optionsmenu__list columns-4">
		<?php
		$feed_layouts = firework_videos_get_feed_layouts();
		foreach ($feed_layouts as $key => $value) :
			$checked = $key === 'row' ? ' checked="checked"' : '';
		?>
		<li class="optionsmenu__item">
			<label class="optionsmenu__label">
				<input <?php echo $checked; ?> class="js-wizard-input" type="radio" name="wizard[layout]" value="<?php echo esc_attr( $value['key'] ); ?>">
				<span class="optionsmenu__icon"></span>
				<?php echo esc_html( $value['label'] ); ?>
			</label>
		</li>
		<?php endforeach ?>
	</ol>

	<p class="wizard__actionbutton-wrapper">
		<button data-button="Feed Setup" class="js-wizard-back / wizard__actionbutton / btn btn-link"><?php _e('Back', 'firework-videos'); ?></button>

		<button data-button="Feed Setup" disabled class="js-wizard-proceed / wizard__actionbutton / btn btn-primary">
			<?php _e('Next', 'firework-videos'); ?>
		</button>
	</p>
</section>