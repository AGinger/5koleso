<?php
global $current_firework_feed;
?>

<input type="hidden" name="<?php echo FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE; ?>_nonce"
       id="<?php echo FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE; ?>_nonce"
       value="<?php echo wp_create_nonce( FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE ); ?>" />

<p class="mb-2 strong">
	<?php _e('Choose your feed categories', 'firework-videos'); ?> <span class="is-error">*</span>
</p>

<p class="mb-2">
	<?php _e('Please choose at least one category', 'firework-videos'); ?>
</p>

<ol class="optionsmenu__list columns-4" id="fireworks_video_categories" data-categories-count="<?php echo count( $current_firework_feed->categories ) ?>">
	<?php
        $categories = firework_videos_get_feed_categories();
	    foreach ($categories as $key => $value) {
	        $id = $value['id'];
            $checked = in_array( $id, $current_firework_feed->categories ) ? ' checked="checked"' : '';
    ?><li class="optionsmenu__item">
		<label class="optionsmenu__label">
			<input class="js-post-category" type="checkbox"
                   name="_video_feed_settings[category][]"
                   value="<?php echo esc_attr( $id ); ?>"
                   <?php echo $checked; ?>
            >
			<span class="fakeinput"></span>
			<?php echo esc_html( $value['display_name'] ); ?>
		</label>
	</li>
	<?php } ?>
</ol>


<?php
	// Number of Videos
	// -----------------------
    $video_count = intval( $current_firework_feed->video_count );

    $twelve_checked = ($video_count === 12) ? ' checked="checked" ' : '';
    $sixteen_checked = ($video_count === 16) ? ' checked="checked" ' : '';
    $twentyfour_checked = ($video_count === 24) ? ' checked="checked" ' : '';
    $infinite_checked = ($video_count === 0) ? ' checked="checked" ' : '';
    $custom_value = ($video_count !== 12 && $video_count !== 16 && $video_count !== 0) ? ' value="' . esc_html( $video_count ) . '" ' : '';

?>
<p class="mb-1 strong">
	<?php _e('Max videos (Grid only)', 'firework-videos'); ?> <span class="is-error">*</span>
</p>

<ol class="optionsmenu__list columns-auto align-items-center">
	<li class="optionsmenu__item">
		<label class="optionsmenu__label">
			<input class="js-video-count" type="radio" name="_video_feed_settings[video_count]" value="12"<?php echo $twelve_checked; ?>>
			<span class="fakeinput"></span>
			12
		</label>
	</li>
	<li class="optionsmenu__item">
		<label class="optionsmenu__label">
			<input class="js-video-count" type="radio" name="_video_feed_settings[video_count]" value="16"<?php echo $sixteen_checked; ?>>
			<span class="fakeinput"></span>
			16
		</label>
	</li>
	<li class="optionsmenu__item">
		<label class="optionsmenu__label">
			<input class="js-video-count" type="radio" name="_video_feed_settings[video_count]" value="24"<?php echo $twentyfour_checked; ?>>
			<span class="fakeinput"></span>
			24
		</label>
	</li>
	<li class="optionsmenu__item">
		<label class="optionsmenu__label">
			<input class="js-video-count" type="radio" name="_video_feed_settings[video_count]" value="0"<?php echo $infinite_checked; ?>>
			<span class="fakeinput"></span>
			<?php _e('Infinite', 'firework-videos'); ?>
		</label>
	</li>
	<li class="optionsmenu__item">
		<input class="js-video-count" placeholder="<?php _e('Custom amount', 'firework-videos' ); ?>"
               type="number" name="_video_feed_settings[video_count_custom]"<?php echo $custom_value; ?>>
	</li>
</ol>


<?php
	// Layout
	// -----------------------
?>
<p class="mb-1 strong">
	<?php _e('Layout of your feed', 'firework-videos'); ?> <span class="is-error">*</span>
</p>

<ol class="optionsmenu__list columns-auto">
	<?php
	$feed_layouts = firework_videos_get_feed_layouts();
	foreach ($feed_layouts as $key => $value) :
		$checked = $key === $current_firework_feed->layout ? ' checked="checked"' : '';
        ?>
	<li class="optionsmenu__item">
		<label class="optionsmenu__label">
			<input <?php echo $checked; ?> type="radio" name="_video_feed_settings[layout]" value="<?php echo esc_attr( $value['key'] ); ?>">
			<span class="optionsmenu__icon"></span>
			<?php echo esc_html( $value['label'] ); ?>
		</label>
	</li>
	<?php endforeach ?>
</ol>