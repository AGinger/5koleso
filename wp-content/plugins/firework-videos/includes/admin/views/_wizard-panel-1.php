<section id="wizard_panel_1" class="wizard__panel">
	<h1>
		<?php _e('Welcome to the Short Video Gallery by Firework', 'firework-videos'); ?>
	</h1>
	<p>
		<?php _e('Welcome to the Firework setup wizard. This wizard will help you sign in to your Firework account, or create a new account.', 'firework-videos'); ?>
	</p>
	<p>
		<?php _e('To proceed, we will redirect you to the Firework signin process. Once you are signed in, we will return you back to your website where you can start creating or embedding video feeds!', 'firework-videos'); ?>
	</p>
	<p class="text-center">
		<button class="js-login / btn btn-primary">
			<?php _e('Login or Signup', 'firework-videos'); ?>
		</button>
		<?php wp_nonce_field( 'firework_videos_wizard_login', 'firework_videos_wizard_login_nonce', false ); ?>
	</p>
	<p style="display: none" class="js-login-redirect-msg / text-center mb-0"><?php _e('Redirecting to Firework...', 'firework-videos'); ?></p>
</section>