<footer class="wizard__footer">
	<p class="text-center">
		<a href="<?php echo admin_url(); ?>"><?php _e( 'Back to Dashboard', 'firework-videos' ); ?></a>
	</p>
</footer>