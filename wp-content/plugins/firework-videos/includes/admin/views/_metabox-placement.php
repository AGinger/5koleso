<?php
global $current_firework_feed;
?>

<p><?php _e('You can automatically embed this feed across your website by choosing the options below:', 'firework-videos'); ?></p>

<?php
// Post Types
// -----------------------
?>
<p class="mb-1 strong"><?php _e('Which content to use?', 'firework-videos'); ?></p>
<ol class="optionsmenu__list columns-auto">
	<?php foreach ( firework_videos_get_custom_post_types() as $custom_post_type ) {
		$checked = in_array( $custom_post_type->name, $current_firework_feed->placement_post_types ) ? ' checked="checked"' : '';
		?>
		<li class="optionsmenu__item">
			<label class="optionsmenu__label">
				<input class="js-wizard-input" type="checkbox"
				       name="_video_feed_settings[placement_post_types][]"
				       value="<?php echo esc_attr( $custom_post_type->name ); ?>"
						<?php echo $checked; ?>
				>
				<span class="fakeinput"></span>
				<?php echo esc_html( $custom_post_type->label ); ?>
			</label>
		</li>
	<?php } ?>
</ol>

<?php
// Placement
// -----------------------
$placement_location = $current_firework_feed->placement_location;
$above_checked = ($placement_location === 'above') ? ' checked="checked" ' : '';
$below_checked = ($placement_location === 'below') ? ' checked="checked" ' : '';
?>
<p class="mb-1 strong"><?php _e('Where to place the feed', 'firework-videos'); ?></p>
<ol class="optionsmenu__list columns-auto">
	<li class="optionsmenu__item">
		<label class="optionsmenu__label">
			<input class="js-wizard-input" type="radio" name="_video_feed_settings[placement_location]" value="above"<?php echo $above_checked; ?>>
			<span class="fakeinput"></span>
			<?php _e('Above the content', 'firework-videos'); ?>
		</label>
	</li>
	<li class="optionsmenu__item">
		<label class="optionsmenu__label">
			<input class="js-wizard-input" type="radio" name="_video_feed_settings[placement_location]" value="below"<?php echo $below_checked; ?>>
			<span class="fakeinput"></span>
			<?php _e('Below the content', 'firework-videos'); ?>
		</label>
	</li>
</ol>


<?php
	// Widget Embed
	// -----------------------
?>

<p class="text-choice">
	<span>
		<?php _e('OR', 'firework-videos'); ?>
	</span>
</p>

<p><?php _e('Embed your feed in a specific location using a Widget', 'firework-videos'); ?></p>


<p class="wizard__actionbutton-wrapper">
	<a href="<?php echo firework_videos_admin_widget_url(); ?>" class="btn btn-primary">
		<?php _e('Go to Widget Options', 'firework-videos'); ?>
	</a>
</p>