<div class="wizard__breadcrumbs-wrapper">
	<ol class="wizard__breadcrumbs-list">
		<li class="wizard__breadcrumbs-item">
			<?php _e( 'Welcome', 'firework-videos' ); ?>
		</li>
		<li class="wizard__breadcrumbs-item">
			<?php _e( 'Feed Setup', 'firework-videos' ); ?>
		</li>
		<li class="wizard__breadcrumbs-item">
			<?php _e( 'Embed Feed', 'firework-videos' ); ?>
		</li>
	</ol>
</div>