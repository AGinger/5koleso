<?php

$feeds = firework_videos_get_all_feeds();

?><section id="wizard_panel_2" class="wizard__panel">
    <?php if ( count( $feeds ) > 0 ) { ?>
	<h2>
		<?php _e('Choose an Existing Feed', 'firework-videos'); ?>
	</h2>

	<ol class="optionsmenu__list columns-2">
		<?php foreach ( $feeds as $feed ) { ?>
		<li class="optionsmenu__item">
			<label class="optionsmenu__label">
				<input class="js-wizard-input" type="radio" name="wizard[existing_feed]" value="<?php echo esc_attr( $feed->ID ); ?>">
				<span class="fakeinput"></span>
				<?php
				if($feed->name === '') {
					_e('(No Title)', 'firework-videos');
				} else {
					 echo esc_html( $feed->name );
				}
				 ?>
			</label>
		</li>
		<?php } ?>

	</ol>

	<p class="text-choice">
		<span>
			<?php _e('OR', 'firework-videos'); ?>
		</span>
	</p>
    <?php } ?>

	<h2>
		<?php _e('Create a New Feed', 'firework-videos'); ?>
	</h2>

	<p class="text-center mb-0">
		<button data-button="New feed" class="js-wizard-proceed / wizard__actionbutton / btn btn-primary">
			<?php _e('Create new feed', 'firework-videos'); ?>
		</button>
	</p>
</section>