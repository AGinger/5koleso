<p>
	<?php _e('To embed video feeds into your website, you need to authenticate with Firework first. Run the setup wizard to get started.', 'firework-videos' ); ?>
</p>
<p>
	<a href="<?php echo firework_videos_admin_wizard_url(); ?>" class="btn btn-primary">
		<?php _e('Run Setup Wizard', 'firework-videos'); ?>
	</a>
</p>