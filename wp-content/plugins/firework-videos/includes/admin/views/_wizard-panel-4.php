<section id="wizard_panel_4" class="wizard__panel">


	<h2>
		<?php _e('Global Embed', 'firework-videos'); ?>
	</h2>

	<p><?php _e('Choose where you would like to place this feed across your website :', 'firework-videos'); ?></p>

	<?php
		// Post Types
		// -----------------------
	?>
	<p class="mb-1 strong"><?php _e('Which content to use?', 'firework-videos'); ?></p>
	<ol class="optionsmenu__list columns-4">
        <?php foreach ( firework_videos_get_custom_post_types() as $custom_post_type ) { ?>
		<li class="optionsmenu__item">
			<label class="optionsmenu__label">
				<input class="js-wizard-input" type="checkbox" name="wizard[posttype]" value="<?php echo esc_attr( $custom_post_type->name ); ?>">
				<span class="fakeinput"></span>
				<?php echo esc_html( $custom_post_type->label ); ?>
			</label>
		</li>
        <?php } ?>
	</ol>

	<?php
		// Placement
		// -----------------------
	?>
	<p class="mb-1 strong"><?php _e('Where to place the feed', 'firework-videos'); ?></p>
	<ol class="optionsmenu__list columns-4">
		<li class="optionsmenu__item">
			<label class="optionsmenu__label">
				<input checked class="js-wizard-input" type="radio" name="wizard[placement]" value="above">
				<span class="fakeinput"></span>
				<?php _e('Above the content', 'firework-videos'); ?>
			</label>
		</li>
		<li class="optionsmenu__item">
			<label class="optionsmenu__label">
				<input class="js-wizard-input" type="radio" name="wizard[placement]" value="below">
				<span class="fakeinput"></span>
				<?php _e('Below the content', 'firework-videos'); ?>
			</label>
		</li>
	</ol>


	<p class="wizard__actionbutton-wrapper">
		<button data-button="Complete - Global" disabled class="js-wizard-proceed / wizard__actionbutton / btn btn-primary">
			<?php _e('Finish', 'firework-videos'); ?>
		</button>
		<?php wp_nonce_field( 'firework_videos_wizard_finish', 'firework_videos_wizard_finish_nonce', false ); ?>
	</p>


	<p class="text-choice">
		<span>
			<?php _e('OR', 'firework-videos'); ?>
		</span>
	</p>

	<?php
		// Widget Embed
		// -----------------------
	?>
	<h2>
		<?php _e('Widget Embed', 'firework-videos'); ?>
	</h2>

	<p><?php _e('Embed your feed in a specific location', 'firework-videos'); ?></p>


	<p class="wizard__actionbutton-wrapper">
		<button data-button="Complete - Widget" class="js-wizard-proceed / wizard__actionbutton / btn btn-primary">
			<?php _e('Go to Widget Options', 'firework-videos'); ?>
		</button>
	</p>

	<p class="wizard__actionbutton-wrapper">
		<button data-button="Embed" class="js-wizard-back wizard__actionbutton / btn btn-link"><?php _e('Back', 'firework-videos'); ?></button>
	</p>

</section>