<?php
namespace Firework_Videos\Admin;

use Firework_Videos\Options;

/**
 * Firework_Videos Admin Settings Class
 * Adds settings page
 *
 * Date: 05/08/2020
 */

if ( !class_exists( 'Firework_Videos\Admin\Settings' ) ) {

	class Settings {

		public function __construct() {
			add_action( 'admin_menu', array( $this, 'add_settings_page' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_admin_stylesheet' ) );

			//ajax call for doing the API Sync
			add_action( 'wp_ajax_firework_videos_settings_sync', array( $this, 'ajax_sync' ) );
			add_action( 'wp_ajax_firework_videos_settings_clear_auth', array( $this, 'ajax_clear_auth' ) );
		}

		/**
		 * Ajax handler for doing the full sync
		 */
		public function ajax_sync() {
			if ( check_admin_referer( 'firework_videos_settings_sync', 'firework_videos_settings_sync_nonce' ) ) {
				//do the full sync

				$sync = new \Firework_Videos\Sync();
				if ( $sync->run_full_sync() ) {
					echo __( 'Sync Completed!', 'firework-videos' );
				} else {
					echo __( 'Sync Errors!', 'firework-videos' );
					echo '<br />';
					foreach ( $sync->errors as $error ) {
						echo $error . '<br />';
					}
				}

				die();

			} else {
				wp_die( __( 'Firework Sync Called Incorrectly!', 'firework-videos' ), __( 'Firework Sync Not Run!', 'firework-videos' ) );
			}
		}

		public function ajax_clear_auth() {
			if ( check_admin_referer( 'firework_videos_settings_clear_auth', 'firework_videos_settings_clear_auth_nonce' ) ) {
				\Firework_Videos\Options::clear_auth();
			}
		}

		/**
		 * Adds the admin stylesheet
		 */
		public function add_admin_stylesheet() {
			// global $post_type;
			if( get_current_screen()->post_type === FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE ) {
				wp_enqueue_style( 'firework-videos-admin-css', FIREWORK_VIDEOS_URL . 'library/css/admin.css', array(), FIREWORK_VIDEOS_VERSION );
				wp_enqueue_script( 'fireworks-videos-admin-js', FIREWORK_VIDEOS_URL . 'library/scripts/admin.min.js', array( 'jquery', 'wp-util' ), FIREWORK_VIDEOS_VERSION, true );
			}
		}

		public function add_settings_page() {
			add_submenu_page(
				'edit.php?post_type=' . FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE,
				__( 'Firework Settings' , 'firework-videos' ),
				__( 'Settings' , 'firework-videos' ),
				'manage_options',
				'firework-video-settings',
				array( $this, 'create_settings_page' )
			);
		}

		public function create_settings_page() {
			?>

			<div class="admin__wrapper" id="fireworks_admin_settings" data-admin-is-loading="false">
				<header class="admin__header">
					<img class="admin__header-logo" src="<?php echo FIREWORK_VIDEOS_URL; ?>library/svg/fireworklogo-white.svg" alt="<?php _e( 'Short Video Gallery by Firework', 'firework-videos' ); ?>">
				</header>

				<span class="firework-spinner"></span>

				<div class="admin__panel">

					<?php if( firework_videos_is_logged_in() ) : ?>
						<?php wp_nonce_field( 'firework_videos_settings_sync', 'firework_videos_settings_sync_nonce' ); ?>
						<h2>
							<?php _e('Sync Feeds', 'firework-videos'); ?>
						</h2>
						<p>
							<?php _e('Sync your video feeds with Firework. The sync will connect to Firework and pull all your existing video feeds into your WordPress site, so that you can easily manage them within WordPress.', 'firework-videos'); ?>
						</p>

						<div class="flex align-items-center mb-3">
							<button class="js-sync-videos / btn btn-primary mr-2">
								<?php _e('Sync', 'firework-videos'); ?>
							</button>
							<p class="mb-0 strong" id="sync-videos-results"></p>
						</div>
					<?php else  : ?>
                        <h2>
							<?php _e('You are not signed in!', 'firework-videos'); ?>
                        </h2>
						<?php
						    include FIREWORK_VIDEOS_PATH . "includes/admin/views/_login.php";
                        ?>
					<?php endif; ?>

					<hr class="mb-3">
					<h2>
						<?php _e('Visit us', 'firework-videos'); ?>
					</h2>
					<p>
						<?php _e('Having any problems? Please contact us on our website', 'firework-videos'); ?>
					</p>
					<p>
						<a target="_blank" href="https://fireworktv.com/" class="btn btn-primary">
							<?php _e('Visit Fireworktv.com', 'firework-videos'); ?>
						</a>
					</p>

					<?php if ( firework_videos_is_logged_in() ) : ?>
                        <hr class="mb-3">
                        <h2>
							<?php _e('Reset Settings', 'firework-videos'); ?>
                        </h2>
                        <p>
							<?php _e('You can reset your settings, which will clear your Firework login details.', 'firework-videos'); ?>
                        </p>
						<?php wp_nonce_field( 'firework_videos_settings_clear_auth', 'firework_videos_settings_clear_auth_nonce' ); ?>
                        <p>
                            <button class="js-clear-auth / btn btn-primary" data-confirm="<?php _e('Are you sure? You will need to authenticate again.', 'firework-videos'); ?>">
								<?php _e('Reset Settings', 'firework-videos'); ?>
                            </button>
                        </p>
					<?php endif; ?>

				</div>
			</div>

		<?php }

	}
}