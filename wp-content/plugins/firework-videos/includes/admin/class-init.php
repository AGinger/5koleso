<?php
namespace Firework_Videos\Admin;

/**
 * Admin Init Class
 * Runs all classes that need to run in the admin
 */

if ( !class_exists( 'Firework_Videos\Admin\Init' ) ) {

	class Init {

		/**
		 * Init constructor.
		 */
		function __construct() {
			new namespace\Updates();
			new namespace\Settings();
			new namespace\Metaboxes();
			new namespace\Wizard();
		}
	}
}
