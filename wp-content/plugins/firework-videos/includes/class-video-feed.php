<?php
namespace Firework_Videos;

/**
 * Firework Video Feed Class
 */

if ( !class_exists( 'Firework_Videos\VideoFeed' ) ) {

	class VideoFeed extends \stdClass {
		/**
		 * private constructor
		 *
		 * @param null $post
		 */
		private function __construct( $post = null ) {
			$this->set_defaults();

			if ( $post !== null ) {
				$this->load( $post );
			}
		}

		/**
		 *  Sets the default when a new video feed is instantiated
		 */
		private function set_defaults() {
			$this->_post = null;
			$this->ID = 0;
			$this->name = '';
			$this->categories = array();
			$this->layout = '';
			$this->video_count = 0;
			$this->placement_post_types = array();
			$this->placement_location = '';
			$this->code = '';
			$this->firework_id = '';
			$this->firework_app_id = '';
		}

		/**
		 * private video feed load function
		 * @param $post
		 */
		private function load( $post ) {
			$this->_post = $post;
			$this->ID = $post->ID;
			$this->name = $post->post_title;
			$this->code = $post->post_content;

			$meta = get_post_meta( $post->ID, FIREWORK_VIDEOS_VIDEO_FEED_META_SETTINGS, true );
			$this->categories = firework_videos_get_from_array( 'category', $meta, array() );
			$this->layout = firework_videos_get_from_array( 'layout', $meta, 'row' );
			$this->video_count = intval( firework_videos_get_from_array('video_count', $meta, 16 ) );
			$video_count_custom = firework_videos_get_from_array('video_count_custom', $meta, '' );
			if ( $video_count_custom !== '' ) {
				$this->video_count = intval( $video_count_custom );
			}
			$this->placement_post_types = firework_videos_get_from_array( 'placement_post_types', $meta, array() );
			$this->placement_location = firework_videos_get_from_array( 'placement_location', $meta, '' );

			$this->firework_id = get_post_meta( $post->ID, FIREWORK_VIDEOS_VIDEO_FEED_META_ID, true );
			$this->firework_app_id = get_post_meta( $post->ID, FIREWORK_VIDEOS_VIDEO_FEED_META_UID, true );

			do_action( 'firework_videos_video_feed_instance_after_load', $this );
		}

		/**
		 * private function to load a video feed by an id
		 * @param $post_id
		 */
		private function load_by_id( $post_id ) {
			$post = get_post( $post_id );
			if ( $post ) {
				$this->load( $post );
			}
		}

		/**
		 * Static function to load a Video Feed instance by passing in a post object
		 * @static
		 *
		 * @param $post
		 *
		 * @return VideoFeed
		 */
		public static function get( $post ) {
			return new self( $post );
		}

		/**
		 * Static function to load a Video Feed instance by post id
		 *
		 * @param $post_id
		 *
		 * @return VideoFeed | boolean
		 */
		public static function get_by_id( $post_id ) {
			$feed = new self();
			$feed->load_by_id( $post_id );
			if ( ! $feed->does_exist() ) {
				return false;
			}
			return $feed;
		}

		/**
		 * Checks if the video feed exists
		 * @return bool
		 */
		public function does_exist() {
			return $this->ID > 0;
		}
	}
}
