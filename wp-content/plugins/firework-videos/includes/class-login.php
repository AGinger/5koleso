<?php

namespace Firework_Videos;
use Firework_Videos\Api;

/**
 * Firework Login logic class
 */

if ( ! class_exists( 'Firework_Videos\Login' ) ) {

	class Login {

		/**
		 * Handle the callback logic after Firework has redirected back after the login
		 */
		public static function handle_auth_callback() {
			$access_token = firework_videos_get_from_array( 'access_token', $_GET, false );
			if ( $access_token !== false ) {
				$expires_in = firework_videos_get_from_array( 'expires_in', $_GET, false );
				\Firework_Videos\Options::set_auth( $access_token, $expires_in );
			}
		}

		/**
		 * Build up a valid Login URL that we can redirect to
		 */
		public static function build_login_url() {
			//check if we have a guest token already stored in a transient
			if ( false === ( $token_response = get_transient( FIREWORK_VIDEOS_TRANSIENT_GUEST_ACCESS_TOKEN ) ) ) {

				//call API to get guest token
				$api = new \Firework_Videos\Api\ApiSession();

				$token_response = $api->get_token();

				if ( is_wp_error( $token_response ) ) {
					return $token_response;
				} else {
					$token = firework_videos_get_from_array( 'token', $token_response, false );

					if ( $token !== false ) {
						$expires_in = intval( firework_videos_get_from_array( 'expires_in', $token_response, 0 ) );
						set_transient( FIREWORK_VIDEOS_TRANSIENT_GUEST_ACCESS_TOKEN, $token_response, $expires_in );
					} else {
						//we got a bad response from API
						return false;
					}
				}
			} else {
				$token = firework_videos_get_from_array( 'token', $token_response, false );
			}

			//The actual redirect URL we want to redirect to after login completed
			$redirect_uri = add_query_arg( 'auth', 'sync', firework_videos_admin_wizard_url() );

			return add_query_arg( array(
				'client_id'     => FIREWORK_VIDEOS_CLIENT_ID,
				'access_token'  => $token,
				'redirect_uri'  => urlencode( FIREWORK_VIDEOS_URL_AUTH_CALLBACK ),
				'response_type' => 'token',
				'state'         => urlencode( base64_encode( 'wpd=' . $redirect_uri ) )
			), FIREWORK_VIDEOS_URL_AUTH );
		}
	}
}