<?php
/**
 * Contains all the Global common functions used throughout the plugin
 */

/**
 * Custom Autoloader used throughout plugin
 *
 * @param $class
 */
function firework_videos_autoloader( $class ) {
	/* Only autoload classes from this namespace */
	if ( false === strpos( $class, FIREWORK_VIDEOS_NAMESPACE ) ) {
		return;
	}

	/* Remove namespace from class name */
	$class_file = str_replace( FIREWORK_VIDEOS_NAMESPACE . '\\', '', $class );

	/* Convert sub-namespaces into directories */
	$class_path = explode( '\\', $class_file );
	$class_file = array_pop( $class_path );
	$class_path = strtolower( implode( '/', $class_path ) );

	/* Convert class name format to file name format */
	$class_file = firework_videos_uncamelize( $class_file );
	$class_file = str_replace( '_', '-', $class_file );
	$class_file = str_replace( '--', '-', $class_file );

	/* Load the class */
	require_once FIREWORK_VIDEOS_DIR . '/includes/' . $class_path . '/class-' . $class_file . '.php';
}

/**
 * Convert a CamelCase string to camel_case
 *
 * @param $str
 *
 * @return string
 */
function firework_videos_uncamelize( $str ) {
	$str    = lcfirst( $str );
	$lc     = strtolower( $str );
	$result = '';
	$length = strlen( $str );
	for ( $i = 0; $i < $length; $i ++ ) {
		$result .= ( $str[ $i ] == $lc[ $i ] ? '' : '_' ) . $lc[ $i ];
	}

	return $result;
}

/**
 * returns a video feed object from a WP_Post object or a post ID
 *
 * @param $post
 *
 * @return Firework_Videos\VideoFeed
 */
function firework_videos_get_video_feed( $post ) {
	if ( is_a( $post, 'WP_Post' ) ) {
		return Firework_Videos\VideoFeed::get( $post );
	}

	return Firework_Videos\VideoFeed::get_by_id( $post );
}

/**
 * Safe way to get value from an array
 *
 * @param $key
 * @param $array
 * @param $default
 *
 * @return mixed
 */
function firework_videos_get_from_array( $key, $array, $default ) {
	if ( is_array( $array ) && array_key_exists( $key, $array ) ) {
		return $array[ $key ];
	} else if ( is_object( $array ) && property_exists( $array, $key ) ) {
		return $array->{$key};
	}

	return $default;
}

/**
 * Returns the Firework Setup Wizard Url within the admin
 *
 * @return string The Url to the wizard
 */
function firework_videos_admin_wizard_url() {
	return admin_url( 'index.php?page=' . FIREWORK_VIDEOS_WIZARD_PAGE );
}

/**
 * Returns the Wordpress Widgets Admin dashboard
 *
 * @return string The Url to the widgets admin dashboard
 */
function firework_videos_admin_widget_url() {
	return admin_url( 'widgets.php' );
}

/**
 * Get the list of available feed categories
 *
 * @return string[][]
 */
function firework_videos_get_feed_categories() {
	$categories = \Firework_Videos\Options::get_categories();

	if ( count( $categories ) === 0 ) {
		return array(
			array(
				'id'            => '0',
				'display_name'  => __( 'Please run sync!', 'firework-videos' ),
				'name'          => 'category_1',
				'tag_type'      => 'main_content',
				'thumbnail_url' => '',
			)
		);
	}

	return $categories;
}

/**
 * returns all public custom post types
 *
 * @return WP_Post_Type[]
 */
function firework_videos_get_custom_post_types() {
	$post_types = get_post_types( array( 'public' => true ), 'objects' );
	unset($post_types['attachment']);
	return $post_types;
}

/**
 * Enqueue the firework feed script
 * https://asset.fireworktv.com/js/fwn.js
 */
function firework_videos_enqueue_feed_script() {
	wp_enqueue_script( 'fwn_js',
		'',
		array(),
		FIREWORK_VIDEOS_VERSION, true );
}

/**
 * Gets all the available feed layouts
 *
 * @return array[]
 */
function firework_videos_get_feed_layouts() {
	return array(
		'row'    => array(
			'key'   => 'row',
			'label' => __( 'Row', 'firework-videos' ),
			'icon'  => '',
		),
		'grid'   => array(
			'key'   => 'grid',
			'label' => __( 'Grid', 'firework-videos' ),
			'icon'  => '',
		),
		'pinned' => array(
			'key'   => 'pinned',
			'label' => __( 'Pinned', 'firework-videos' ),
			'icon'  => '',
		),
	);
}

/**
 * Pulls all feeds stored
 *
 * @return Firework_Videos\VideoFeed[]
 */
function firework_videos_get_all_feeds() {
	$args = array(
		'post_type'     => FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE,
		'post_status'   => array( 'publish' ),
		'cache_results' => false,
		'nopaging'      => true,
	);

	$posts = get_posts( $args );

	if ( empty( $posts ) ) {
		return array();
	}

	$feeds = array();

	foreach ( $posts as $post ) {
		$feeds[$post->ID] = \Firework_Videos\VideoFeed::get( $post );
	}

	return $feeds;
}

/**
 * Find a specific feed using the Firework feed identifier (not the post ID)
 *
 * @return Firework_Videos\VideoFeed|bool
 */
function firework_videos_find_feed( $firework_app_id ) {
	$args = array(
		'post_type'     => FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE,
		'post_status'   => array( 'publish' ),
		'cache_results' => false,
		'nopaging'      => true,
		'meta_key'      => FIREWORK_VIDEOS_VIDEO_FEED_META_UID,
		'meta_value'    => $firework_app_id,
	);

	$posts = get_posts( $args );

	if ( empty( $posts ) ) {
		return false;
	} else {
		return \Firework_Videos\VideoFeed::get( $posts[0] );
	}
}

/**
 * Returns if a member is logged into Firework with a token
 *
 * @return boolean
 */
function firework_videos_is_logged_in () {
	return Firework_Videos\Options::has_auth();
}
