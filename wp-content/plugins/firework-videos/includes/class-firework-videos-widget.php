<?php

namespace Firework_Videos;

/*
Firework Widget Class
 */

if ( ! class_exists( 'Firework_Videos\Firework_Videos_Widget' ) ) {

	class Firework_Videos_Widget extends \WP_Widget {
		const MODE_ROW = 'row';
		const MODE_GRID = 'grid';
		const MODE_PINNED = 'pinned';

		public function __construct() {
			parent::__construct(

			// Base ID of your widget
				'firework_videos_widget',

				// Widget name will appear in UI
				__( 'Firework Video Gallery', 'firework-videos' ),

				// Widget description
				array( 'description' => __( 'Display short videos feed on your site.', 'firework-videos' ) )
			);
		}

		// Creating widget front-end

		public function widget( $args, $instance ) {
			firework_videos_enqueue_feed_script();

			// Sanitize title
			$fwn_title = apply_filters( 'widget_title', $instance['fwn_title'] );

			// Start content rendering
			echo $args['before_widget'];

			// Build widget title
			if ( ! empty( $fwn_title ) ) {
				echo $args['before_title'] . $fwn_title . $args['after_title'];
			}

            //initialization javascript
            $script = ScriptGenerator::generate( $instance['fwn_app_id'], $instance['fwn_mode'], $instance['fwn_max_videos'], '' );
			echo $script;

			echo $args['after_widget'];
		}

		// Widget settings
		public function form( $instance ) {
			$fwn_app_id     = isset( $instance['fwn_app_id'] ) ? $instance['fwn_app_id'] : '';
			$fwn_mode       = isset( $instance['fwn_mode'] ) ? $instance['fwn_mode'] : FIREWORK_VIDEOS_DEFAULT_FEED_LAYOUT_MODE;
			$fwn_title      = isset( $instance['fwn_title'] ) ? $instance['fwn_title'] : __( 'Stories and Short Videos', 'firework-videos' );
			$fwn_max_videos = isset( $instance['fwn_max_videos'] ) ? $instance['fwn_max_videos'] : '15';

			// Widget admin form
			?>
            <p>
                <label
                        for="<?php echo $this->get_field_id( 'fwn_title' ); ?>"
                ><?php echo __( 'Title:', 'firework-videos' ); ?></label>
                <input
                        class="widefat"
                        id="<?php echo $this->get_field_id( 'fwn_title' ); ?>"
                        name="<?php echo $this->get_field_name( 'fwn_title' ); ?>"
                        type="text"
                        value="<?php echo esc_attr( $fwn_title ); ?>"
                />
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'fwn_app_id' ); ?>"><?php echo __( 'Feed:', 'firework-videos' ); ?></label>
                <select class="widefat"
                        id="<?php echo $this->get_field_id( 'fwn_app_id' ); ?>"
                        name="<?php echo $this->get_field_name( 'fwn_app_id' ); ?>"
                        type="text"
                        value="<?php echo esc_attr( $fwn_app_id ); ?>">
                    <option><?php _e('Select Feed', 'firework-videos'); ?></option>
                    <?php foreach ( firework_videos_get_all_feeds() as $feed ) {
                        $checked = ( $fwn_app_id === $feed->firework_app_id ) ? ' selected="selected" ' : '';
	                    echo '<option value="' . $feed->firework_app_id . '"' . $checked . '>' . $feed->name . '</option>';
                    } ?>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'fwn_mode' ); ?>"><?php echo __( 'Mode:', 'firework-videos' ); ?></label>
                <select
                        class="widefat"
                        id="<?php echo $this->get_field_id( 'fwn_mode' ); ?>"
                        name="<?php echo $this->get_field_name( 'fwn_mode' ); ?>"
                >
                    <option value="<?php echo self::MODE_ROW ?>" <?php echo $fwn_mode == self::MODE_ROW ? 'selected' : '' ?>><?php echo __( 'Row', 'firework-videos' ) ?></option>
                    <option value="<?php echo self::MODE_GRID ?>" <?php echo $fwn_mode == self::MODE_GRID ? 'selected' : '' ?>><?php echo __( 'Grid', 'firework-videos' ) ?></option>
                    <option value="<?php echo self::MODE_PINNED ?>" <?php echo $fwn_mode == self::MODE_PINNED ? 'selected' : '' ?>><?php echo __( 'Pinned', 'firework-videos' ) ?></option>
                </select>
            </p>
            <p>
                <label
                        for="<?php echo $this->get_field_id( 'fwn_max_videos' ); ?>"
                ><?php echo __( 'Max. videos (Grid only):', 'firework-videos' ); ?></label>
                <input
                        class="widefat"
                        id="<?php echo $this->get_field_id( 'fwn_max_videos' ); ?>"
                        name="<?php echo $this->get_field_name( 'fwn_max_videos' ); ?>"
                        type="number"
                        value="<?php echo esc_attr( $fwn_max_videos ); ?>"
                />
            </p>
			<?php
		}

		// Updating widget replacing old instances with new
		public function update( $new_instance, $old_instance ) {
			$instance                   = array();
			$instance['fwn_app_id']     = ( ! empty( $new_instance['fwn_app_id'] ) ) ? strip_tags( $new_instance['fwn_app_id'] ) : '';
			$instance['fwn_mode']       = ( ! empty( $new_instance['fwn_mode'] ) ) ? strip_tags( $new_instance['fwn_mode'] ) : '';
			$instance['fwn_title']      = ( ! empty( $new_instance['fwn_title'] ) ) ? strip_tags( $new_instance['fwn_title'] ) : '';
			$instance['fwn_max_videos'] = ( ! empty( $new_instance['fwn_max_videos'] ) ) ? strip_tags( $new_instance['fwn_max_videos'] ) : '';

			return $instance;
		}
	}
}
