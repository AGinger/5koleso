<?php

namespace Firework_Videos;

/**
 * Class for generating the script block
 */

if ( ! class_exists( 'Firework_Videos\ScriptGenerator' ) ) {
	class ScriptGenerator {

		/**
		 * Generates the javascript to show the feed
		 *
		 * @param $firework_app_id
		 * @param $mode
		 * @param $max_videos
		 * @param $title
		 * @param string $placement
		 * @param bool $add_margins
		 *
		 * @return string
		 */
		public static function generate( $firework_app_id, $mode, $max_videos, $title, $placement = 'middle', $add_margins = false ) {
			if ( empty( $firework_app_id) ) {
				return '';
			}

			$container_id = 'fwn_videos_' . rand( 1000, 9999 );

			$style = '';

			//override placement for pinned
			if ( $mode === 'pinned' ) {
				$placement = 'bottom';
			} else {
				if ( $add_margins ) {
					//add some inline css to the container
					if ( $placement === 'top' ) {
						$style = ' style="margin-bottom:10px;" ';
					} else {
						$style = ' style="margin-top:10px;" ';
					}
				}
			}

			$html = '<div id="' . esc_attr( $container_id ) . '"' . $style . '></div>';
			$html .= '<script type="text/javascript">';

			// FWN widget queue for async loading and multiple widget instances
			$html .= '
            !function(e,t,c,a){if(!e.fwn&&(a="fwn_script",n=e.fwn=function(){
            n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)
            },e._fwn||(e._fwn=n),n.queue=[],!t.getElementById(a))){}
            }(window,document,"--fwn.js enqueued using wordpress callbacks--");
            ';

			$html .= '
            fwn({
                app_id: "' . $firework_app_id . '",
                mode: "' . $mode . '",
                ' . ( $max_videos ? ( 'max_videos: ' . $max_videos . ',' ) : '' ) . '
                page_type: "article",
                ' . ( ( $mode === 'grid' && !empty( $title ) ) ? ( 'title: "' . $title . '",' ) : '' ) . '
                placement: "' . $placement . '",
                target: document.getElementById("' . $container_id . '")
            });';

			$html .= '</script>';

			return $html;
		}
	}
}