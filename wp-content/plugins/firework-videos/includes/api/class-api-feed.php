<?php
namespace Firework_Videos\Api;

use WP_Error;
use Firework_Videos;

/**
 * API class for feed functions
 */

if ( !class_exists( 'Firework_Videos\Api\ApiFeed' ) ) {

	class ApiFeed extends ApiBase {
		/**
		 * Business ID
		 *
		 * @var string
		 */
		protected $business_id;

		public function __construct() {
			parent::__construct();
			$this->business_id = Firework_Videos\Options::get_business_id();
		}

		/**
		 * Returns true if we have a business id
		 * @return bool
		 */
		public function has_business_id() {
			return $this->business_id !== false;
		}

		/**
		 * Returns a new WP_Error for not having a business id
		 * @return WP_Error
		 */
		protected function get_business_error() {
			return new WP_Error( 'firework-api-business-error', __( 'No Business Set!', 'firework-videos' ) );
		}

		/**
		 * return all feeds for the business
		 *
		 * @return array|WP_Error
		 */
		function get_feeds() {
			if ( ! $this->has_auth() ) {
				return $this->get_auth_error();
			}

			if ( !$this->has_business_id() ) {
				return $this->get_business_error();
			}

			$this->build_request( 'bus/' . $this->business_id . '/feeds/' );
			return $this->fetch();
		}

		/**
		 * Create a new feed
		 *
		 * @param $name
		 * @param $country
		 * @param $source
		 * @param $base_tag_ids
		 * @param $mode
		 * @param $max_videos
		 *
		 * @return array|WP_Error
		 */
		function create_feed( $name, $country, $source, $base_tag_ids, $mode, $max_videos ) {
			if ( ! $this->has_auth() ) {
				return $this->get_auth_error();
			}

			if ( !$this->has_business_id() ) {
				return $this->get_business_error();
			}

			$body = array(
				'name' => $name,
				'country' => $country,
				'source' => $source,
				'base_tag_ids' => $base_tag_ids,
				'mode' => $mode,
				'max_videos' => $max_videos
			);

			$this->build_request( 'bus/' . $this->business_id . '/feeds/', $body, 'POST' );
			return $this->fetch();
		}

		/**
		 * Create a new feed
		 *
		 * @param $feed_id
		 * @param $name
		 * @param $country
		 * @param $source
		 * @param $base_tag_ids
		 * @param $mode
		 * @param $max_videos
		 *
		 * @return array|WP_Error
		 */
		function update_feed( $feed_id, $name, $country, $source, $base_tag_ids, $mode, $max_videos ) {
			if ( ! $this->has_auth() ) {
				return $this->get_auth_error();
			}

			if ( !$this->has_business_id() ) {
				return $this->get_business_error();
			}

			$body = array(
				'name' => $name,
				'country' => $country,
				'source' => $source,
				'base_tag_ids' => array_values( $base_tag_ids ),
				'mode' => $mode,
				'max_videos' => $max_videos
			);

			$this->build_request( 'bus/' . $this->business_id . '/feeds/' . $feed_id, $body, 'PUT' );
			return $this->fetch();
		}

		function delete_feed( $feed_id ) {
			if ( ! $this->has_auth() ) {
				return $this->get_auth_error();
			}

			if ( !$this->has_business_id() ) {
				return $this->get_business_error();
			}

			$body = array(
				'id' => $feed_id
			);

			$this->build_request( 'bus/' . $this->business_id . '/feeds/' . $feed_id, $body, 'DELETE' );
			return $this->fetch();

		}
	}
}
