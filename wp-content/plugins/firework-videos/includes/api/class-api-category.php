<?php
namespace Firework_Videos\Api;

use WP_Error;

/**
 * API class for category functions
 */

if ( !class_exists( 'Firework_Videos\Api\ApiCategory' ) ) {

	class ApiCategory extends ApiBase {

		/**
		 * return all categories
		 *
		 * @return array|WP_Error
		 */
		function get_categories() {
			if ( ! $this->has_auth() ) {
				return $this->get_auth_error();
			}

			$this->build_request( '/bus/feeds/tags/' );
			return $this->fetch();
		}
	}
}
