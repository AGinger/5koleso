<?php

namespace Firework_Videos\Api;

use WP_Error;

/**
 * API class for session functions
 */

if ( ! class_exists( 'Firework_Videos\Api\ApiSession' ) ) {

	class ApiSession extends ApiBase {

		/**
		 * Returns a guest token that can be used for login
		 *
		 * @return array|WP_Error
		 */
		function get_token() {
			$body = array(
				'provider' => 'fw_visitor',
				'vit'      => ''
			);

			$this->build_request( 'sessions/', $body, 'POST', true );

			return $this->fetch();
		}
	}
}