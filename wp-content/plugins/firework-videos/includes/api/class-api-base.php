<?php

namespace Firework_Videos\Api;

use WP_Error;
use Firework_Videos;

/**
 * Base class for all API classes
 */

if ( ! class_exists( 'Firework_Videos\Api\ApiBase' ) ) {

	class ApiBase {
		/**
		 * Auth Token
		 *
		 * @var string
		 */
		protected $auth_token;

		/**
		 * Arguments
		 *
		 * @var array
		 */
		protected $args;

		/**
		 * API Route
		 *
		 * @var string
		 */
		protected $route;

		/**
		 * __construct function.
		 */
		public function __construct() {
			$this->auth_token = Firework_Videos\Options::get_access_token();
			$this->args       = array();
		}

		/**
		 * Returns true if we have a auth token
		 * @return bool
		 */
		public function has_auth() {
			return $this->auth_token !== false;
		}

		/**
		 * Returns a new WP_Error for not being authenticated
		 * @return WP_Error
		 */
		protected function get_auth_error() {
			return new WP_Error( 'firework-api-auth-error', __( 'Not Authenticated!', 'firework-videos' ) );
		}

		/**
		 * Prepares API request.
		 *
		 * @param string $route API route to make the call to.
		 * @param array $args Arguments to pass into the API call.
		 * @param string $method HTTP Method to use for request.
		 * @param bool $exclude_auth_header Optional wheather to exclude authorization header
		 */
		protected function build_request( $route, $args = array(), $method = 'GET', $exclude_auth_header = false ) {
			$this->args['headers'] = array(
				'Authorization' => 'Bearer ' . $this->auth_token,
				'Content-Type'  => 'application/json',
			);

			if ( $exclude_auth_header === true ) {
				unset( $this->args['headers']['Authorization'] );
			}

			$this->args['method'] = $method;
			$this->route          = $route;

			// Generate query string for GET requests.
			if ( 'GET' === $method ) {
				$this->route = add_query_arg( array_filter( $args ), $route );
			} elseif ( 'application/json' === $this->args['headers']['Content-Type'] ) {
				$this->args['body'] = wp_json_encode( $args );
			} else {
				$this->args['body'] = $args;
			}
		}

		/**
		 * Fetch the request from the API.
		 *
		 * @access private
		 * @return array|WP_Error Request results or WP_Error on request failure.
		 */
		protected function fetch() {
			// Make the request.
			$response = wp_remote_request( FIREWORK_VIDEOS_URL_API . $this->route, $this->args );

			// Retrieve Status code & body.
			$code = wp_remote_retrieve_response_code( $response );
			$body = json_decode( wp_remote_retrieve_body( $response ), true );

			// Return WP_Error if request is not successful.
			if ( ! $this->is_status_ok( $code ) ) {
				return new WP_Error( 'firework-api-response-error', sprintf( __( 'Bad Response: %d', 'firework-videos' ), $code ), $body );
			}

			return $body;
		}

		/**
		 * Check if HTTP status code is a success.
		 *
		 * @param int $code HTTP status code.
		 *
		 * @return boolean       True if status is within valid range.
		 */
		protected function is_status_ok( $code ) {
			return ( 200 <= $code && 300 > $code );
		}
	}
}