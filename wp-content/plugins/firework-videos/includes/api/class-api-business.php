<?php
namespace Firework_Videos\Api;

use WP_Error;

/**
 * API class for business functions
 */

if ( ! class_exists( 'Firework_Videos\Api\ApiBusiness' ) ) {

	class ApiBusiness extends ApiBase {

		/**
		 * Create a business
		 *
		 * @param $name
		 * @param $website
		 * @param $email
		 *
		 * @return array|WP_Error
		 */
		function create_business( $name, $website, $email ) {
			if ( ! $this->has_auth() ) {
				return $this->get_auth_error();
			}

			$body = array(
				'name'                  => $name,
				'website'               => $website,
				'primary_email_address' => $email
			);

			$this->build_request( 'bus/', $body, 'POST' );

			return $this->fetch();
		}

		/**
		 * Get all businesses
		 *
		 * @return array|WP_Error
		 */
		function get_businesses() {
			if ( ! $this->has_auth() ) {
				return $this->get_auth_error();
			}

			$this->build_request( 'bus/' );

			return $this->fetch();
		}
	}
}