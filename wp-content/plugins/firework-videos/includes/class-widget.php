<?php
namespace Firework_Videos;

/*
Firework Widget Init Class
 */

if ( !class_exists( 'Firework_Videos\Widget' ) ) {
	class Widget {
		public function __construct() {
			add_action('widgets_init', array($this, 'register_widget'));
		}

		public function register_widget() {
			register_widget('Firework_Videos\Firework_Videos_Widget');
		}
	}
}