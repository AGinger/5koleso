<?php

namespace Firework_Videos;

use WP_Error;
use WP_Post;

/**
 * Class for doing a sync with Firework
 */

if ( ! class_exists( 'Firework_Videos\Sync' ) ) {
	class Sync {
		/**
		 * What errors occurred when the sync ran
		 * @var array
		 */
		public $errors;

		public function __construct() {
			$this->errors = array();
		}

		/**
		 * Returns true if there were any errors during the sync
		 * @return bool
		 */
		public function has_errors() {
			return count( $this->errors ) > 0;
		}

		/**
		 * checks that we have auth
		 * @return bool
		 */
		private function check_auth() {
			if ( ! Options::has_auth() ) {
				$this->errors[] = __( 'You are not authenticated with Firework! Please authenticate and try again.', 'firework-videos' );

				return false;
			}

			return true;
		}

		public function run_full_sync() {
			//this should be run when we have connected with Firework and have an auth token

			//check we have auth set
			if ( $this->check_auth() ) {

				//sync all category tags
				$this->sync_categories();

				//sync the business for the current site
				$this->sync_business();

				//sync all feeds for the business
				$this->sync_feeds();
			}

			return !$this->has_errors();
		}

		/**
		 * Syncs all categories from the API and stores them
		 */
		public function sync_categories() {
			if ( $this->check_auth() ) {
				$api = new namespace\Api\ApiCategory();

				$response = $api->get_categories();

				if ( is_wp_error( $response ) ) {
					$this->errors[] = __( 'Could not sync categories! Error : ', 'firework-videos' ) . $this->extract_error( $response );
				} else if ( is_array( $response ) && count( $response ) > 0 ) {
					//store the categories in options
					$categories = $response['tags'];

					Options::set_categories( $categories );
				} else {
					$this->errors[] = __( 'No categories were found!', 'firework-videos' );
				}

			} else {
				$this->errors[] = __( 'Could not sync categories!', 'firework-videos' );
			}
		}

		/**
		 * Extracts the error from the API calls
		 * @param $error
		 *
		 * @return mixed|string|void
		 */
		public function extract_error( $error ) {
			if ( is_wp_error( $error ) ) {
				$error_data = $error->get_error_data();
				if ( is_array( $error_data ) ) {
					if ( array_key_exists( 'message', $error_data ) ) {
						return $error_data['message'];
					} else if ( array_key_exists( 'error', $error_data ) ) {
						return $error_data['error'];
					}
				}
				return $error_data;
			}
			return __( 'Unknown', 'firework-videos' );
		}

		/***
		 * Syncs the business for the current site, and creates one if needed
		 */
		public function sync_business() {
			if ( $this->check_auth() ) {

				$api = new namespace\Api\ApiBusiness();

				if ( ! $api->has_auth() ) {
					return false;
				}

				$businesses_response = $api->get_businesses();

				$businesses = array();

				if ( is_wp_error( $businesses_response ) ) {
					$this->errors[] = __( 'Could not sync business! Error : ', 'firework-videos' ) . $this->extract_error( $businesses_response );
				} else {

					if ( array_key_exists( 'businesses', $businesses_response ) ) {
						$businesses = $businesses_response['businesses'];
					}

					$current_domain = $this->get_domain( site_url() );

					//loop through the businesses and get the one created for this site
					foreach ( $businesses as $business ) {
						if ( $this->get_domain( $business['website'] ) ===  $current_domain ) {
							//this is the business for the current website

							//store the business id in options
							Options::set_business_id( $business['id'] );
							return true;
						}
					}

					//if we get here, then it means we could not find a business for the current site
					$name    = get_bloginfo( 'name' );
					$website = site_url();
					$email   = get_bloginfo( 'admin_email' );

					//create a business
					$businesses_response = $api->create_business( $name, $website, $email );
					if ( !is_wp_error( $businesses_response ) ) {
						Options::set_business_id( $businesses_response['id'] );
						return true;
					} else {
						$this->errors[] = __( 'Could not create business! Error : ', 'firework-videos' ) . $this->extract_error( $businesses_response );
					}
				}
			} else {
				$this->errors[] = __( 'Could not sync business!', 'firework-videos' );
			}
		}

		function get_domain( $url ) {
			$parsed_url = parse_url( trim( $url ) );
			if ( isset( $parsed_url['host'] ) ) {
				return $parsed_url['host'];
			}
			$url_parts = explode( '/', $parsed_url['path'], 2 );
			return array_shift( $url_parts );
		}


		/**
		 * Syncs the feeds from the API and creates all the post types
		 */
		public function sync_feeds() {
			//check we have a business
			if ( Options::get_business_id() !== false ) {

				//get all feeds for the business from the API
				$api   = new namespace\Api\ApiFeed();
				$feeds_response = $api->get_feeds();

				if ( is_wp_error( $feeds_response ) ) {
					$this->errors[] = __( 'Could not sync feeds! Error : ', 'firework-videos' ) . $this->extract_error( $feeds_response );
				} else {

					$feeds = firework_videos_get_from_array( 'feeds', $feeds_response, false );

					if ( $feeds !== false ) {
						//get all feeds we have already saved to the video feed custom post type
						$feed_objects = firework_videos_get_all_feeds();

						//create the video feed custom posts
						foreach ( $feeds as $feed ) {
							$firework_id = $feed['id'];

							//try and find an already saved video feed custom post type
							$feed_object = $this->find_feed_in_post_array( $firework_id, $feed_objects );

							if ( $feed_object === false ) {
								//create a new feed custom post type
								$this->create_feed( $feed );
							} else {
								//update the existing feed custom post type
								$this->update_feed( $feed_object->ID, $feed );

								//remove the feed from the array so that it does not get trashed
								unset( $feed_objects[ $feed_object->ID ] );
							}
						}

						//clean up any left over feeds - these should be trashed
						foreach ( $feed_objects as $feed ) {
							wp_update_post( array(
								'ID'          => $feed->ID,
								'post_status' => 'trash'
							) );
						}
					}
				}

			} else {
				$this->errors[] = __( 'Could not sync feeds! No business found.', 'firework-videos' );
			}
		}

		/**
		 * Find the existing feed from an array of video feed objects
		 *
		 * @param $firework_id
		 * @param $feed_objects
		 *
		 * @return bool|mixed
		 */
		private function find_feed_in_post_array( $firework_id, $feed_objects ) {
			foreach ( $feed_objects as $feed_object ) {
				if ( $feed_object->firework_id === $firework_id ) {
					return $feed_object;
				}
			}

			return false;
		}

		/**
		 * Create a feed custom post type from the feed data
		 *
		 * @param $feed_object
		 */
		private function create_feed( $feed_object ) {
			$post_id = $this->create_feed_custom_post_type( $feed_object['name'] );

			//link the custom post type to the feed, so updates will work
			update_post_meta( $post_id, FIREWORK_VIDEOS_VIDEO_FEED_META_ID, $feed_object['id'] );
			update_post_meta( $post_id, FIREWORK_VIDEOS_VIDEO_FEED_META_UID, $feed_object['uid'] );

			//update the feed details
			$this->update_feed( $post_id, $feed_object );
		}

		/**
		 * Create the VideoFeed custom post type
		 *
		 * @param $name
		 *
		 * @return int|WP_Error
		 */
		public function create_feed_custom_post_type( $name ) {
			$post = apply_filters( 'firework_videos_create_video_feed_arguments', array(
				'post_content' => '',
				'post_title'   => $name,
				'post_status'  => 'publish',
				'post_type'    => FIREWORK_VIDEOS_VIDEO_FEED_CUSTOM_POST_TYPE,
			) );

			return wp_insert_post( $post );
		}

		/**
		 * Update a feed custom post type from the feed data
		 *
		 * @param $post_id int
		 * @param $feed_object
		 */
		private function update_feed( $post_id, $feed_object ) {
			$settings = get_post_meta( $post_id, FIREWORK_VIDEOS_VIDEO_FEED_META_SETTINGS, true );

			if ( !is_array( $settings ) ) {
				$settings = array();
			}

			if ( array_key_exists( 'base_tag_ids', $feed_object ) ) {
				$settings['category'] = $feed_object['base_tag_ids'];
			}
			if ( array_key_exists( 'mode', $feed_object ) ) {
				$settings['layout'] = $feed_object['mode'];
			}
			if ( array_key_exists( 'max_videos', $feed_object ) ) {
				$settings['video_count'] = $feed_object['max_videos'];
			}

			//set the feed settings
			update_post_meta( $post_id, FIREWORK_VIDEOS_VIDEO_FEED_META_SETTINGS, $settings );
		}

		/**
		 * Push the Video Feed object back up to the Firework API
		 *
		 * @param $feed VideoFeed
		 */
		public function sync_single_feed( $feed ) {
			$api = new namespace\Api\ApiFeed();

			if ( empty( $feed->firework_id ) ) {
				//Create the feed
				$feed_response = $api->create_feed(
					$feed->name,
					'US',
					site_url(),
					$feed->categories,
					$feed->layout,
					$feed->video_count
				);

				if ( is_wp_error( $feed_response ) ) {
					//bad response from the API
				} else {
					//successfully created. Save post meta to link them
					update_post_meta( $feed->ID, FIREWORK_VIDEOS_VIDEO_FEED_META_ID, $feed_response['id'] );
					update_post_meta( $feed->ID, FIREWORK_VIDEOS_VIDEO_FEED_META_UID, $feed_response['uid'] );
				}
			} else {
				//update the feed
				$feed_response = $api->update_feed(
					$feed->firework_id,
					$feed->name,
					'US',
					site_url(),
					$feed->categories,
					$feed->layout,
					$feed->video_count
				);
			}

			if ( is_wp_error( $feed_response ) ) {
				//bad response from the API
				return false;
			} else {
				if ( defined( 'FIREWORK_VIDEOS_DEBUG' ) && FIREWORK_VIDEOS_DEBUG === true  ) {
					if ( isset( $feed_response ) && !is_wp_error( $feed_response ) ) {
						update_post_meta( $feed->ID, FIREWORK_VIDEOS_VIDEO_FEED_META_DEBUG, $feed_response );
					}
				}

				//successfully updated
				return true;
			}
		}
	}
}