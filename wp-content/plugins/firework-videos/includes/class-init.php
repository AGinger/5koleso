<?php
namespace Firework_Videos;

/**
 * Firework Init Class
 * Runs at the startup of the plugin
 * Assumes after all checks have been made, and all is good to go!
 *
 * Date: 06/05/2020
 */

if ( !class_exists( 'Firework_Videos\Init' ) ) {

	class Init {

		/**
		 * Initialize the plugin by setting localization, filters, and administration functions.
		 */
		public function __construct() {

			//load the plugin text domain
			add_action( 'plugins_loaded', function() {
				load_plugin_textdomain(
					FIREWORK_VIDEOS_SLUG,
					false,
					dirname( plugin_basename( FIREWORK_VIDEOS_FILE ) ) . '/languages/'
				);
			});



			require_once( FIREWORK_VIDEOS_PATH . 'includes/constants.php' );

			//setup post types
			new namespace\PostTypes();
			new namespace\Widget();

			if ( is_admin() ) {
				new namespace\Admin\Init();
			}

			new namespace\AutoEmbed();
		}
	}
}