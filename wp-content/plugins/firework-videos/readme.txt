=== Short Video Gallery by Firework ===
Contributors: loopnowplugins, stefanbackor, dwalintukan, bradvin
Tags: video, short video content, best video gallery, social media, responsive gallery, SEO optimizer 
Requires at least: 5.0
Tested up to: 5.4.1
Requires PHP: 5.4
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


The 1st video gallery that curates the best short videos on the Internet. Customize a short video gallery to engage the audience and draw traffic!    


== Description ==

##The 1st Short Video Gallery Plugin on WordPress

Watch it in action:

<iframe width="560" height="315" src="https://www.youtube.com/embed/K3EuNWh5QWM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Short Video Gallery turns your text-based Wordpress site into a media-rich content avenue with a lightweight and aesthetically beautiful feed of relevant short videos, enhancing your SEO in 4 ways:
 
*1. Longer session time and reduce bounce rate*
*2. Free high-quality video feed with zero production cost*
*3. Mobile-first tap & swipe design to capture more screen time*
*4. Higher engagement on your blog leading to better search rankings*
 
**Create your own mobile-friendly short video feeds for free with over 1 million+ premium video content from Firework - the fastest-growing video social media in the world.** 


## What makes us stand out? 


#### 1. High-engagement Contextual Content 

Our rich short video content library with 20+ categories includes:

- Style & Fashion
- Fitness & Exercise
- Food & Drink
- Pets
- Travel
- & much more!


#### 2. No Compromise on Page Speed

Videos load from a high bandwidth, third party server imposing no limitation on your page speed.


#### 3. Make Your Website Mobile-ready

Convert your website into a short video social media for free. Our tap & swipe-through video story feed layouts that are perfect for the mobile web. 


#### 4. Multiple video thumbnail embeds with autoplay.

Three different gallery layouts to fit in every page type. Draw the eyeballs of your audiences with autoplay video thumbnails. 


## Case Studies

> [The Sports Daily ](https://thesportsdaily.com/2020/05/28/could-jadeveon-clowney-actually-be-purposely-avoiding-signing-with-browns/) is a subsidiary of USA Today and is a popular sports news website. 30 days post integrating the Short Video Gallery beta version, its organic visitor per week increased by 112.71%. 

> [A popular Japanese pet dog blog](kyuzitsu-inubu.com) has seen a 129% increase in its desktop visitor duration time, better SEO, and thus, 145% increase in organic traffic 30 days after installing the Short Video Gallery feed. 


## Short Video Gallery fits all types of websites. See more at: 

[Lifestyle Blog](https://wordandsole.com)
[Destination Wedding Website](http://honeymoons.com) 
[Travel Blog](https://epicureandculture.com)
[Mom & Lifestyle Blog](https://bertmanderson.com/positive-affirmations-working-moms/)


## What is Firework?

[Firework](https://fireworktv.com) is a US-based fast-growing short-video platform with 30-second video content from premium partners and verified creators across categories like travel, lifestyle, food, pets, wellness and others.


If you have any questions, please feel free to reach us at plugins@loopapps.com


== Frequently Asked Questions ==


= Can I use these videos for free? =
Yes, all the videos from the Short Video Gallery are public videos uploaded to Firework. You can use this plugin for free. You can think of embedding this Gallery as embedding a series of 30-second clips from YouTube.  


= Do I need to download the Firework App for this plugin? =
No, you don't need to download an app. All you need is to register with Firework so that the platform can authorize you with all the amazing content! 


= Can I upload my own videos to this gallery? =
Currently, the plugin only offers a *set menu* of videos of 20+ categories. It is great for users looking for popular short video content who don't have the ability to produce so much on their own. We will add options for video upload in the future updates :) 


= Can I be a Firework creator? =
Please visit our website: info.fireworktv.com for more information. 


= Can I change the content in a gallery category? =
Right now, the video content is dynamic and fed through the active Firework video pipeline. If you want only a limited number of videos, you can use the *max video* option in the plugin.


= Why do I see sponsored content within the gallery feed? =
Firework powers the rich content library provided to you for free. We as the developer bear the cost that comes with acquiring premium short video content. If you want to learn more about the publisher’s program and learn how to monetize through Short Video Gallery, please contact: publisher@firewortnetwork.tv


== Installation ==

PERFORM A NEW INSTALLATION
After downloading the ZIP file of the plugin,

1. Log in to the administrator panel.
2. Go to Plugins Add > New > Upload.
3. Click Choose file (**Browse**) and select the downloaded zip file of the plugin.
4. Click **Install Now** button.
5. Click **Activate** button for activating Short Video Gallery. You will need to register an account with Firework to access the videos. 


== Screenshots ==

1. Frontend - Row Mode
2. Frontend - Video Player
3. Frontend - Grid Mode
4. Frontend - Pinned Mode
5. Frontend - Mobile View 
6. Case Studies
7. Backend - Onboarding Wizard
8. Backend - Authenticate with Firework
9. Backend - Make Your First Feed
10. Backend - Choose the Feed Placement
11. Backend - Video Update & Preview
12. Backend - Two Ways to Embed the Gallery


== Upgrade Notice ==

Update now to get all the latest features, bug fixes and improvements!

== Changelog ==

= 2.0.7 =
* Fix : minor bug with feed syncing

= 2.0.6 =
* Update : readme and screenshots

= 2.0.5 =
* Fix : fixed deploy issues with assets

= 2.0.3 =
* New : onboarding wizard
* New : full video feed sync with Firework
* New : manage your video feed like any other post in the admin
* New : global embeds for feeds

= 1.0 =
* First version with widget support
