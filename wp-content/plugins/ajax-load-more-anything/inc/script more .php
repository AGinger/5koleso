<script type="text/javascript">
		(function($) {
			'use strict';

			jQuery(document).ready(function() {

				var noItemMsg = "Load more button hidden because no more item to load";

				//wrapper zero
				<?php if(!empty(get_option('ald_wrapper_class'))):?>
					$("<?php echo esc_attr( get_option('ald_wrapper_class') ); ?>").append('<div class="lmb-container"><a href="#" class="btn loadMoreBtn" id="loadMore"><?php echo esc_attr( get_option('ald_load_label') ); ?></a></div>');

					$("<?php echo esc_attr( get_option('ald_load_class') ); ?>").slice(0, <?php echo esc_attr( get_option('ald_item_show') ); ?>).show();
					$("#loadMore").on('click', function (e) {
						e.preventDefault();
						$("<?php echo esc_attr( get_option('ald_load_class') ); ?>:hidden").slice(0, <?php echo esc_attr( get_option('ald_item_load') ); ?>).slideDown();
						if ($("<?php echo esc_attr( get_option('ald_load_class') ); ?>:hidden").length == 0) {
							$("#loadMore").fadeOut('slow');
						}
					});
					if ($("<?php echo esc_attr( get_option('ald_load_class') ); ?>:hidden").length == 0) {
						$("#loadMore").fadeOut('slow');
						console.log( noItemMsg );
					}
				<?php endif;?>
				// end wrapper 1

				//wrapper 2
				<?php if(!empty(get_option('ald_wrapper_classa'))):?>
					$("<?php echo esc_attr( get_option('ald_wrapper_classa') ); ?>").append('<div class="lmb-container"><a href="#" class="btn loadMoreBtn" id="loadMorea"><?php echo esc_attr( get_option('ald_load_labela') ); ?></a></div>');

					$("<?php echo esc_attr( get_option('ald_load_classa') ); ?>").slice(0, <?php echo esc_attr( get_option('ald_item_showa') ); ?>).show();

					$("#loadMorea").on('click', function (e) {
						e.preventDefault();
						$("<?php echo esc_attr( get_option('ald_load_classa') ); ?>:hidden").slice(0, <?php echo esc_attr( get_option('ald_item_loada') ); ?>).slideDown();
						if ($("<?php echo esc_attr( get_option('ald_load_classa') ); ?>:hidden").length == 0) {
							$("#loadMorea").fadeOut('slow');
						}
					});
					if ($("<?php echo esc_attr( get_option('ald_load_classa') ); ?>:hidden").length == 0) {
						$("#loadMorea").fadeOut('slow');
						console.log( noItemMsg );
					}
				<?php endif;?>
				// end wrapper 2

				// wrapper 3
				<?php if(!empty(get_option('ald_wrapper_classb'))):?>
					$("<?php echo esc_attr( get_option('ald_wrapper_classb') ); ?>").append('<div class="lmb-container"><a href="#" class="btn loadMoreBtn" id="loadMoreb"><?php echo esc_attr( get_option('ald_load_labelb') ); ?></a></div>');

					$("<?php echo esc_attr( get_option('ald_load_classb') ); ?>").slice(0, <?php echo esc_attr( get_option('ald_item_showb') ); ?>).show();
					$("#loadMoreb").on('click', function (e) {
						e.preventDefault();
						$("<?php echo esc_attr( get_option('ald_load_classb') ); ?>:hidden").slice(0, <?php echo esc_attr( get_option('ald_item_loadb') ); ?>).slideDown();
						if ($("<?php echo esc_attr( get_option('ald_load_classb') ); ?>:hidden").length == 0) {
							$("#loadMoreb").fadeOut('slow');
							console.log( noItemMsg );
						}
					});
					if ($("<?php echo esc_attr( get_option('ald_load_classb') ); ?>:hidden").length == 0) {
						$("#loadMoreb").fadeOut('slow');
					}
				<?php endif;?>
				// end wrapper 3

				//wraper 4
				<?php if(!empty(get_option('ald_wrapper_classc'))):?>
					$("<?php echo esc_attr( get_option('ald_wrapper_classc') ); ?>").append('<div class="lmb-container"><a href="#" class="btn loadMoreBtn" id="loadMorec"><?php echo esc_attr( get_option('ald_load_labelc') ); ?></a></div>');

					$("<?php echo esc_attr( get_option('ald_load_classc') ); ?>").slice(0, <?php echo esc_attr( get_option('ald_item_showc') ); ?>).show();

					$("#loadMorec").on('click', function (e) {
						e.preventDefault();
						$("<?php echo esc_attr( get_option('ald_load_classc') ); ?>:hidden").slice(0, <?php echo esc_attr( get_option('ald_item_loadc') ); ?>).slideDown();
						if ($("<?php echo esc_attr( get_option('ald_load_classc') ); ?>:hidden").length == 0) {
							$("#loadMorec").fadeOut('slow');
						}
					});
					if ($("<?php echo esc_attr( get_option('ald_load_classc') ); ?>:hidden").length == 0) {
						$("#loadMorec").fadeOut('slow');
						console.log( noItemMsg );
					}
				<?php endif;?>
				// end wrapper 4

				//wrapper 5
				<?php if(!empty(get_option('ald_wrapper_classd'))):?>
					$("<?php echo esc_attr( get_option('ald_wrapper_classd') ); ?>").append('<div class="lmb-container"><a href="#" class="btn loadMoreBtn" id="loadMored"><?php echo esc_attr( get_option('ald_load_labeld') ); ?></a></div>');

					$("<?php echo esc_attr( get_option('ald_load_classd') ); ?>").slice(0, <?php echo esc_attr( get_option('ald_item_showd') ); ?>).show();

					$("#loadMored").on('click', function (e) {
						e.preventDefault();
						$("<?php echo esc_attr( get_option('ald_load_classd') ); ?>:hidden").slice(0, <?php echo esc_attr( get_option('ald_item_loadd') ); ?>).slideDown();
						if ($("<?php echo esc_attr( get_option('ald_load_classd') ); ?>:hidden").length == 0) {
							$("#loadMored").fadeOut('slow');
						}
					});
					if ($("<?php echo esc_attr( get_option('ald_load_classd') ); ?>:hidden").length == 0) {
						$("#loadMored").fadeOut('slow');
						console.log( noItemMsg );
					}
				<?php endif;?>
				// end wrapper 5

				//wrapper 5
				<?php if(!empty(get_option('ald_wrapper_classe'))):?>
					$("<?php echo esc_attr( get_option('ald_wrapper_classe') ); ?>").append('<div class="lmb-container"><a href="#" class="btn loadMoreBtn" id="loadMored"><?php echo esc_attr( get_option('ald_load_labele') ); ?></a></div>');

					$("<?php echo esc_attr( get_option('ald_load_classe') ); ?>").hide();

					$("<?php echo esc_attr( get_option('ald_load_classe') ); ?>").slice(0, <?php echo esc_attr( get_option('ald_item_showe') ); ?>).css({ 'display': 'flex' });

					$("#loadMored").on('click', function (e) {
						e.preventDefault();
						$("<?php echo esc_attr( get_option('ald_load_classe') ); ?>:hidden").slice(0, <?php echo esc_attr( get_option('ald_item_loade') ); ?>).css({ 'display': 'flex' });
						if ($("<?php echo esc_attr( get_option('ald_load_classe') ); ?>:hidden").length == 0) {
							$("#loadMored").fadeOut('slow');
						}
					});
					if ($("<?php echo esc_attr( get_option('ald_load_classe') ); ?>:hidden").length == 0) {
						$("#loadMored").fadeOut('slow');
						console.log( noItemMsg );
					}
				<?php endif;?>
				// end wrapper 6

			});

		})(jQuery);
	</script>